#!/bin/sh

if [ ! -n "$POSCA_MATRIX_URL" ]; then
  echo >&2 "Fatal error: POSCA_MATRIX_URL not set"
  exit 2
fi

if [ ! -n "$POSCA_MATRIX_DOMAIN" ]; then
  echo >&2 "Fatal error: POSCA_MATRIX_DOMAIN not set"
  exit 2
fi

for file in /usr/share/nginx/html/assets/index*.js;
do
	sed -i "s|POSCA_MATRIX_URL|$POSCA_MATRIX_URL|g" $file
	sed -i "s|POSCA_MATRIX_DOMAIN|$POSCA_MATRIX_DOMAIN|g" $file
done
