stages:
  - analysis
  - docker
  - deploy



# === Analysis ===

format:
  stage: analysis
  image: ocaml/opam:alpine-ocaml-5.1
  before_script:
    - opam switch create . 5.1.1 -y --deps-only || true
    - opam install -y . --deps-only
    - opam pin -y add posca.dev .
  script:
    - make format
  cache:
    key: dev
    when: always
    policy: pull
    paths:
      - _opam
      - _build

build:
  stage: analysis
  image: registry.gitlab.com/technostructures/docker-images/opam-npm:alpine-ocaml-5.1
  before_script:
    - opam switch create . 5.1.1 -y --deps-only || true
    - npm install --legacy-peer-deps
    - opam install -y . --deps-only
    - opam pin -y add posca.dev .
  script:
    - make bundle
  cache:
    key: dev
    when: always
    policy: pull
    paths:
      - _opam
      - _build
      - node_modules



# === Docker ===

.docker:
  stage: docker
  image: carlallen/docker:buildx
  services:
    - name: docker:dind
      command: ["dockerd", "--host=tcp://0.0.0.0:2375"]
      alias: 'docker'
  variables:
    - DOCKER_DRIVER: overlay2
    - DOCKER_HOST: tcp://docker:2375/
    - DOCKER_BUILDKIT: 1
  interruptible: true
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY

.docker_build:
  extends: .docker
  before_script:
    - docker buildx create --driver=docker-container --name=buildkit-builder --use
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - docker pull $IMAGE_TAG_LATEST || true
    - >
      docker buildx build
      -f $DOCKERFILE
      --pull
      --build-arg VCS_REF=$CI_COMMIT_SHA
      --build-arg VCS_URL=$CI_PROJECT_URL
      --tag $IMAGE_TAG
      --cache-from $IMAGE_TAG_LATEST
      --cache-from type=registry,ref=$IMAGE_TAG_CACHE
      --cache-to type=registry,ref=$IMAGE_TAG_CACHE,mode=max
      --push
      .

.docker_tag:
  extends: .docker
  needs:
    - build_docker_image
  script:
    - docker pull $IMAGE_TAG
    - docker tag $IMAGE_TAG $IMAGE_NEW_TAG
    - docker push $IMAGE_NEW_TAG
  variables:
    GIT_STRATEGY: none

Build production image:
  extends: .docker_build
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
    - if: $CI_COMMIT_BRANCH != "main"
      when: manual
  allow_failure: true
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    IMAGE_TAG_LATEST: $CI_REGISTRY_IMAGE:latest
    IMAGE_TAG_CACHE: $CI_REGISTRY_IMAGE:cache
    DOCKERFILE: Dockerfile
    VITE_MATRIX_URL: $VITE_MATRIX_URL
    VITE_MATRIX_HOMESERVER: $VITE_MATRIX_HOMESERVER
    VITE_API_URL: $VITE_API_URL

Tag latest production image:
  extends: .docker_tag
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  needs: ["Build production image"]
  environment:
    name: production
    url: https://posca.pm
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    IMAGE_NEW_TAG: $CI_REGISTRY_IMAGE:latest

Tag tagged production image:
  extends: .docker_tag
  needs: ["Build production image"]
  rules:
    - if: $CI_COMMIT_BRANCH == "main" && $CI_COMMIT_TAG
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    IMAGE_NEW_TAG: $CI_REGISTRY_IMAGE:CI_COMMIT_REF_NAME



# === Deploy ===

pages:
  stage: deploy
  # needs: ["Build production image"]
  # needs:
  #   - job: build_builder_image
  #     optional: true
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
  environment:
    name: pages
    url: https://dev.posca.pm
  image: registry.gitlab.com/technostructures/docker-images/opam-npm:alpine-ocaml-5.1
  interruptible: true
  before_script:
    - opam switch create . 5.1.1 -y --deps-only || true
    - npm install --legacy-peer-deps
    - opam install -y . --deps-only
    - opam pin -y add posca.dev .
  script:
    - rm -rf dist
    - make bundle
    - rm -rf public
    - mv dist public
    - cp public/index.html public/404.html
  cache:
    key: dev
    when: always
    policy: pull-push
    paths:
      - node_modules
      - _opam
      - _build
  artifacts:
    paths:
    - public
