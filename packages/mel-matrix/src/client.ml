type user_id = string

type sync_state =
  [`CATCHUP | `ERROR | `PREPARED | `RECONNECTING | `STOPPED | `SYNCING]
[@@deriving jsConverter]

type access_token = string

type home_server = string

type device_id = string

type login_map = string Js.Dict.t

type data

type event_emitter

type event_type = string

(* ([ | `room_message [@mel.as "m.room.message"] ] [@mel.string]) *)

type login_response =
  < user_id: user_id
  ; access_token: access_token
  ; home_server: home_server
  ; device_id: device_id >
  Js.t
(* also refresh_token and expires_in_ms *)

type create_room_options =
  { room_alias_name: string Js.Undefined.t
  ; visibility: string
  ; (* either public or private *)
    invite: user_id array
  ; name: string
  ; topic: string
  ; creation_content: string Js.Dict.t Js.Undefined.t
  ; initial_state: Models.dict_event array }

type create_room_response =
  < room_id: Common.room_id ; room_alias: string option > Js.t

type store

type indexed_db

type local_storage

type indexed_db_worker

type indexed_db_store_opts =
  { indexedDB: indexed_db
  ; localStorage: local_storage
  ; dbName: string
  ; workerFactory: unit -> indexed_db_worker }

external new_indexed_db_store : indexed_db_store_opts -> store
  = "IndexedDBStore"
[@@mel.new] [@@mel.module "matrix-js-sdk"]

type client_config_part =
  < base_url: string Js.Nullable.t
  ; error: string Js.Nullable.t
  ; state: string
  ; store: store >
  Js.t

type client_config = client_config_part Js.Dict.t

type auto_discovery =
  < findClientConfig: string -> client_config Js.Promise.t [@mel.meth] > Js.t

type paginate_opts = < backwards: bool ; limit: int > Js.t

type guest_params =
  {user_id: string; device_id: string; access_token: string; home_server: string}

type start_client_opts = {threadSupport: bool}

type request_token_response = {sid: string}

type auth_dict

type client =
  < credentials: < userId: user_id > Js.t
  ; baseUrl: string
  ; clientRunning: bool
  ; getHomeserverUrl: unit -> string [@mel.meth]
  ; getAccessToken: unit -> access_token [@mel.meth]
  ; getDomain: unit -> string [@mel.meth]
  ; startClient: start_client_opts -> unit Js.Promise.t [@mel.meth]
  ; stopClient: unit -> unit [@mel.meth]
  ; removeAllListeners: unit -> unit [@mel.meth]
  ; registerGuest: unit -> guest_params Js.Promise.t [@mel.meth]
  ; setGuest: bool -> unit [@mel.meth]
  ; login: string -> string Js.Dict.t -> login_response Js.Promise.t [@mel.meth]
  ; loginWithPassword: string -> string -> login_response Js.Promise.t
        [@mel.meth]
  ; register:
         string
      -> string
      -> string option
      -> auth_dict option (* -> auth_dict Js.Undefined.t *)
      -> login_response Js.Promise.t
        [@mel.meth]
  ; requestRegisterEmailToken:
      string -> string -> int -> request_token_response Js.Promise.t
        [@mel.meth]
  ; logout: bool -> unit Js.Promise.t [@mel.meth]
  ; isLoggedIn: unit -> bool [@mel.meth]
  ; isGuest: unit -> bool [@mel.meth]
  ; getAccountData: string -> Models.matrix_event Js.Undefined.t [@mel.meth]
  ; setRoomAccountData:
      Common.room_id -> string -> string Js.Dict.t -> bool Js.Promise.t
        [@mel.meth]
  ; createRoom: create_room_options -> create_room_response Js.Promise.t
        [@mel.meth]
  ; fetchRoomEvent: Common.room_id -> Common.event_id -> string Js.Promise.t
        [@mel.meth]
        (* ; getEventMapper: unit -> (string -> Models.matrix_event) [@mel.meth] *)
  ; getUserId: unit -> user_id [@mel.meth]
  ; getSyncState: unit -> string [@mel.meth]
  ; getUser: user_id -> User.t Js.Nullable.t [@mel.meth]
  ; getJoinedRooms:
      unit -> < joined_rooms: Common.room_id array > Js.t Js.Promise.t
        [@mel.meth]
  ; getThreadTimeline:
         Models.event_timeline_set
      -> Common.event_id
      -> Models.event_timeline Js.Promise.t
        [@mel.meth]
  ; getEventTimeline:
         Models.event_timeline_set
      -> Common.event_id
      -> Models.event_timeline Js.Promise.t
        [@mel.meth]
        (* ; getEventTimeline : Event_timeline_set.t -> Common.event_id -> (Event_timeline.t Js.Undefined.t) Js.Promise.t [@mel.meth] *)
  ; redactEvent:
      Common.room_id -> string -> < event_id: string > Js.t Js.Promise.t
        [@mel.meth]
  ; sendMessage:
         Common.room_id
      -> Models.event_content
      -> < event_id: string > Js.t Js.Promise.t
        [@mel.meth]
  ; store: Store.t
  ; getRoom: Common.room_id -> Models.room Js.Nullable.t
        [@mel.meth]
        (* ; resolveRoomAlias: Common.room_id -> < room_id: string > Js.t Js.Promise.t *)
        (*       [@mel.meth] *)
  ; getRoomIdForAlias: Common.room_id -> < room_id: string > Js.t Js.Promise.t
        [@mel.meth]
  ; peekInRoom: Common.room_id -> Models.room Js.Promise.t [@mel.meth]
  ; _AutoDiscovery: auto_discovery
  ; setMaxListeners: int -> unit [@mel.meth]
  ; supportsThreads: unit -> bool [@mel.meth]
  ; paginateEventTimeline:
      Models.event_timeline -> paginate_opts -> bool Js.Promise.t
        [@mel.meth] >
  Js.t

(* external matrixcs : client = "matrixcs"   *)
(* external matrixcs : client = "matrixc" [@@mel.scope "window"] *)

external find_client_config : string -> client_config Js.Promise.t
  = "findClientConfig"
[@@mel.scope "AutoDiscovery"] [@@mel.module "matrix-js-sdk"]

type client_well_known

external from_discovery_config : client_well_known -> client_config Js.Promise.t
  = "fromDiscoveryConfig"
[@@mel.scope "AutoDiscovery"] [@@mel.module "matrix-js-sdk"]

(* external sendStateEventStatement : *)
(*      client *)
(*   -> Common.room_id *)
(*   -> string (1* event type *1) *)
(*   -> [%mel.obj: < objects : string array > ] (1* can be anything *1) *)
(*   -> string (1* state key *1) *)
(*   -> string Js.Promise.t = "sendStateEvent" *)
(*   [@@mel.send] *)

(* external sendStateEventType : *)
(*      client *)
(*   -> Common.room_id *)
(*   -> string (1* event type *1) *)
(*   -> [%mel.obj: < _type : string > ] (1* can be anything *1) *)
(*   -> string (1* state key *1) *)
(*   -> string Js.Promise.t = "sendStateEvent" *)
(*   [@@mel.send] *)

(* external sendStateEventId : *)
(*      client *)
(*   -> Common.room_id *)
(*   -> string (1* event type *1) *)
(*   -> [%mel.obj: < id : string > ] (1* can be anything *1) *)
(*   -> string (1* state key *1) *)
(*   -> string Js.Promise.t = "sendStateEvent" *)
(*   [@@mel.send] *)

type sync_data =
  < error: string Js.Nullable.t
  ; oldSyncToken: string Js.Nullable.t
  ; nextSyncToken: string Js.Nullable.t
  ; catchingUp: bool Js.Nullable.t
  ; fromCache: bool Js.Nullable.t >
  Js.t

external on :
     client
  -> ([ `timeline of
           Models.matrix_event
        -> Models.room Js.Nullable.t
        -> bool (* toStartOfTimeLine *)
        -> bool (* removed *)
        -> data
        -> unit
        [@mel.as "Room.timeline"]
      | `room_accountdata of
        Models.matrix_event -> Models.room -> Models.matrix_event -> unit
        [@mel.as "Room.accountData"]
      | `room_redaction of Models.matrix_event -> Models.room -> unit
        [@mel.as "Room.redaction"]
      | `sync of string -> string -> sync_data -> unit [@mel.as "sync"]
      | `room of Models.room -> unit [@mel.as "Room"] ]
     [@mel.string] )
  -> event_emitter = "on"
[@@mel.send]

external off :
     client
  -> ([ `timeline of
           Models.matrix_event
        -> Models.room Js.Nullable.t
        -> bool
        -> bool
        -> data
        -> unit
        [@mel.as "Room.timeline"] ]
     [@mel.string] )
  -> event_emitter = "off"
[@@mel.send]

external once :
     client
  -> ([ `sync of
           string (* state *)
        -> string (* prevState *)
        -> string (* data *)
        -> unit
      | `logged_out of string -> unit [@mel.as "Session.logged_out"] ]
     [@mel.string] )
  -> event_emitter = "once"
[@@mel.send]

external create_client :
  < baseUrl: string ; timelineSupport: bool ; store: store > Js.t -> client
  = "createClient"
[@@mel.module "matrix-js-sdk"]

external get_http_uri_for_mxc :
     string
  -> ?mxc:string
  -> ?width:int Js.Undefined.t
  -> ?height:int Js.Undefined.t
  -> ?resize_method:string Js.Undefined.t
  -> ?allow_direct_links:bool
  -> unit
  -> string = "getHttpUriForMxc"
[@@mel.module "matrix-js-sdk"]

external create_client_params :
     < baseUrl: string
     ; userId: string
     ; accessToken: string
     ; timelineSupport: bool
     ; store: store >
     Js.t
  -> client = "createClient"
[@@mel.module "matrix-js-sdk"]

module type CustomEventContent = sig
  type t
end

module MakeEventAccessors (S : CustomEventContent) = struct
  type event =
    < getStateKey: unit -> string [@mel.meth]
    ; getContent: unit -> S.t [@mel.meth] >
    Js.t

  external send_message :
    client -> string -> S.t -> < event_id: string > Js.t Js.Promise.t
    = "sendMessage"
  [@@mel.send]

  external send_event :
    client -> string -> string -> S.t -> < event_id: string > Js.t Js.Promise.t
    = "sendEvent"
  [@@mel.send]

  external get_content : Models.matrix_event -> S.t = "getContent" [@@mel.send]

  external get_state_events : Models.room_state -> string -> event array
    = "getStateEvents"
  [@@mel.send]

  external get_state_event :
    Models.room_state -> string -> string -> event Js.Nullable.t
    = "getStateEvents"
  [@@mel.send]

  let get_state_event_exn event type_ state_key =
    get_state_event event type_ state_key
    |> Js.Nullable.toOption |> Belt.Option.getUnsafe

  external send_state_event :
       client
    -> Common.room_id
    -> string (* event type *)
    -> S.t (* can be anything *)
    -> string (* state key *)
    -> string Js.Promise.t = "sendStateEvent"
  [@@mel.send]
end
