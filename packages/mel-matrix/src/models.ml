(* type event_content = < body: string ; msgtype: string > Js.t *)
type event_content = string Js.Dict.t

type dict_event =
  {content: string Js.Dict.t; state_key: string; type': string [@mel.as "type"]}

type tag

type re_emitter

type account_data

type room_summary =
  < info: < title: string > Js.t ; roomId: Common.room_id > Js.t

type matrix_event =
  < error:
      string Js.Nullable.t
      (* ; event : event *)
      (* should not be used *)
  ; forwardLooking: bool
  ; sender: Room_member.t
  ; status: string Js.Nullable.t
  ; target: string Js.Nullable.t
  ; replyEventId: string Js.Undefined.t
  ; isRedaction: unit -> bool [@mel.meth]
  ; isRelation: string Js.Undefined.t -> unit -> bool [@mel.meth]
  ; getReplyEventId: unit -> Common.event_id [@mel.meth]
  ; getRelation: unit -> Event_relation.event_relation Js.Nullable.t [@mel.meth]
  ; getThread: unit -> thread Js.Undefined.t [@mel.meth]
  ; getId: unit -> Common.event_id [@mel.meth]
  ; getSender: unit -> Common.user_id [@mel.meth]
  ; getType: unit -> string [@mel.meth]
  ; getWireType: unit -> string [@mel.meth]
  ; getRoomId: unit -> Common.room_id [@mel.meth]
  ; getTs: unit -> int [@mel.meth]
  ; getDate: unit -> Js.Date.t [@mel.meth]
  ; getOriginalContent: unit -> event_content [@mel.meth]
  ; getContent: unit -> event_content [@mel.meth]
  ; getWireContent: unit -> event_content [@mel.meth]
  ; getPrevContent: unit -> event_content [@mel.meth]
  ; getAge: unit -> int [@mel.meth]
  ; getLocalAge: unit -> int [@mel.meth]
  ; getStateKey: unit -> string Js.Undefined.t [@mel.meth]
  ; isState: unit -> bool [@mel.meth] >
  Js.t

and thread =
  < (* lastReply: (Event.event -> bool) -> Event.event Js.Undefined.t [@mel.meth] *)
  id: string
  ; getUnfilteredTimelineSet: unit -> event_timeline_set [@mel.meth]
  ; liveTimeline: event_timeline >
  Js.t

and event_timeline =
  < getEvents: unit -> matrix_event array [@mel.meth]
  ; getState:
         (* [`forwards [@mel.as "f"] | `backwards [@mel.as "b"]] *)
         string
      -> room_state Js.Undefined.t
        [@mel.meth] >
  Js.t

and event_timeline_set =
  < thread: thread Js.Undefined.t
  ; getLiveTimeline: unit -> event_timeline [@mel.meth] >
  Js.t

and room =
  < accountData: account_data
  ; getAccountData: string -> matrix_event Js.Undefined.t [@mel.meth]
  ; getType: unit -> string Js.Undefined.t [@mel.meth]
  ; currentState: room_state
  ; myUserId: Common.user_id
  ; name: string
  ; relations: relations_container
  ; createThreadsTimelineSets: unit -> unit Js.Promise.t [@mel.meth]
  ; fetchRoomThreads: unit -> unit Js.Promise.t
        [@mel.meth]
        (* ; processThreadedEvents: matrix_event array -> unit [@mel.meth] *)
  ; createThread: string -> matrix_event -> matrix_event array -> thread
        [@mel.meth]
  ; oldState: room_state
  ; reEmitter: re_emitter
  ; roomId: Common.room_id
  ; storageToken: string Js.Undefined.t
  ; summary: room_summary
  ; tags: tag Js.Dict.t
  ; maySendMessage: unit -> bool [@mel.meth]
  ; timeline: matrix_event array
  ; getUnfilteredTimelineSet: unit -> event_timeline_set [@mel.meth]
  ; getLiveTimeline: unit -> event_timeline [@mel.meth]
  ; findEventById: Common.event_id -> matrix_event Js.Undefined.t [@mel.meth]
  ; getAvatarUrl: string -> int -> int -> string -> string Js.Nullable.t
        [@mel.meth]
        (* getAvatarUrl(baseUrl, width, height, resizeMethod, allowDefault?, allowDirectLinks): null | string *)
  ; getCanonicalAlias: unit -> Common.room_alias Js.Nullable.t [@mel.meth]
  ; getAltAliases: unit -> Common.room_alias array [@mel.meth] >
  Js.t

and relations =
  < getRelations: unit -> matrix_event [@mel.meth]
  ; getSortedAnnotationsByKey: unit -> (string * matrix_event Set.t) array
        [@mel.meth] >
  Js.t

and relations_container =
  < getChildEventsForEvent:
      string -> string -> string -> relations Js.Undefined.t
        [@mel.meth] >
  Js.t

and room_state =
  < getStateEvents: string -> matrix_event array [@mel.meth] > Js.t
