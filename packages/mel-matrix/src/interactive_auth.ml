type session_id = string

type stage = string

type flow = {stages: stage array}

type recaptcha_params = {public_key: string}

type recaptcha_response = {type': string [@mel.as "type"]; response: string}

type policy = {name: string; url: string}

(* type policies = {privacy_policy: policy Js.Dict.t} *)

type terms_params = {policies: policy Js.Dict.t Js.Dict.t}
(* type terms_params = {policies: policies} *)

type terms_response = {type': string [@mel.as "type"]}

type flow_params =
  { recaptcha: recaptcha_params Js.Undefined.t [@mel.as "m.login.recaptcha"]
  ; terms: terms_params Js.Undefined.t [@mel.as "m.login.terms"] }

type auth_data =
  { completed: string array Js.Undefined.t
  ; session: session_id Js.Undefined.t
  ; flows: flow array Js.Undefined.t
  ; params: flow_params Js.Undefined.t }

type inputs =
  { emailAddress: string option [@mel.optional]
  ; phoneCountry: string option [@mel.optional]
  ; phoneNumber: string option [@mel.optional]
  ; registrationToken: string option [@mel.optional] }
[@@deriving jsProperties]

type auth_type = string

(* would be nice if we could do something like: *)

(* | Password [@mel.as "m.login.password"] *)
(* | Recaptcha [@mel.as "m.login.recaptcha"] *)
(* | Terms [@mel.as "m.login.terms"] *)
(* | Email [@mel.as "m.login.email.identity"] *)
(* | Msisdn [@mel.as "m.login.msisdn"] *)
(* | Sso [@mel.as "m.login.sso"] *)
(* | SsoUnstable [@mel.as "org.matrix.login.sso"] *)
(* | Dummy [@mel.as "m.login.dummy"] *)
(* | RegistrationToken [@mel.as "m.login.registration_token"] *)

type stage_status =
  { emailSid: string Js.Undefined.t
  ; errcode: string Js.Undefined.t
  ; error: string Js.Undefined.t }

type stage_params

type 'response interactive_auth_options =
  { matrixClient: Client.client
  ; authData: auth_data option [@mel.optional]
  ; inputs: inputs option [@mel.optional]
  ; sessionId: session_id option [@mel.optional]
  ; clientSecret: string option [@mel.optional]
  ; emailSid: string option [@mel.optional]
  ; supportedStages: auth_type array option [@mel.optional]
  ; doRequest: Client.auth_dict Js.Null.t -> 'response Js.Promise.t
  ; stateUpdated: auth_type -> stage_status -> unit
  ; requestEmailToken:
         string
      -> string
      -> int
      -> session_id
      -> Client.request_token_response Js.Promise.t
  ; busyChanged: (bool -> unit) option [@mel.optional] }
[@@deriving jsProperties]

type 'response interactive_auth =
  < attemptAuth: unit -> 'response Js.Promise.t [@mel.meth]
  ; getChosenFlow: unit -> flow [@mel.meth]
  ; getClientSecret: unit -> string [@mel.meth]
  ; getEmailSid: unit -> string [@mel.meth]
  ; getSessionId: unit -> session_id [@mel.meth]
  ; getStageParams: string -> stage_params [@mel.meth]
  ; poll: unit -> unit Js.Promise.t [@mel.meth]
  ; requestEmailToken: unit -> unit Js.Promise.t [@mel.meth]
  ; setEmailSid: string -> unit [@mel.meth]
  ; submitAuthDict: Client.auth_dict -> bool -> unit [@mel.meth] >
  Js.t

external get_params_recaptcha :
     _ interactive_auth
  -> (_[@mel.as "m.login.recaptcha"])
  -> unit
  -> recaptcha_params = "getStageParams"
[@@mel.send]

external get_params_terms :
  _ interactive_auth -> (_[@mel.as "m.login.terms"]) -> unit -> terms_params
  = "getStageParams"
[@@mel.send]

external new_interactive_auth :
  'response interactive_auth_options -> 'response interactive_auth
  = "InteractiveAuth"
[@@mel.module "matrix-js-sdk/src/interactive-auth"] [@@mel.new]

type matrix_error =
  { (* {httpStatus: int; errcode: string Js.Undefined.t; data: flow_data} *)
    httpStatus: int
  ; flows: flow array Js.Undefined.t
  ; errcode: string Js.Undefined.t }
