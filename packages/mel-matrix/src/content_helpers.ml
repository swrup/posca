external parse_topic_content :
  Models.event_content -> < text: string ; html: string Js.Undefined.t > Js.t
  = "parseTopicContent"
[@@mel.scope "ContentHelpers"] [@@mel.module "matrix-js-sdk"]
