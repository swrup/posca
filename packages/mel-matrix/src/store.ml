type user

type storage

type group

type filter

type account_data

type t =
  < accountData: account_data
  ; filters: filter Js.Dict.t
  ; groups: group Js.Dict.t
  ; localStorage: storage
  ; startup: unit -> unit Js.Promise.t [@mel.meth]
  ; destroy: unit -> unit [@mel.meth]
  ; rooms: Models.room Js.Dict.t
  ; users: user Js.Dict.t >
  Js.t
