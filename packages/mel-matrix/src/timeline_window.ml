type t =
  < getEvents: unit -> Models.matrix_event array [@mel.meth]
  ; load: string Js.Undefined.t -> unit Js.Promise.t [@mel.meth]
  ; paginate: string -> int -> bool Js.Promise.t [@mel.meth]
  ; timelineSet: Models.event_timeline_set >
  Js.t

external create : Client.client -> Models.event_timeline_set -> t
  = "TimelineWindow"
[@@mel.new] [@@mel.module "matrix-js-sdk"]
