type matrix_event2

type t =
  < events: matrix_event2 Js.Dict.t
  ; membership: string
  ; name: string
  ; powerLevel: int
  ; powerLevelNorm: int
  ; rawDisplayName: string
  ; getAvatarUrl: string -> int -> int -> string -> string Js.Nullable.t
        [@mel.meth]
        (* getAvatarUrl(baseUrl, width, height, resizeMethod, allowDefault?, allowDirectLinks): null | string *)
  ; roomId: Common.room_id
  ; typing: bool
  ; user: string Js.Null.t
  ; userId: Common.user_id >
  Js.t
