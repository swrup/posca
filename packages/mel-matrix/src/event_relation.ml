type event_id_part =
  { (* { event_id: Common.event_id Js.Undefined.t } *)
    (* { rel_event_id: Common.event_id [@mel.as "event_id"] } *)
    event_id: Common.event_id }

type event_relation =
  { event_id: Common.event_id Js.Undefined.t
  ; is_falling_back: bool Js.Undefined.t
  ; key: string Js.Undefined.t
  ; in_reply_to: event_id_part Js.Undefined.t [@mel.as "m.in_reply_to"]
  ; subthread: event_id_part Js.Undefined.t [@mel.as "pm.imago.subthread"]
  ; rel_type: string Js.Undefined.t }
