(* Type corresponding to JS Set *)

type 'a t = {size: int}

let to_array : 'a t -> 'a array = fun set -> [%raw {|[...set]|}]
