import { defineConfig } from 'vite'

// import nodePolyfills from "rollup-plugin-polyfill-node";
import { nodePolyfills } from 'vite-plugin-node-polyfills';

import path from 'path'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons-next'

import melangePlugin from 'vite-plugin-melange'

import { VitePWA } from 'vite-plugin-pwa'

export default defineConfig({
  build: {
    rollupOptions: {
      output: {
        manualChunks(id) {
          if (id.includes('node_modules')) {
            return id
              .toString()
              .split('node_modules/')[1]
              .split('/')[0]
              .toString()
          }
        },
      },
    },
  },
  plugins: [
    melangePlugin({
      emitDir: "src",
      buildCommand: "make build",
      watchCommand: "make watch",
    }),
    nodePolyfills({
      include: ['buffer', 'process'],
      globals: {
        Buffer: true,
        global: true,
        process: true,
      },
      protocolImports: false,
    }),
    createSvgIconsPlugin({
      // Specify the icon folder to be cached
      iconDirs: [path.resolve(process.cwd(), 'node_modules/bytesize-icons/dist/icons'), path.resolve(process.cwd(), 'icons')],
      // Specify symbolId format
      symbolId: 'icon-[name]',
    }),
    VitePWA({
      includeAssets: ['favicon.ico', 'apple-touch-icon.png', 'mask-icon.svg'],
      manifest: {
        name: 'Posca',
        short_name: 'Posca',
        description: 'A Matrix-based social network',
        theme_color: '#122149',
        background_color: '#122149',
        icons: [
          {
            src: 'pwa-64x64.png',
            sizes: '64x64',
            type: 'image/png'
          },
          {
            src: 'pwa-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'pwa-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          },
          {
            src: 'maskable-icon-512x512.png',
            sizes: '512x512',
            type: 'image/png',
            purpose: 'maskable'
          }
        ]
      }
    })
  ],
  worker: {
    plugins: () => [
      melangePlugin({
        emitDir: "src",
        buildCommand: "make build",
        watchCommand: "make watch",
      })
    ]
  },
  server: {
    host: '0.0.0.0',
    watch: {
      awaitWriteFinish: {
        stabilityThreshold: 500,
        pollInterval: 20
      }
    }
  },
})
