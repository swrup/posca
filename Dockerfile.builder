FROM ocaml/opam:ubuntu-23.04-ocaml-4.14

RUN sudo apt-get -y update

RUN sudo apt-get -y install npm

COPY --chown=opam Makefile posca.opam package.json package-lock.json /home/opam/

RUN make init

ENV OPAMSWITCH=/home/opam

ENV NPM_CONFIG_PREFIX=/home/opam
