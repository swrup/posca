type model = {sync_state: Matrix.Client.sync_state option}

let init () = {sync_state= None}

type msg =
  | GoTo of Router.route
  | ToggleMenu
  | SetSync of Matrix.Client.sync_state option
[@@deriving accessors]

let update model = function
  | GoTo _route ->
      (model, Tea.Cmd.none)
  | ToggleMenu ->
      (* should not happen *)
      (model, Tea.Cmd.none)
  | SetSync sync_state ->
      ({sync_state}, Tea.Cmd.none)

let sync_state_view sync_state =
  let sync_state_class =
    match sync_state with
    | Some sync_state -> (
      match sync_state with
      | `CATCHUP | `PREPARED | `RECONNECTING ->
          "border-b-icon-red"
      | `SYNCING ->
          "hidden"
      | `ERROR | `STOPPED ->
          "border-icon-red" )
    | None ->
        "border-paper-white border-b-beware-orange"
  in
  let open Tea.Html in
  let open Tea.Html.Attributes in
  li
    [ class'
        ( "my-auto h-8 w-8 animate-spin rounded-full border-8 "
        ^ sync_state_class ) ]
    []

let view model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let logo =
    li
      [class' "relative m-0 flex grow flex-col justify-center"]
      [ Router.link goTo Index
          ~props:[class' "flex flex-row justify-center p-0 md:justify-start"]
          [ img
              [ class' "m-0 h-10"
              ; src "/logo_posca.svg"
              ; Tea.Html.Attributes.alt "Posca" ]
              [] ] ]
  in
  let hamburger_button =
    Components.button_link ~icon_only:true ~msg:ToggleMenu ~icon:"main-menu"
      ~mobile_icon_only:true
      ~classes:"w-8 justify-center !p-0 md:w-[60px] md:!px-5 md:!py-2.5"
      (T.header_toggle_menu ())
  in
  let login_button =
    Components.button_link ~route:(goTo, Login) ~icon:"sign-in"
      (T.header_login ())
  in
  let register_button =
    Components.button_link ~route:(goTo, Register) ~icon:"home"
      (T.login_register_button ())
  in
  let profile_button =
    let username =
      match Matrix.current_user_name Matrix.client with
      | Some name ->
          name
      | None ->
          "Not connected"
    in
    let icon, image =
      let homeserver_url = Matrix.client##getHomeserverUrl () in
      match
        Matrix.current_user ()
        |. Belt.Option.flatMap (function user ->
               user##avatarUrl |> Js.Undefined.toOption )
        |. Belt.Option.flatMap (function avatar_url ->
               Some
                 (Matrix_sdk.Client.get_http_uri_for_mxc homeserver_url
                    ~mxc:avatar_url ~width:(Js.Undefined.return 100)
                    ~height:(Js.Undefined.return 100)
                    ~resize_method:(Js.Undefined.return "scale")
                    ~allow_direct_links:true () ) )
      with
      | Some url ->
          ( None
          , Some
              (img
                 [ class'
                     "mr-2 inline-block h-6 w-6 rounded-full border-2 \
                      border-contrast-dark text-right"
                 ; src url ]
                 [] ) )
      | None ->
          (Some "user", None)
    in
    Components.button_link ~route:(goTo, Index) ?icon ?image username
  in
  let settings_button =
    Components.button_link ~route:(goTo, Settings) ~icon:"settings"
      (T.header_settings ())
  in
  let logout_button =
    Components.button_link ~msg:(GoTo Logout) ~icon:"sign-out"
      (T.header_logout ())
  in
  let overflow_button items =
    Components.ClickDropdown.element "md:py-2.5mr-2 !p-0 pl-3 md:hidden md:px-5"
      ~items ~button_icon:"ellipsis-vertical" ~button_label:"More"
  in
  let left_buttons =
    [hamburger_button; logo; sync_state_view model.sync_state]
  in
  let right_buttons =
    if Auth.is_logged_in () && not (Auth.is_guest ()) then
      [profile_button; settings_button; logout_button]
    else [login_button; register_button; settings_button]
  in
  let all_buttons =
    left_buttons
    |. Belt.List.concat
         (Belt.List.map right_buttons (fun item ->
              li [class' "clear-both m-0 hidden md:flex"] [item] ) )
    |. Belt.List.concat
         [ li
             [class' "flex content-center items-center justify-center"]
             [overflow_button right_buttons] ]
  in
  header
    [ class'
        "mx-4 h-14 shrink-0 items-center border-b-4 border-contrast-dark \
         bg-cardboard-gray text-lg text-dominant-dark dark:bg-dominant-dark \
         dark:text-cardboard-gray md:mx-0 md:h-16" ]
    [ nav
        [class' "h-full"]
        [ul [class' "m-0 flex h-full flex-row items-stretch p-0"] all_buttons]
    ]
