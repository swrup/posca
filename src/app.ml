open Router

type msg =
  | AuthMsg of Auth.msg
  | ContentMsg of Content.msg
  | HeaderMsg of Header.msg
  | SidebarMsg of Sidebar.msg
  | LocationChanged of Tea.Navigation.Location.t
  | GoTo of route (* | Logout *)
  | GotSync of string * string * Matrix_sdk.Client.sync_data
  | GotRoom of Matrix.room
  | ResetUpdatedClient
  | StartedClient of (unit, string) result
  | PromptRefresh
  | Refresh
  | CancelPrompt
[@@deriving accessors]

let msg_to_string (msg : msg) =
  match msg with
  | AuthMsg authMsg ->
      "auth msg: " ^ Auth.msg_to_string authMsg
  | ContentMsg contentMsg ->
      Content.msg_to_string contentMsg
  | HeaderMsg _headerMsg ->
      "header msg"
  | SidebarMsg _sidebarMsg ->
      "sidebar msg"
  | LocationChanged _ ->
      "location changed"
  | GoTo _ ->
      "go to"
  | GotSync _ ->
      "got Sync"
  | GotRoom _ ->
      "got Room"
  | ResetUpdatedClient ->
      "reset updated client"
  | StartedClient _ ->
      "started client"
  | PromptRefresh ->
      "prompt refresh"
  | Refresh ->
      "refresh"
  | CancelPrompt ->
      "cancel prompt"

(* | Logout -> "logout" *)

type prompt = RefreshPrompt | NoPrompt

type model =
  { content: Content.model
  ; sidebar: Sidebar.model
  ; header: Header.model
  ; route: Router.route
  ; pwa_refresh: unit -> unit
  ; prompt: prompt }

let update_route model = function
  | route when model.route = route ->
      (model, Tea.Cmd.none)
  (* | AuthRoute auth_route -> *)
  (*     let auth, route = Auth.update_route model.auth auth_route in *)
  (*     {auth; route}, location_of_route route |> Tea.Navigation.newUrl *)
  | Room (room_address, maybe_event) as route ->
      let chat_cmd =
        Rooms.set_route model.content.rooms (room_address, maybe_event)
      in
      ( {model with route}
      , Tea.Cmd.map (fun m -> contentMsg (Content.roomsMsg m)) chat_cmd )
  | CreateRoom (display, space) as route ->
      ( {model with route}
      , Tea.Cmd.msg @@ ContentMsg (CreateRoomMsg (SetParams (display, space)))
      )
  | route ->
      ({model with route}, Tea.Cmd.none)

let update_route_and_navigate model = function
  | route when model.route = route ->
      (model, Tea.Cmd.none)
  | route ->
      Router.set_title_for_route route ;
      let updated_model, cmd = update_route model route in
      ( updated_model
      , Tea.Cmd.batch [cmd; location_of_route route |> Tea.Navigation.newUrl] )

let init () location =
  Matrix.set_client @@ Matrix.create_client true ;
  let auth_cmd = Auth.init () in
  let model =
    { content= Content.init ()
    ; sidebar= Sidebar.init ()
    ; header= Header.init ()
    ; route= Index
    ; pwa_refresh= Pwa.register ()
    ; prompt= NoPrompt }
  in
  (* Js.log (route_of_location location) ; *)
  let model, location_cmd = route_of_location location |> update_route model in
  (* let auth_cmd = Auth.init_cmd in *)
  let cmd = Tea.Cmd.batch [Tea.Cmd.map authMsg auth_cmd; location_cmd] in
  (model, cmd)

let start_client_cmd () =
  Matrix.start_client () |. Tea_promise.result startedClient

let update model = function
  (* | Logout -> *)
  (*     {model with auth = Auth.logout model.auth}, Tea.Cmd.none *)
  | LocationChanged location ->
      (* Js.log "LocationChanged" ; *)
      (* ({model with route= route_of_location location}, Tea.Cmd.none) *)
      route_of_location location |> update_route model
  | GoTo route ->
      (* Js.log "GoTo" ; *)
      update_route_and_navigate model route
  | SidebarMsg (GoTo route) ->
      let model, cmd = update_route_and_navigate model route in
      let model =
        if Html.is_mobile () then
          {model with sidebar= {model.sidebar with show_menu= false}}
        else model
      in
      (model, cmd)
  | SidebarMsg sidebar_msg ->
      let sidebar, sidebar_cmd = Sidebar.update model.sidebar sidebar_msg in
      ({model with sidebar}, Tea.Cmd.map sidebarMsg sidebar_cmd)
  | HeaderMsg (GoTo Logout) ->
      (model, Tea.Cmd.msg @@ AuthMsg Logout)
  | HeaderMsg (GoTo route) ->
      update_route_and_navigate model route
  | HeaderMsg ToggleMenu ->
      let sidebar =
        {model.sidebar with show_menu= not model.sidebar.show_menu}
      in
      let model = {model with sidebar} in
      (model, Tea.Cmd.none)
  | HeaderMsg (SetSync _) ->
      (model, Tea.Cmd.none)
  (* | HeaderMsg header_msg -> *)
  (*     let header, header_cmd = Header.update model.header header_msg in *)
  (*     ({model with header}, Tea.Cmd.map headerMsg header_cmd) *)
  | ContentMsg (GoTo route) ->
      update_route_and_navigate model route
  | ContentMsg content_msg ->
      let content, content_cmd = Content.update model.content content_msg in
      ({model with content}, Tea.Cmd.map contentMsg content_cmd)
  | AuthMsg (GoTo route) ->
      update_route_and_navigate model route
  | AuthMsg auth_msg ->
      let auth_cmd = Auth.update auth_msg in
      (model, Tea.Cmd.map authMsg auth_cmd)
  | GotSync (state, prev_state, data) ->
      (* Js.log state ; *)
      let sync_state = Matrix_sdk.Client.sync_stateFromJs state in
      let header, _header_cmd =
        Header.update model.header (SetSync sync_state)
      in
      ({model with header}, Tea.Cmd.none)
  | GotRoom room ->
      let sidebar, _sidebar_cmd = Sidebar.update model.sidebar (AddRoom room) in
      let rooms, rooms_cmd = Rooms.update model.content.rooms (AddRoom room) in
      let content = {model.content with rooms} in
      ( {model with sidebar; content}
      , rooms_cmd |> Tea.Cmd.map Content.roomsMsg |> Tea.Cmd.map contentMsg )
  | ResetUpdatedClient ->
      let () = Matrix.reset_updated_client () in
      (model, start_client_cmd ())
  | StartedClient (Ok ()) ->
      let () = Js.log "started client" in
      (model, Tea.Cmd.none)
  | StartedClient (Error err) ->
      let () = Js.log err in
      (model, Tea.Cmd.none)
  | PromptRefresh ->
      ({model with prompt= RefreshPrompt}, Tea.Cmd.none)
  | Refresh ->
      let () = model.pwa_refresh () in
      ({model with prompt= NoPrompt}, Tea.Cmd.none)
  | CancelPrompt ->
      ({model with prompt= NoPrompt}, Tea.Cmd.none)

let prompt_view question buttons =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  div
    [class' "left-8 top-8 dark:bg-dominant-dark dark:text-cardboard-gray"]
    [ div
        [class' "mx-auto w-fit"]
        [ p []
            [ text question
            ; buttons
              (* ; Components.rpill_button  *)
              (* ; button [] [text "Cancel"] *) ] ] ]

let view model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  let prompt_view () =
    match model.prompt with
    | RefreshPrompt ->
        prompt_view
          "The application has been updated. Refresh to get the new version."
          (div
             [class' "mt-2"]
             [ button [class' "border p-2"; onClick refresh] [text "Refresh"]
             ; button
                 [class' "ml-4 border p-2"; onClick cancelPrompt]
                 [text "Cancel"] ] )
    | NoPrompt ->
        noNode
  in
  div
    [ id "body"
    ; classList
        [ ("dark", model.content.settings.color_theme == Dark)
        ; ("light", model.content.settings.color_theme == Light)
        ; ("flex h-screen flex-col bg-dominant-dark", true) ] ]
    [ prompt_view ()
    ; Header.view model.header |> Vdom.map headerMsg
    ; main
        [ class'
            "flex min-h-0 grow flex-row bg-cardboard-gray text-dominant-dark \
             dark:bg-dominant-dark dark:text-cardboard-gray" ]
        [ Sidebar.view model.route model.sidebar |> Vdom.map sidebarMsg
        ; Content.view model.route model.content |> Vdom.map contentMsg ]
    ; footer ~key:"footer"
        [ class'
            "hidden shrink-0 bg-cardboard-gray p-4 text-dominant-dark \
             dark:bg-dominant-dark dark:text-cardboard-gray" ]
        [text "Posca 2023"] ]

let immediate_msg_sub name tagger =
  let open Vdom in
  let enableCall callbacks =
    let _ = callbacks.enqueue tagger in
    fun () -> ()
  in
  Tea_sub.registration name enableCall

let subscriptions model =
  (* Js.log "SUBSCRIBING" ; *)
  let non_matrix_subs =
    [Html.registerGlobal "sw_refresh" "sw_refresh" (fun _event -> PromptRefresh)]
  in
  let matrix_subs =
    [ Auth.subscriptions () |> Tea.Sub.map authMsg
    ; Register.subscriptions model.content.register
      |> Tea.Sub.map (fun m -> contentMsg (Content.registerMsg m))
    ; Matrix.subscribe_on_sync gotSync
    ; Matrix.subscribe_on_room gotRoom
    ; Rooms.subscriptions model.content.rooms
      |> Tea.Sub.map (fun m -> contentMsg (Content.roomsMsg m)) ]
  in
  let subs =
    if Matrix.updated_client then
      Belt.List.concat non_matrix_subs
        [immediate_msg_sub "reset_updated_client" resetUpdatedClient]
    else Belt.List.concat non_matrix_subs matrix_subs
  in
  (* Js.log @@ Tablecloth.List.to_array subs ; *)
  Tea.Sub.batch subs

let shutdown _model = Tea.Cmd.none

let start_app container =
  Tea.Navigation.navigationProgram locationChanged
    {init; update; view; subscriptions; shutdown}
    container ()

let start_debug_app ?(init = init) ?(shutdown = shutdown) container =
  Tea.Debug.navigationProgram locationChanged
    {init; update; view; subscriptions; shutdown}
    msg_to_string container ()

let start_hot_debug_app container cachedModel =
  Js.log "start" ;
  Js.log cachedModel ;
  (* Replace the existing shutdown function with one that returns the current
   * state of the app, for hot module replacement purposes *)
  (* inspired by https://github.com/walfie/ac-tune-maker *)
  let modelRef = ref None in
  let shutdown model =
    modelRef := Some model ;
    Tea.Cmd.none
  in
  let init =
    match cachedModel with
    | None ->
        init
    | Some model ->
        fun flags location ->
          let _model, cmd = init flags location in
          (model, cmd)
  in
  let app = start_debug_app ~init ~shutdown container in
  let oldShutdown = app##shutdown in
  let newShutdown () = oldShutdown () ; Js.log "shutdown" ; !modelRef in
  let _ = Js.Obj.assign app [%obj {shutdown= newShutdown}] in
  newShutdown
