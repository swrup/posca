let rpill_button ?key ?(icon_only = false) msg icon label' =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  let button_text = if icon_only then noNode else text label' in
  let button_other_class = if icon_only then "rounded-full" else "" in
  button ?key
    [ onClick msg
    ; Icons.aria_label label'
    ; title label'
    ; class'
        ( "bg-[transparent] p-2 duration-300 hover:border-contrast-dark/50 \
           hover:text-contrast-dark/50 focus:border-contrast-dark/50 "
        ^ button_other_class ) ]
    [Icons.icon ~class':"h-5 w-5" icon; span [] [button_text]]

let rpill_link msg route icon label' =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  Router.link
    ~props:[Icons.aria_label label'; class' "button icon rpill"; title label']
    msg route
    [Icons.icon ~class':"h-5 w-5" icon; span [] [text label']]

let button_link ?msg ?route ?icon ?image ?(icon_only = false)
    ?(mobile_icon_only = false) ?(classes = "") label' =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  let additional_link_props =
    match (msg, route) with
    | None, None ->
        [href "#"]
    | Some msg, _ ->
        [onClick msg]
    | _, Some (route_msg, route) ->
        Router.link_props route_msg route
  in
  let link_text =
    span
      [ classList
          [ ("hidden", icon_only)
          ; ("ml-2.5", Belt.Option.isSome icon)
          ; ("hidden md:inline", mobile_icon_only && not icon_only) ] ]
      [text label']
  in
  let link_inner =
    match (icon, image) with
    | Some icon, _ ->
        [Icons.icon ~class':"h-10 w-10" icon; link_text]
    | _, Some image ->
        [image; link_text]
    | _ ->
        [link_text]
  in
  a
    (Belt.List.concat
       [ Icons.aria_label label'
       ; class'
           ( classes
           ^ " align-center hover:bg-contrast-dark/50/25 flex h-full \
              cursor-pointer flex-row items-center px-5 py-2.5" )
       ; title label' ]
       additional_link_props )
    link_inner

let message_pill ?(invert_colors = false) identifier icon amount is_on
    maybe_link =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let bg_class =
    if invert_colors then "bg-dominant-soft-dark" else "bg-dominant-dark"
  in
  let border_class =
    if invert_colors then "border-dominant-dark"
    else "border-dominant-soft-dark"
  in
  let content =
    [ Icons.icon
        ~class':("l-0 w8 h-8 w-8 rounded-full border-2 p-1 " ^ border_class)
        icon
    ; span [class' "py-1 pl-2 pr-5"] [text (string_of_int amount)] ]
  in
  let additional_classes = if is_on then "border-white border-2" else "" in
  match maybe_link with
  | Some msg ->
      a ~unique:identifier
        [ class' @@ "my-1 flex h-fit w-fit cursor-pointer rounded-3xl "
          ^ bg_class ^ " " ^ additional_classes
        ; Tea.Html.Events.onClick msg ]
        content
  | None ->
      div
        [ class' @@ "my-1 flex h-fit w-fit cursor-pointer rounded-3xl "
          ^ bg_class ^ " " ^ additional_classes ]
        content

module ClickDropdown = struct
  type dom_rect =
    < height: int
    ; width: int
    ; top: int
    ; right: int
    ; bottom: int
    ; left: int >
    Js.t

  let opened : Dom.node option ref = ref None

  let toggle_visibility _elem = [%raw {|_elem.classList.toggle('!block')|}]

  let get_rect _elem = [%raw {|_elem.getBoundingClientRect()|}]

  (* let position dropdown btn = *)
  (*   let btn_rect : dom_rect = get_rect btn in *)
  (*   let dropdown_rect : dom_rect = get_rect dropdown in *)
  (*   let ideal_left_pos = btn_rect##right + 10 in *)
  (*   let ideal_top_pos = btn_rect##top - 10 in *)
  (*   let () = *)
  (*     if *)
  (*       ideal_left_pos + [%raw {|dropdown.offsetWidth|}] *)
  (*       < [%raw {|document.documentElement.clientWidth|}] *)
  (*     then *)
  (*       let _ = *)
  (*         Web.Node.setStyleProperty dropdown "left" *)
  (*           (Js.Null.return @@ string_of_int ideal_left_pos ^ "px") *)
  (*       in *)
  (*       Web.Node.setStyleProperty dropdown "right" (Js.Null.return "unset") *)
  (*     else *)
  (*       let _ = *)
  (*         Web.Node.setStyleProperty dropdown "right" (Js.Null.return "0") *)
  (*       in *)
  (*       Web.Node.setStyleProperty dropdown "left" (Js.Null.return "unset") *)
  (*   in *)
  (*   let () = *)
  (*     if *)
  (*       ideal_top_pos + dropdown_rect##height *)
  (*       < [%raw {|document.documentElement.clientHeight|}] *)
  (*     then *)
  (*       let _ = *)
  (*         Web.Node.setStyleProperty dropdown "top" *)
  (*           (Js.Null.return @@ string_of_int ideal_top_pos ^ "px") *)
  (*       in *)
  (*       Web.Node.setStyleProperty dropdown "bottom" (Js.Null.return "unset") *)
  (*     else *)
  (*       let _ = *)
  (*         Web.Node.setStyleProperty dropdown "bottom" (Js.Null.return "0") *)
  (*       in *)
  (*       Web.Node.setStyleProperty dropdown "top" (Js.Null.return "unset") *)
  (*   in *)
  (*   () *)

  let handle_dropdown elem =
    let node = Webapi.Dom.Element.asNode elem in
    let dropdown = Webapi.Dom.Node.lastChild node in
    (* |> Belt.Option.getUnsafe *)
    (* let dropdown = [%raw {|elem.lastChild|}] in *)
    let () = toggle_visibility dropdown in
    opened :=
      match !opened with
      | None ->
          (* let () = position dropdown elem in *)
          dropdown
      | c when c = dropdown ->
          None
      | Some c ->
          let () = toggle_visibility c in
          dropdown

  let handle_click : Dom.event -> unit =
   fun [@bs] event ->
    let target =
      Webapi.Dom.Event.target event |> Webapi.Dom.EventTarget.unsafeAsElement
    in
    let nearest_button = Webapi.Dom.Element.closest ".click-dropdown" target in
    (* let nearest_button : Web.Node.t Js.Nullable.t = *)
    (*   [%raw {|_event.target.closest('.click-dropdown')|}] *)
    (* in *)
    match
      (* (Js.Nullable.toOption nearest_button, Js.Nullable.toOption !opened) *)
      (nearest_button, !opened)
    with
    | Some e, _ ->
        handle_dropdown e
    | _, Some c ->
        let () = toggle_visibility c in
        opened := None
    | _, _ ->
        ()

  let _ =
    let document = Webapi.Dom.document |> Webapi.Dom.Document.asEventTarget in
    Webapi.Dom.EventTarget.addEventListener "click" handle_click document

  let element ?button_text ?button_icon classes ~button_label ~items =
    let open Tea.Html in
    let open Tea.Html.Attributes in
    let item_view item =
      li
        [ class'
            "block cursor-pointer font-normal hover:bg-dominant-dark \
             active:bg-deco-blue" ]
        [item]
    in
    let button_classes =
      match button_text with None -> "h-8 w-8 rounded-full" | Some _ -> ""
    in
    div
      [class' (classes ^ " click-dropdown relative")]
      (* ; Tea.Html2.Attributes.tabindex 0 ] *)
      [ button
          [ class'
              ( "click-dropdown__button button_round flex items-center \
                 justify-center self-center bg-[transparent] p-1 text-center \
                 duration-300 hover:border-contrast-dark/50 \
                 hover:text-contrast-dark/50 focus:border-contrast-dark/50 "
              ^ button_classes )
          ; Icons.aria_label button_label
          ; title button_label
            (* ; Tea.Html.onCB "click" "" (fun event -> *)
            (*   event##stopPropagation () |> ignore; *)
            (*   event##preventDefault () |> ignore; *)
            (*   None *)
            (*   ) *) ]
          [ ( match button_icon with
            | Some icon ->
                Icons.icon icon
            | None ->
                noNode )
          ; (match button_text with Some t -> text t | None -> noNode) ]
      ; ul
          [ class'
              "click-dropdown__menu absolute right-2 top-6 z-50 mt-3 hidden \
               w-auto overflow-hidden rounded-xl border-2 \
               bg-dominant-soft-dark" ]
          (Belt.List.map items item_view) ]
end
