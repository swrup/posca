type on_message

type post_message

type worker

type context

external new_indexed_db_worker : post_message -> worker = "IndexedDBStoreWorker"
[@@mel.new] [@@mel.module "matrix-js-sdk/lib/indexeddb-worker"]

external context : context = "self"

external set_on_message : context -> on_message -> unit = "onmessage"
[@@mel.set]

external get_on_message : worker -> on_message = "onMessage" [@@mel.get]

external get_post_message : context -> post_message = "postMessage" [@@mel.get]

let remote_worker = new_indexed_db_worker (get_post_message context)

let () = set_on_message context (get_on_message remote_worker)
