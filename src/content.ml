type model =
  { rooms: Rooms.model
  ; login: Login.model
  ; register: Register.model
  ; settings: Settings.model
  ; create_chat: Create_room.model }

type msg =
  | RoomsMsg of Rooms.msg
  | LoginMsg of Login.msg
  | RegisterMsg of Register.msg
  | SettingsMsg of Settings.msg
  | CreateRoomMsg of Create_room.msg
  | GoTo of Router.route
[@@deriving accessors]

let msg_to_string = function
  | RoomsMsg msg ->
      "room msg: " ^ Rooms.msg_to_string msg
  | LoginMsg _msg ->
      "login msg" (*Login.msg_to_string msg*)
  | RegisterMsg msg ->
      Register.msg_to_string msg
  | SettingsMsg _msg ->
      "settings msg"
  | CreateRoomMsg _msg ->
      "create chat msg"
  | GoTo _msg ->
      "goto"

let init () =
  { rooms= Rooms.init ()
  ; login= Login.init ()
  ; register= Register.init ()
  ; settings= Settings.init ()
  ; create_chat= Create_room.init () }

let update model = function
  | RoomsMsg (GoTo route) ->
      (model, Tea.Cmd.msg (GoTo route))
  | RoomsMsg chat_msg ->
      let rooms, rooms_cmd = Rooms.update model.rooms chat_msg in
      ({model with rooms}, Tea.Cmd.map roomsMsg rooms_cmd)
  | LoginMsg (GoTo route) ->
      (model, Tea.Cmd.msg (GoTo route))
  | LoginMsg login_msg ->
      let login, login_cmd = Login.update model.login login_msg in
      ({model with login}, Tea.Cmd.map loginMsg login_cmd)
  | GoTo _ ->
      (* this should never match *)
      (model, Tea.Cmd.none)
  | RegisterMsg (GoTo route) ->
      (model, Tea.Cmd.msg (GoTo route))
  | RegisterMsg register_msg ->
      let register, register_cmd =
        Register.update model.register register_msg
      in
      ({model with register}, Tea.Cmd.map registerMsg register_cmd)
  | SettingsMsg (GoTo route) ->
      (model, Tea.Cmd.msg (GoTo route))
  | SettingsMsg settings_msg ->
      let settings, settings_cmd =
        Settings.update model.settings settings_msg
      in
      ({model with settings}, Tea.Cmd.map settingsMsg settings_cmd)
  | CreateRoomMsg (GoTo route) ->
      (model, Tea.Cmd.msg (GoTo route))
  | CreateRoomMsg create_chat_msg ->
      let create_chat, create_chat_cmd =
        Create_room.update model.create_chat create_chat_msg
      in
      ({model with create_chat}, Tea.Cmd.map createRoomMsg create_chat_cmd)

let view (route : Router.route) model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  div
    [id "content"; class' "grow overflow-hidden"]
    [ ( match route with
      | Index ->
          Index_view.view model
      | Login ->
          Login.view model.login |> Vdom.map loginMsg
      | Register ->
          Register.view model.register |> Vdom.map registerMsg
      | Logout ->
          div [] []
      | Settings ->
          Settings.view model.settings |> Vdom.map settingsMsg
      | Room (_room_address, _maybe_event) ->
          Rooms_view.view model.rooms |> Vdom.map roomsMsg
      | CreateRoom (room_type, maybe_space_id) ->
          Create_room.view model.create_chat room_type maybe_space_id
          |> Vdom.map createRoomMsg ) ]
