external show_modal_unsafe : Dom.element option -> unit = "showModal"
[@@mel.send]

external close_modal_unsafe : Dom.element option -> unit = "close" [@@mel.send]

external focus : Dom.element option -> unit = "focus" [@@mel.send]

let on_key ?(key = "") test_key msg =
  Tea.Html.Events.onCB "keydown" ~key (fun event ->
      if test_key event then
        let () = Webapi.Dom.Event.preventDefault event in
        Some msg
      else None )
(* let target = Webapi.Dom.Event.target event in *)
(* match Webapi.Dom.EventTarget.unsafeAsElement target |> Webapi.Dom.HtmlInputElement.ofElement with *)
(* | Some _ ->  *)
(*         if test_key event then *)
(*           let () = Webapi.Dom.Event.preventDefault event in *)
(*           Some msg *)
(*         else None *)
(* | None -> None) *)
(* let value = Webapi.Dom.HtmlInputElement.value @@  in *)
(*   match Js.Undefined.toOption ev##target with *)
(*   | None -> *)
(*       None *)
(*   | Some target -> ( *)
(*     match Js.Undefined.toOption target##value with *)
(*     | None -> *)
(*         None *)
(*     | Some value -> *)
(*         if test_key ev then *)
(*           let () = ev##preventDefault () |> ignore in *)
(*           Some msg *)
(*         else None ) ) *)

let on_key_value ?(key = "") test_key msg =
  Tea.Html.Events.onCB "keydown" ~key (fun event ->
      if test_key event then
        let () = Webapi.Dom.Event.preventDefault event in
        let target = Webapi.Dom.Event.target event in
        match
          Webapi.Dom.EventTarget.unsafeAsElement target
          |> Webapi.Dom.HtmlInputElement.ofElement
        with
        | Some input_element ->
            let value = Webapi.Dom.HtmlInputElement.value input_element in
            Some (msg value)
        | None ->
            None
      else None )
(* match Js.Undefined.toOption ev##target with *)
(* | None -> *)
(*     None *)
(* | Some target -> ( *)
(*   match Js.Undefined.toOption target##value with *)
(*   | None -> *)
(*       None *)
(*   | Some value -> *)
(*       if test_key ev then *)
(*         let () = ev##preventDefault () |> ignore in *)
(*         Some (msg value) *)
(*       else None ) ) *)

let on_enter ?(key = "enter") msg =
  on_key ~key
    (fun ev ->
      [%raw {|ev.keyCode|}] = 13
      && [%raw {|!ev.ctrlKey|}] && [%raw {|!ev.shiftKey|}] )
    msg

let on_enter_value ?(key = "enter") msg =
  on_key_value ~key
    (fun ev ->
      [%raw {|ev.keyCode|}] = 13
      && [%raw {|!ev.ctrlKey|}] && [%raw {|!ev.shiftKey|}] )
    msg

let on_ctrl_enter ?(key = "ctrl_enter") msg =
  on_key ~key
    (fun ev -> [%raw {|ev.keyCode|}] = 13 && [%raw {|ev.ctrlKey|}])
    msg

let on_ctrl_enter_value ?(key = "ctrl_enter") msg =
  on_key_value ~key
    (fun ev -> [%raw {|ev.keyCode|}] = 13 && [%raw {|ev.ctrlKey|}])
    msg

let registerGlobal name key tagger =
  let open Vdom in
  let enableCall callbacks_base =
    let callbacks = ref callbacks_base in
    let fn ev = Some (tagger ev) in
    let handler = EventHandlerCallback (key, fn) in
    let elem =
      Webapi.Dom.document |> Webapi.Dom.Document.documentElement
      |> Webapi.Dom.Element.asEventTarget
    in
    let cache = eventHandlerRegister callbacks elem name handler in
    fun () ->
      let _ = eventHandlerUnregister elem name cache in
      ()
  in
  Tea_sub.registration key enableCall

external window_width : int = "innerWidth" [@@mel.scope "window"]

let is_mobile () = window_width < 768

let get_element_by_id id =
  let document = Webapi.Dom.document in
  Webapi.Dom.Document.getElementById id document

external window_set : Webapi.Dom.Window.t -> string -> ('a -> 'b) -> unit = ""
[@@mel.set_index]

let global_callback_cmd name tagger =
  let open Vdom in
  Tea_cmd.call (fun callbacks ->
      window_set Webapi.Dom.window name (fun param ->
          !callbacks.enqueue (tagger param) ) )
