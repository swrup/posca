type attributes = {class': string [@mel.as "class"]}

type editor_props = {attributes: attributes}

type storage_config = {serialize: string -> string}

type storage_configs = {markdown: storage_config}

type extend_options =
  { addKeyboardShortcuts: unit -> (unit -> bool) Js.Dict.t
  ; addStorage: unit -> storage_configs }

type extension = < extend: extend_options -> unit -> unit [@mel.meth] > Js.t

type options =
  { element: Dom.element
  ; extensions: extension array
  ; content: string
  ; autofocus: bool
  ; editorProps: editor_props }

type commands = < setContent: string -> unit [@mel.meth] > Js.t

type markdown = < getMarkdown: unit -> string [@mel.meth] > Js.t

type storage = {markdown: markdown}

type heading_opt = {level: int}

type node_attributes = {href: string Js.Undefined.t}

type chain

(* type chain = *)
(*   < focus: unit -> chain [@mel.meth] *)
(*   ; toggleBold: unit -> chain [@mel.meth] *)
(*   ; toggleItalic: unit -> chain [@mel.meth] *)
(*   ; toggleStrike: unit -> chain [@mel.meth] *)
(*   ; toggleCode: unit -> chain [@mel.meth] *)
(*   ; toggleBlockquote: unit -> chain [@mel.meth] *)
(*   ; toggleCodeBlock: unit -> chain [@mel.meth] *)
(*   ; setHorizontalRule: unit -> chain [@mel.meth] *)
(*   ; toggleBulletList: unit -> chain [@mel.meth] *)
(*   ; toggleOrderedList: unit -> chain [@mel.meth] *)
(*   ; toggleTaskList: unit -> chain [@mel.meth] *)
(*   ; sinkListItem: string -> chain [@mel.meth] *)
(*   ; liftListItem: string -> chain [@mel.meth] *)
(*   ; extendMarkRange: string -> chain [@mel.meth] *)
(*   ; unsetLink: unit -> chain [@mel.meth] *)
(*   ; setLink: node_attributes -> chain [@mel.meth] *)
(*   ; toggleHeading: heading_opt -> chain [@mel.meth] *)
(*   ; undo: unit -> chain [@mel.meth] *)
(*   ; redo: unit -> chain [@mel.meth] *)
(*   ; run: unit -> unit [@mel.meth] > *)
(*   Js.t *)

type can =
  < undo: unit -> bool [@mel.meth]
  ; redo: unit -> bool [@mel.meth]
  ; sinkListItem: string -> bool [@mel.meth]
  ; liftListItem: string -> bool [@mel.meth] >
  Js.t

type editor =
  < commands: commands
  ; storage: storage (* ; setOptions: options -> unit [@mel.meth] *)
  ; createNodeViews: unit -> unit [@mel.meth]
  ; chain: unit -> chain [@mel.meth]
  ; can: unit -> can [@mel.meth]
  ; isActive: string -> bool [@mel.meth]
  ; getAttributes: string -> node_attributes [@mel.meth]
  ; getHTML: unit -> string [@mel.meth]
  ; options: options [@mel.get] >
  Js.t

external chain : editor -> chain = "chain" [@@mel.send]

external focus : chain -> chain = "focus" [@@mel.send]

external run : chain -> unit = "run" [@@mel.send]

external toggle_bold : chain -> chain = "toggleBold" [@@mel.send]

external toggle_italic : chain -> chain = "toggleItalic" [@@mel.send]

external toggle_strike : chain -> chain = "toggleStrike" [@@mel.send]

external toggle_code : chain -> chain = "toggleCode" [@@mel.send]

external toggle_blockquote : chain -> chain = "toggleBlockquote" [@@mel.send]

external toggle_code_block : chain -> chain = "toggleCodeBlock" [@@mel.send]

external toggle_bullet_list : chain -> chain = "toggleBulletList" [@@mel.send]

external toggle_ordered_list : chain -> chain = "toggleOrderedList" [@@mel.send]

external toggle_task_list : chain -> chain = "toggleTaskList" [@@mel.send]

external toggle_heading : heading_opt -> chain = "toggleHeading"
[@@mel.send.pipe: chain]

external set_horizontal_rule : chain -> chain = "setHorizontalRule" [@@mel.send]

external sink_list_item : string -> chain = "sinkListItem"
[@@mel.send.pipe: chain]

external lift_list_item : string -> chain = "liftListItem"
[@@mel.send.pipe: chain]

external undo : chain -> chain = "undo" [@@mel.send]

external redo : chain -> chain = "redo" [@@mel.send]

external extend_mark_range : string -> chain = "extendMarkRange"
[@@mel.send.pipe: chain]

external set_link : node_attributes -> chain = "setLink"
[@@mel.send.pipe: chain]

external unset_link : chain -> chain = "unsetLink" [@@mel.send]

external is_active_heading : editor -> string -> heading_opt -> bool
  = "isActive"
[@@mel.send]

external new_editor_js : options -> editor = "Editor"
[@@mel.module "@tiptap/core"] [@@mel.new]

external starter_kit : extension = "default"
[@@mel.module "@tiptap/starter-kit"]

type starter_kit_options = {hardBreak: bool}

external starter_kit_configure : starter_kit_options -> extension = "configure"
[@@mel.module "@tiptap/starter-kit"] [@@mel.scope "StarterKit"]

external task_list : extension = "default"
[@@mel.module "@tiptap/extension-task-list"]

external task_item : extension = "default"
[@@mel.module "@tiptap/extension-task-item"]

external typography : extension = "default"
[@@mel.module "@tiptap/extension-typography"]

external link : extension = "default" [@@mel.module "@tiptap/extension-link"]

external hard_break : extension = "default"
[@@mel.module "@tiptap/extension-hard-break"]

external hard_break_extend : extend_options -> extension = "extend"
[@@mel.module "@tiptap/extension-hard-break"] [@@mel.scope "HardBreak"]

type markdown_options = {breaks: bool}

external markdown_configure : markdown_options -> extension = "configure"
[@@mel.module "tiptap-markdown"] [@@mel.scope "Markdown"]

type element_options = {element: Dom.element}

external set_element : editor -> element_options -> unit = "setOptions"
[@@mel.send]

let custom_hard_break send_on_enter =
  hard_break_extend
    { addKeyboardShortcuts=
        (fun () ->
          let shift_enter_break =
            ("Shift-Enter", [%raw {|() => this.editor.commands.setHardBreak()|}])
          in
          let enter_noop = ("Enter", fun () -> true) in
          let keymap =
            if send_on_enter && not (Html.is_mobile ()) then
              [shift_enter_break; enter_noop]
            else [shift_enter_break]
          in
          Js.Dict.fromList keymap )
    ; addStorage=
        (fun () ->
          {markdown= {serialize= [%raw {|(state) => state.write('\n')|}]}} ) }
(* {markdown= {serialize= Js.Dict.fromList [("hardBreak", "\n")]}} ) } *)

let new_editor ?(send_on_enter = false) elem =
  Js.log send_on_enter ;
  new_editor_js
    { element= elem
    ; extensions=
        [| starter_kit_configure {hardBreak= false}
         ; custom_hard_break send_on_enter
         ; task_list
         ; task_item
         ; typography
         ; link
         ; markdown_configure {breaks= true} |]
    ; content= ""
    ; autofocus= true
    ; editorProps=
        { attributes=
            { class'=
                "prose w-full max-w-full rounded-b-[18px] \
                 bg-dominant-soft-dark p-2 text-cardboard-gray shadow-none \
                 outline-none dark:prose-invert hover:border-contrast-dark/50 \
                 focus:border-contrast-dark/50 \
                 focus-visible:border-contrast-dark/50 prose-headings:m-0 \
                 prose-p:mb-2 prose-a:cursor-pointer prose-blockquote:m-0 \
                 prose-hr:mb-4 dark:text-cardboard-gray" } } }
