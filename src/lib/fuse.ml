module type Item = sig
  type t
end

module MakeFuse (S : Item) = struct
  type fuse

  type key = {name: string; getFn: S.t -> string}

  type options =
    { includeScore: bool
    ; threshold: float
    ; keys: key array
    ; useExtendedSearch: bool }

  type result = {item: S.t; refIndex: int; score: float}

  external create : S.t array -> options -> fuse = "default"
  [@@mel.new] [@@mel.module "fuse.js"]

  external search : fuse -> string -> result array = "search" [@@mel.send]
end
