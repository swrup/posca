type markdownit

type node = {type': string [@mel.as "type"]; children: node array Js.nullable}

external parse : markdownit -> string -> _ Js.t -> node array = "parse"
[@@mel.send]

(* external parse_inline : markdownit -> string -> _ Js.t -> string array = "parseInline" [@@mel.send] *)

external new_markdownit : unit -> markdownit = "default"
[@@mel.module "markdown-it"]

let is_rich text =
  let plain_types =
    [|"paragraph_open"; "paragraph_close"; "text"; "softbreak"; "inline"|]
  in
  let rec node_is_plain node =
    Belt.Array.some plain_types (( = ) node.type')
    &&
    match Js.Nullable.toOption node.children with
    | None ->
        true
    | Some c ->
        Belt.Array.every c node_is_plain
  in
  let markdownit = new_markdownit () in
  let parsed = parse markdownit text [%mel.obj {foo= "bar"}] in
  let is_plain = Belt.Array.every parsed node_is_plain in
  not is_plain

let as_html text = ""
