type event

external create_event : string -> event = "Event" [@@mel.new]

external document_dispatch_event : event -> unit = "dispatchEvent"
[@@mel.scope "document"]

type register_sw_params =
  { on_need_refresh: unit -> unit [@mel.as "onNeedRefresh"]
  ; on_offline_ready: unit -> unit [@mel.as "onOfflineReady"] }

type return_register_sw = unit -> unit

external register_sw : register_sw_params -> return_register_sw = "registerSW"
[@@mel.module "virtual:pwa-register"]

let register () =
  let on_need_refresh () =
    let () = Js.log "onNeedRefresh" in
    document_dispatch_event (create_event "sw_refresh")
  in
  let on_offline_ready () = Js.log "onOffLineReady" in
  register_sw {on_need_refresh; on_offline_ready}

(* let subscribe_sw_update tagger = *)
(*   let open Vdom in *)
(*   let enableCall callbacks = *)
(*     let on_need_refresh = fun () -> Js.log "onNeedRefresh" in *)
(*     let on_offline_ready = fun () -> *)
(*       let () = Js.log "onOffLineReady" in *)
(*       callbacks.enqueue tagger *)
(*     in *)
(*     let _ = register_sw {on_need_refresh; on_offline_ready} in *)
(*     fun () -> () *)
(*   in *)
(*   Tea_sub.registration "service_worker" enableCall *)
