type time_ago

type locale

external new_time_ago : string -> time_ago = "default"
[@@mel.new] [@@mel.module "javascript-time-ago"]

external js_format : time_ago -> Js.Date.t -> string = "format" [@@mel.send]

external time_ago_module : time_ago = "default"
[@@mel.module "javascript-time-ago"]

external add_locale : time_ago -> locale -> unit = "addLocale" [@@mel.send]

external locale_en : locale = "default"
[@@mel.module "javascript-time-ago/locale/en"]

external locale_fr : locale = "default"
[@@mel.module "javascript-time-ago/locale/fr"]

let () =
  add_locale time_ago_module locale_en ;
  add_locale time_ago_module locale_fr

let time_ago = ref (new_time_ago "en")

let update_locale lc = time_ago := new_time_ago lc

let format date = js_format !time_ago date
