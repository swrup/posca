type room = Room of Matrix.room | Space of Matrix.room * room array

type model = {show_menu: bool; filter_name: string; rooms: room array}

type msg =
  | SetFilterName of string
  | GoTo of Router.route
  | AddRoom of Matrix.room
[@@deriving accessors]

let msg_to_string _msg = "content msg"

let init () =
  let show_menu = if Html.is_mobile () then false else true in
  {show_menu; filter_name= ""; rooms= [||]}

let get_parents room =
  let room_state = room##currentState in
  let parent_states =
    Matrix.SpaceParentState.get_state_events room_state "m.space.parent"
  in
  Belt.Array.map parent_states (fun state -> state##getStateKey ())

let get_children room =
  let room_state = room##currentState in
  let child_states =
    Matrix.SpaceChildState.get_state_events room_state "m.space.child"
  in
  Belt.Array.map child_states (fun state -> state##getStateKey ())

let rec make_tree room other_rooms =
  match room##getType () |> Js.Undefined.toOption with
  | Some "m.space" ->
      let children =
        Belt.Array.keepMap (get_children room) (fun child ->
            Belt.Array.getBy other_rooms (fun other_room ->
                child = other_room##roomId ) )
      in
      Space
        ( room
        , Belt.Array.map children (fun child -> make_tree child other_rooms) )
  | _ ->
      Room room

let build_tree rooms =
  let rest, first_level =
    Belt.Array.partition rooms (fun room ->
        Belt.Array.some rooms (fun other_room ->
            Belt.Array.some (get_children other_room) (fun other_room_child ->
                room##roomId = other_room_child ) ) )
  in
  Belt.Array.map first_level (fun room -> make_tree room rest)

let room_list () = Js.Dict.values Matrix.client##store##rooms

let get_rooms () = room_list () |> build_tree

let filter_by_name match_fun rooms =
  let rec filter_room = function
    | Room r ->
        if match_fun r then Some (Room r) else None
    | Space (r, children) -> (
        if match_fun r then Some (Space (r, children))
        else
          let children =
            Belt.Array.keepMap children (fun child -> filter_room child)
          in
          match children with
          | [||] ->
              None
          | filtered_children ->
              Some (Space (r, filtered_children)) )
  in
  Belt.Array.keepMap rooms (fun r -> filter_room r)

module RoomFuse = Fuse.MakeFuse (struct
  type t = Matrix.room
end)

let update model = function
  | SetFilterName "" ->
      let rooms = get_rooms () in
      ({model with filter_name= ""; rooms}, Tea.Cmd.none)
  | SetFilterName filter_name ->
      let fuse_options =
        { RoomFuse.includeScore= true
        ; threshold= 0.4
        ; useExtendedSearch= true
        ; keys= [|{name= "name"; getFn= (fun room -> room##name)}|] }
      in
      let fuse = RoomFuse.create (room_list ()) fuse_options in
      let fuse_results = RoomFuse.search fuse filter_name in
      let match_fun room =
        Js.Array.some fuse_results ~f:(fun result ->
            result.RoomFuse.item##roomId = room##roomId )
      in
      let rooms = get_rooms () |> filter_by_name match_fun in
      ({model with filter_name; rooms}, Tea.Cmd.none)
  | AddRoom _ ->
      let rooms = get_rooms () in
      ({model with rooms}, Tea.Cmd.none)
  | GoTo _ ->
      (model, Tea.Cmd.none)

let equal_to_option value = function None -> false | Some v -> v = value

let room_aliases room =
  match room##getCanonicalAlias () |> Js.Nullable.toOption with
  | None ->
      room##getAltAliases ()
  | Some alias ->
      Js.Array.concat (room##getAltAliases ()) ~other:[|alias|]

let equal_to_room room (route : Router.route) =
  match route with
  | Room (Id room_id, _) ->
      room_id = room##roomId
  | Room (Alias room_alias, _) ->
      Js.Array.some (room_aliases room) ~f:(fun a -> a = room_alias)
  | _ ->
      false

let header_dropdown () =
  if Auth.is_logged_in () && not (Auth.is_guest ()) then
    Components.ClickDropdown.element "sidebar-header__dropdown mr-2 font-light"
      ~items:
        [ Components.button_link
            ~route:(goTo, Router.CreateRoom (Chat, None))
            ~icon:"plus" "New room"
        ; Components.button_link
            ~route:(goTo, Router.CreateRoom (Space, None))
            ~icon:"plus" "New space" ]
      ~button_icon:"ellipsis-vertical" ~button_label:"More"
  else Tea.Html.noNode

let space_dropdown space =
  if Auth.is_logged_in () && not (Auth.is_guest ()) then
    Components.ClickDropdown.element
      "sidebar-item__dropdown mr-2 hidden pl-3 group-hover:block"
      ~items:
        [ Components.button_link
            ~route:(goTo, Router.CreateRoom (Chat, Some space##roomId))
            ~icon:"plus" "New room"
        ; Components.button_link
            ~route:(goTo, Router.CreateRoom (Space, Some space##roomId))
            ~icon:"plus" "New room" ]
      ~button_icon:"ellipsis-vertical" ~button_label:"More"
  else Tea.Html.noNode

let chat_dropdown _chat =
  if Auth.is_logged_in () && not (Auth.is_guest ()) then
    Components.ClickDropdown.element
      "sidebar-item__dropdown mr-2 hidden pl-3 group-hover:block" ~items:[]
      ~button_icon:"ellipsis-vertical" ~button_label:"More"
  else Tea.Html.noNode

let room_type_gradient_from room =
  match Matrix.room_type room with
  | Space ->
      "from-room-space"
  | Chat ->
      "from-room-chat"
  | Statuses ->
      "from-room-statuses"
  | Forum ->
      "from-room-forum"
  | Media ->
      "from-room-media"

let room_type_border_left room =
  match Matrix.room_type room with
  | Space ->
      "border-l-room-space"
  | Chat ->
      "border-l-room-chat"
  | Statuses ->
      "border-l-room-statuses"
  | Forum ->
      "border-l-room-forum"
  | Media ->
      "border-l-room-media"

let avatar_view room =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let homeserver_url = Matrix.client##getHomeserverUrl () in
  match
    room##getAvatarUrl homeserver_url 100 100 "scale" |> Js.Nullable.toOption
  with
  | Some url ->
      img
        [ class'
            "mr-2 inline-block h-8 w-8 rounded-full border-2 \
             border-dominant-soft-dark"
        ; src url ]
        []
  | None ->
      noNode

let rec room_view route = function
  | Room room ->
      let open Tea.Html in
      let open Tea.Html.Attributes in
      li ~unique:room##roomId
        [ classList
            [ ( "sidebar-item group relative m-0 flex max-h-10 items-center \
                 justify-between border-l-4 border-y-[transparent] px-0 \
                 transition duration-100 hover:bg-contrast-dark/10 "
                ^ room_type_border_left room
              , true )
            ; ("border-y-4", not @@ equal_to_room room route)
            ; ( "to-transparentbg-gradient-to-r to-transparent border-y-0 \
                 bg-gradient-to-r transition duration-300 after:absolute \
                 after:right-0 after:z-50 after:h-12 after:w-1 \
                 after:border-y-[4px] after:border-r-[4px] \
                 after:border-y-contrast-dark after:border-r-[transparent] \
                 after:bg-dominant-dark "
                ^ room_type_gradient_from room
              , equal_to_room room route ) ] ]
        [ Router.link
            ~props:
              [ class'
                  "my-1 flex flex-grow items-center overflow-hidden \
                   overflow-ellipsis whitespace-nowrap p-1 pl-6" ]
            goTo
            (Room (Id room##roomId, None)) (* XXX *)
            [ span
                [class' "room-name overflow-hidden"]
                [avatar_view room; text room##name] ]
          (* ; chat_dropdown room ] *) ]
  | Space (room, children) ->
      let open Tea.Html in
      let open Tea.Html.Attributes in
      li ~unique:room##roomId
        [class' "sidebar-item m-0 border-l-2 border-[transparent] px-0"]
        [ div
            [ class'
                "align-stretch group m-1 ml-1  flex justify-between rounded \
                 p-1 font-bold text-contrast-dark" ]
            [ span
                [class' "room-name overflow-hidden p-1"]
                [avatar_view room; text room##name]
            ; space_dropdown room ]
          (* Old design *)
          (* [class' "bg-bleu-metal sidebar-item align-stretch m-0 justify-between rounded-lg px-0"] *)
          (* [ span [class' "room-name bg-bleu-gris block rounded-t-lg"] [text room##name] *)
        ; ul
            [class' "ml-1"]
            (children |. Belt.Array.map (room_view route) |> Belt.List.fromArray)
        ]

let room_list_view route model =
  let tree = model.rooms in
  let open Tea.Html in
  let open Tea.Html.Attributes in
  (* let chat_view room = *)
  (*   li ~unique:room##roomId *)
  (*     [ class' *)
  (* "sidebar-item align-stretch \ *) (* m-0 flex justify-between px-0
     py-1          transition duration-500 hover:bg-contrast-dark/10" ] *)
  (*     [ Router.link *)
  (*         ~props: *)
  (*           [ classList *)
  (* [ ( "\ *) (* flex flex-grow items-center overflow-hidden
     overflow-ellipsis                    whitespace-nowrap p-1" *)
  (*                 , true ) *)
  (*               ; ("pl-6", true) *)
  (*               ; ("bg-contrast-dark/50", equal_to_room room route) ] ] *)
  (*         goTo *)
  (*         (Room (Id room##roomId)) (* XXX *) *)
  (*         [span [class' "room-name"] [text room##name]] *)
  (*     ; chat_dropdown room ] *)
  (* in *)
  ul
    [ id "sidebar-list"
    ; class' "sidebar-list sidebar-group-list min-h-full overflow-auto pt-12" ]
    (tree |. Belt.Array.map (room_view route) |> Belt.List.fromArray)

let view route model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let header =
    div
      [ id "sidebar-header"
      ; classList
          [ ("z-10 w-full bg-dominant-dark md:w-[15rem]", model.show_menu)
          ; ("w-0 truncate", Bool.not model.show_menu)
          ; ( "absolute flex h-12 items-center justify-between duration-500"
            , true ) ] ]
      [ label
          [ id "sidebar-filter-label"
          ; class' "icon-label relative m-2 block w-full" ]
          [ Icons.icon
              ~class':
                "absolute left-2 top-1/2 h-5 w-5 -translate-y-1/2 transform"
              "search"
          ; input'
              [ type' "text"
              ; class'
                  "text-color-main-text inline h-8 w-full rounded-full pl-8 \
                   shadow-none outline-none hover:border-contrast-dark/50 \
                   focus:border-contrast-dark/50 \
                   focus-visible:border-contrast-dark/50 \
                   dark:bg-dominant-soft-dark dark:text-cardboard-gray md:w-48"
              ; id "sidebar-filter-field"
              ; Tea.Html.Events.onInput setFilterName ]
              [text "Filter"] ]
      ; header_dropdown () ]
  in
  div
    [ id "sidebar"
    ; classList
        [ ("w-full md:w-64", model.show_menu)
        ; ("z-10 w-0", Bool.not model.show_menu)
        ; ( "relative flex shrink-0 scroll-px-1 flex-col overflow-hidden \
             whitespace-nowrap p-0 duration-500 ease-in-out after:absolute \
             after:right-[10px] after:top-0 after:h-full after:min-h-full \
             after:overflow-auto after:border-l-[4px] \
             after:border-contrast-dark"
          , true ) ] ]
    (if Auth.is_logged_in () then [header; room_list_view route model] else [])
