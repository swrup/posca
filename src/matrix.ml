module Client = Matrix_sdk.Client

type room_id = Matrix_sdk.Common.room_id

type room_alias = Matrix_sdk.Common.room_alias

type event_id = Matrix_sdk.Common.event_id

type client = Matrix_sdk.Client.client

type room = Matrix_sdk.Models.room

type matrix_event = Matrix_sdk.Models.matrix_event

type event_content = Matrix_sdk.Models.event_content

type event_relation = Matrix_sdk.Event_relation.event_relation

type create_room_response = Matrix_sdk.Client.create_room_response

type login_response = Matrix_sdk.Client.login_response

type client_config = Matrix_sdk.Client.client_config

type event_timeline = Matrix_sdk.Models.event_timeline

type timeline_window = Matrix_sdk.Timeline_window.t

(* let matrixcs = Matrix_sdk.matrixcs *)

(* let matrixcs =  *)

(* external client : client ref = "matrixcs" [@@mel.module "document"] *)

(* let client = Client.matrixcs *)

type room_address = Id of room_id | Alias of room_alias

external client : client = "matrixc" [@@mel.scope "window"]

external updated_client : bool = "updatedClient" [@@mel.scope "window"]

external window_indexed_db : Matrix_sdk.Client.indexed_db = "indexedDB"
[@@mel.scope "window"]

external window_local_storage : Matrix_sdk.Client.local_storage = "localStorage"
[@@mel.scope "window"]

let default_server = Config.matrix_url

external new_indexed_db_worker : unit -> Matrix_sdk.Client.indexed_db_worker
  = "default"
[@@mel.new] [@@mel.module "./store_worker.ml?worker"]

let new_store db_name =
  Matrix_sdk.Client.new_indexed_db_store
    { indexedDB= window_indexed_db
    ; localStorage= window_local_storage
    ; dbName= db_name
    ; workerFactory= (fun () -> new_indexed_db_worker ()) }

let create_client is_guest =
  let _ = Js.log default_server in
  let store_name = if is_guest then "posca:guest" else "posca:logged-in" in
  let store = new_store store_name in
  Matrix_sdk.Client.create_client
    [%mel.obj {baseUrl= default_server; store; timelineSupport= true}]

let create_client_to_server server =
  let _ = Js.log server in
  let store = new_store "posca:logged-in" in
  let s =
    Matrix_sdk.Client.create_client
      [%mel.obj {baseUrl= server; store; timelineSupport= true}]
  in
  Js.log s ; s

let create_client_with_token ?(base_url = default_server) user_id access_token
    is_guest =
  let store_name = if is_guest then "posca:guest" else "posca:logged-in" in
  let store = new_store store_name in
  let login_map =
    [%mel.obj
      { baseUrl= base_url
      ; accessToken= access_token
      ; userId= user_id
      ; timelineSupport= true
      ; store }]
  in
  Matrix_sdk.Client.create_client_params login_map

(* external start_client : client -> unit = "startClient" [@@mel.send] *)

let start_client () =
  client##setMaxListeners 500 ;
  let threadSupport = not @@ client##isGuest () in
  client##store##startup ()
  |> Js.Promise.then_ (fun _ -> client##startClient {threadSupport})

(* external stop_client : client -> unit = "stopClient" [@@mel.send] *)

let stop_client () = client##stopClient ()

let remove_all_listeners () = client##removeAllListeners ()

let destroy_store () = client##store##destroy ()

let set_client new_client = [%mel.raw {|window.matrixc = new_client|}] |> ignore

let trigger_client_start () =
  [%mel.raw {|window.updatedClient = true|}] |> ignore

let reset_updated_client () =
  [%mel.raw {|window.updatedClient = false|}] |> ignore

let get_joined_rooms client =
  client##getJoinedRooms ()
  |> Js.Promise.then_ (fun res ->
         res##joined_rooms |> Belt.List.fromArray |> Js.Promise.resolve )

(* let new_client_params matrix_id access_token = *)
(*   let login_map = Js.Dict.empty () in *)
(*   let () = Js.Dict.set login_map "baseUrl" "http://matrix.imago.local:8008" in *)
(*   let () = Js.Dict.set login_map "accessToken" access_token in *)
(*   let () = Js.Dict.set login_map "userId" matrix_id in *)
(*   create_client_params login_map *)

(* let login client username password = *)
(* let login_map = Js.Dict.empty () in *)
(* let () = Js.Dict.set login_map "user" username in *)
(* let () = Js.Dict.set login_map "password" password in *)
(* client##login "m.login.password" login_map *)

let login_with_password username password =
  client##loginWithPassword username password

(* let login_with_token client token = *)
(*   client##loginWithToken token *)

(* let register client username password = *)
(*   client##register username password *)

type prototype

type matrix_error_class = < prototype: prototype > Js.t

external matrix_error_class : matrix_error_class = "MatrixError"
[@@mel.module "matrix-js-sdk/src/http-api"]

let register username password session_id auth =
  client##register username password session_id auth
  |> Js.Promise.catch (fun e ->
         (* we have to do this because of `instanceof MatrixError` in js-sdk returns false if MatrixError has been imported from different places (http-api or http-api/errors) *)
         (* TODO: we probably should do it only if it's a MatrixError (has `errcode`, `data`, `name`) *)
         (* TODO: we probably should do it somewhere else? *)
         let set_prototype : 'a -> prototype -> unit =
          fun a b -> [%raw {|Object.setPrototypeOf(a, b)|}]
         in
         set_prototype e matrix_error_class##prototype ;
         Js.Promise.reject @@ Obj.magic e )

let request_register_email_token email secret attempt =
  client##requestRegisterEmailToken email secret attempt

type client_well_known_part = {base_url: string}

let find_server_config homeserver =
  Client.find_client_config homeserver
  |> Js.Promise.then_ (fun config ->
         let homeserver_wellknown = Js.Dict.unsafeGet config "m.homeserver" in
         match homeserver_wellknown##state with
         | "SUCCESS" ->
             Js.Promise.resolve config
         | _ ->
             let base_url =
               if Js.String.includes ~search:"://" homeserver then homeserver
               else "https://" ^ homeserver
             in
             let fake_config =
               Js.Dict.fromArray [|("m.homeserver", {base_url})|]
             in
             Client.from_discovery_config (Obj.magic fake_config) )

let logout () = client##logout true

type room_type = Space | Chat | Forum | Statuses | Media

let room_type_of_string = function
  | "space" ->
      Space
  | "forum" ->
      Forum
  | "statuses" ->
      Statuses
  | "media" ->
      Media
  | _ ->
      Chat

let string_of_room_type = function
  | Space ->
      "space"
  | Forum ->
      "forum"
  | Statuses ->
      "statuses"
  | Media ->
      "media"
  | Chat ->
      "chat"

let parse_room_type type_string =
  match type_string with
  | "m.space" ->
      Space
  | "org.technostructures.posca.statuses" ->
      Statuses
  | "org.technostructures.posca.forum" ->
      Forum
  | "org.technostructures.posca.media" ->
      Media
  | _ ->
      Chat

let room_default_type room =
  match room##getType () |> Js.Undefined.toOption with
  | Some type_string ->
      parse_room_type type_string
  | None ->
      Chat

let room_type room =
  let state_type = room##getType () |> Js.Undefined.toOption in
  let account_data_type =
    match
      room##getAccountData "org.technostructures.posca.display"
      |> Js.Undefined.toOption
    with
    | None ->
        None
    | Some event ->
        let content = event##getContent () in
        Js.Dict.get content "display"
  in
  match (state_type, account_data_type) with
  | _, Some type_string ->
      parse_room_type type_string
  | Some type_string, None ->
      parse_room_type type_string
  | None, None ->
      Chat

let create_room ?(invite = [||]) ?(alias = None) ?(visibility = "public")
    ?(topic = "") ?space ?(display = Chat) name =
  let type' =
    match display with
    | Space ->
        Some ("type", "m.space")
    | Chat ->
        None
    | Statuses ->
        Some ("type", "org.technostructures.posca.statuses")
    | Forum ->
        Some ("type", "org.technostructures.posca.forum")
    | Media ->
        Some ("type", "org.technostructures.posca.media")
  in
  let creation_content =
    Belt.Option.map type' (fun tup -> Js.Dict.fromList [tup])
    |> Js.Undefined.fromOption
  in
  (* let initial_state = match space with *)
  (* | Some space_id -> [| *)
  (*   { state_key = space_id *)
  (*   ; type' = "m.space.parent" *)
  (*   ;  *)
  (* |] *)
  (* | None -> [||] *)
  (* in *)
  let options : Matrix_sdk.Client.create_room_options =
    { invite
    ; name
    ; topic
    ; visibility
    ; creation_content
    ; room_alias_name= Js.Undefined.fromOption alias
    ; initial_state=
        [| { type'= "m.room.guest_access"
           ; content= Js.Dict.fromList [("guest_access", "can_join")]
           ; state_key= "" }
         ; { type'= "m.room.history_visibility"
           ; content= Js.Dict.fromList [("history_visibility", "world_readable")]
           ; state_key= "" } |] }
    (* [%mel.obj {invite; name; room_alias_name= alias; topic; visibility}] *)
  in
  client##createRoom options

let current_user () =
  let user_id = client##getUserId () in
  let user = client##getUser user_id in
  Js.Nullable.toOption user

let current_user_name _client =
  current_user () |. Belt.Option.map (function user -> user##displayName)

(* TODO: refacto those 2 functions *)

let like_count room event_id =
  match
    room##relations##getChildEventsForEvent event_id "m.annotation" "m.reaction"
    |> Js.Undefined.toOption
  with
  | None ->
      0
  | Some relations -> (
      let sorted_annotations = relations##getSortedAnnotationsByKey () in
      match
        Js.Array.find ~f:(fun (a, _) -> a = {js|👍️|js}) sorted_annotations
      with
      | Some (_, set) ->
          set.Matrix_sdk.Set.size
      | None ->
          0 )

let personal_like room event_id =
  match
    room##relations##getChildEventsForEvent event_id "m.annotation" "m.reaction"
    |> Js.Undefined.toOption
  with
  | None ->
      None
  | Some relations -> (
      let sorted_annotations = relations##getSortedAnnotationsByKey () in
      match
        Js.Array.find ~f:(fun (a, _) -> a = {js|👍️|js}) sorted_annotations
      with
      | None ->
          None
      | Some (_, set) ->
          Js.Array.find
            ~f:(fun e -> client##getUserId () = e##getSender ())
            (Matrix_sdk.Set.to_array set) )

let subscribe_to_timeline tagger =
  let open Vdom in
  let enableCall callbacks =
    let args =
      `timeline
        (fun event room _toStartOfTimeline _removed _data ->
          callbacks.enqueue (tagger event room) )
    in
    let _ = Matrix_sdk.Client.on client args in
    fun () ->
      (* let _ = off client args in *)
      ()
  in
  Tea_sub.registration "Room.timeline" enableCall

let subscribe_to_room_accountdata tagger =
  let open Vdom in
  let enableCall callbacks =
    let args =
      `room_accountdata
        (fun event room old_event ->
          callbacks.enqueue (tagger event room old_event) )
    in
    let _ = Matrix_sdk.Client.on client args in
    fun () ->
      (* let _ = off client args in *)
      ()
  in
  Tea_sub.registration "Room.accountData" enableCall

let subscribe_to_room_redaction tagger =
  let open Vdom in
  let enableCall callbacks =
    let args =
      `room_redaction (fun event room -> callbacks.enqueue (tagger event room))
    in
    let _ = Matrix_sdk.Client.on client args in
    fun () ->
      (* let _ = off client args in *)
      ()
  in
  Tea_sub.registration "Room.redaction" enableCall

let subscribe_on_sync tagger =
  let open Vdom in
  let enableCall callbacks =
    let args =
      `sync
        (fun state prev_state data ->
          callbacks.enqueue (tagger state prev_state data) )
    in
    let _ = Matrix_sdk.Client.on client args in
    fun () -> ()
  in
  Tea_sub.registration "sync" enableCall

let subscribe_on_room tagger =
  let open Vdom in
  let enableCall callbacks =
    let args = `room (fun room -> callbacks.enqueue (tagger room)) in
    let _ = Matrix_sdk.Client.on client args in
    fun () -> ()
  in
  Tea_sub.registration "room" enableCall

let subscribe_once_logged_out tagger =
  let open Vdom in
  let enableCall callbacks =
    let args = `logged_out (fun error -> callbacks.enqueue (tagger error)) in
    let _ = Matrix_sdk.Client.once client args in
    fun () -> ()
  in
  Tea_sub.registration "sync" enableCall

let remove_mx_reply formatted_body =
  let regexp =
    Js.Re.fromStringWithFlags ~flags:"s" "^<mx-reply>.+<\\/mx-reply>"
  in
  Js.String.replaceByRe ~regexp ~replacement:"" formatted_body

(* https://spec.matrix.org/latest/client-server-api/#mroommessage-msgtypes *)

type body = FormattedBody of string | Body of string

(* type thumbnail_info = *)
(*   { h : int *)
(*   ; mimetype : string *)
(*   ; size : int *)
(*   ; w : int } *)
(**)
(* type image_info = *)
(*   { h : int *)
(*   ; mimetype: string *)
(*   ; size : int *)
(*   ; thumbnail_info : thumbnail_info *)
(*   ; thumbnail_url : string *)
(*   ; w : int } *)

type image_params = {body: string; url: string}

(* @TODO: uncomment and implement all message types *)
type parsed_message = Text of body | Image of image_params
(* | Emote of body *)
(* | Notice of body *)
(* | File of ... *)
(* | Audio of ... *)
(* | Location of ... *)
(* | Video of ... *)

let parse_message event =
  let content = event##getContent () in
  match Js.Dict.get content "msgtype" with
  | None ->
      Text (Body (Js.Dict.unsafeGet content "body"))
  | Some "m.text" -> (
    match Js.Dict.get content "formatted_body" with
    | Some formatted_body ->
        Text (FormattedBody formatted_body)
    | None ->
        Text (Body (Js.Dict.unsafeGet content "body")) )
  | Some "m.image" ->
      let body = Js.Dict.unsafeGet content "body" in
      let url = Js.Dict.unsafeGet content "url" in
      Image {body; url}
  (* | Some "m.emote" -> *)
  (* | Some "m.notice" -> *)
  (* | Some "m.file" -> *)
  (* | Some "m.audio" -> *)
  (* | Some "m.location" -> *)
  (* | Some "m.video" -> *)
  | Some _ ->
      Text (Body (Js.Dict.unsafeGet content "body"))

let image_to_html ?(width = Some 400) ?(height = Some 400)
    ?(resize_method = Some "scale") ?(allow_direct_links = true) ?(classes = "")
    image_params =
  let base_url = client##getHomeserverUrl () in
  let uri =
    Matrix_sdk.Client.get_http_uri_for_mxc base_url ~mxc:image_params.url
      ~width:(Js.Undefined.fromOption width)
      ~height:(Js.Undefined.fromOption height)
      ~resize_method:(Js.Undefined.fromOption resize_method)
      ~allow_direct_links ()
  in
  let open Tea.Html in
  let open Tea.Html.Attributes in
  img [class' classes; src uri; title image_params.body] []

module TypeState = Client.MakeEventAccessors (struct
  type t = < _type: string > Js.t
end)

module IdState = Client.MakeEventAccessors (struct
  type t = < id: Matrix_sdk.Common.room_id > Js.t
end)

module SpaceParentState = Client.MakeEventAccessors (struct
  type t = < canonical: bool ; via: string array > Js.t
end)

module SpaceChildState = Client.MakeEventAccessors (struct
  type t = < suggested: bool ; via: string array > Js.t
end)

module SpaceRelationState = Client.MakeEventAccessors (struct
  type t = < via: string array > Js.t
end)

(* https://github.com/matrix-org/matrix-spec-proposals/blob/kegan/msc/threading/proposals/2836-threading.md *)

type thread_message_relationship = {rel_type: string; event_id: string}

type thread_message_in_reply_to = {event_id: string}

type thread_message_relates_to =
  { event_id: Matrix_sdk.Common.event_id
  ; is_falling_back: bool (* ; key: string Js.Undefined.t *)
  ; in_reply_to: thread_message_in_reply_to [@mel.as "m.in_reply_to"]
  ; rel_type: string }

type thread_message_content =
  { body: string
  ; msgtype: string
  ; format: string option [@mel.optional]
  ; formatted_body: string option [@mel.optional]
  ; relates_to: thread_message_relates_to option
        [@mel.as "m.relates_to"] [@mel.optional] }
[@@deriving jsProperties]

module ThreadMessage = Client.MakeEventAccessors (struct
  type t = thread_message_content
end)

type reaction_relates_to = {event_id: event_id; rel_type: string; key: string}

type reaction_content =
  {relates_to: reaction_relates_to [@mel.as "m.relates_to"]}

module ReactionMessage = Client.MakeEventAccessors (struct
  type t = reaction_content
end)

let remove_title body =
  let regexp = [%re "/^#+ (.*?)\\n\\n/s"] in
  Js.String.replaceByRe ~regexp ~replacement:"" body

let remove_title_formatted body =
  let regexp = [%re "/^(<h[1-5]>.+?<\\/h[1-5]>)/"] in
  Js.String.replaceByRe ~regexp ~replacement:"" body
