type msg =
  | GoTo of Router.route
  | SetCurrent of Matrix.event_id option
  | InitializedRoom of Matrix.event_id option
  | SavePostTitle of string
  | ChangeDisplay of string
  | ShowDisplayModal
  | HideDisplayModal
  | ShowSendBox
  | HideSendBox
  | Scrolled of Scroll.scroll_position
  | SentMessage of (< event_id: string > Js.t, string) result
  | PaginatedBackwards of int option * (bool, string) result
  | ChangedDisplay of (bool, string) result
  | Focus of Matrix.event_id * Matrix.event_id option
  | Redact of Matrix.matrix_event
  | EventMsg of Matrix.matrix_event * Event.msg
  | LoadedTimelineWindow of
      Matrix.matrix_event option * (Matrix.timeline_window, string) result
  | GotEvent of Matrix.matrix_event * bool
  | FetchedEvent of (Matrix.matrix_event, string) result
  | EditorMsg of Editor.msg
[@@deriving accessors]

type status = Loading | Loaded

type timeline_view =
  { events: Event.model array
  ; timeline_window: Matrix.timeline_window
  ; status: status }

type model =
  { new_post_title: string
  ; send_editor: Editor.model
  ; scrolled_position: Scroll.scroll_position
  ; display: Matrix.room_type
  ; timeline_views: timeline_view Js.Dict.t
  ; current_event: Matrix.matrix_event option }

let display_to_string = function
  | Matrix.Chat ->
      "chat"
  | Forum ->
      "forum"
  | Statuses ->
      "statuses"
  | Media ->
      "media"
  | Space ->
      "space"

let display_to_identifier_string = function
  | Matrix.Chat ->
      "org.technostructures.posca.chat"
  | Forum ->
      "org.technostructures.posca.forum"
  | Statuses ->
      "org.technostructures.posca.statuses"
  | Media ->
      "org.technostructures.posca.media"
  | Space ->
      "org.technostructures.posca.space"

let string_to_display = function
  | "org.technostructures.posca.forum" ->
      Matrix.Forum
  | "org.technostructures.posca.statuses" ->
      Statuses
  | "org.technostructures.posca.media" ->
      Media
  | _ ->
      Chat

let msg_to_string = function
  | GoTo _ ->
      "GoTo"
  | SetCurrent _ ->
      "SetCurrent"
  | InitializedRoom _ ->
      "InitializedRoom"
  | SavePostTitle _ ->
      "SavePostTitle"
  | ChangeDisplay _ ->
      "ChangeDisplay"
  | ShowDisplayModal ->
      "ShowDisplayModal"
  | HideDisplayModal ->
      "HideDisplayModal"
  | ShowSendBox ->
      "ShowSendBox"
  | HideSendBox ->
      "HideSendBox"
  | Scrolled _ ->
      "Scrolled"
  | SentMessage _ ->
      "SentMessage"
  | PaginatedBackwards _ ->
      "PaginatedBackwards"
  | ChangedDisplay _ ->
      "ChangedDisplay"
  | Focus _ ->
      "Focus"
  | Redact _ ->
      "Redact"
  | EventMsg _ ->
      "EventMsg"
  | GotEvent _ ->
      "GotEvent"
  | LoadedTimelineWindow _ ->
      "LoadedTimelineWindow"
  | FetchedEvent _ ->
      "FetchedEvent"
  | EditorMsg _ ->
      "EditorMsg"

let init_room_cmd room maybe_event_id =
  room##createThreadsTimelineSets ()
  |> Js.Promise.then_ (fun _ -> room##fetchRoomThreads ())
  |> Js.Promise.catch (fun _ -> Js.Promise.resolve ())
  |. Tea_promise.cmd (fun _ -> Some (InitializedRoom maybe_event_id))

let fetch_event_cmd room event_id =
  ( match room##findEventById event_id |> Js.Undefined.toOption with
  | Some e ->
      Js.Promise.resolve e
  | None ->
      Matrix.client##fetchRoomEvent room##roomId event_id
      |> Js.Promise.then_ (fun t ->
             let e = [%raw {| matrixc.getEventMapper()(t)|}] in
             Js.Promise.resolve e ) )
  |. Tea_promise.result fetchedEvent

let load_timeline_window_live_cmd timeline_window =
  timeline_window##load Js.Undefined.empty
  |> Js.Promise.then_ (fun _ -> Js.Promise.resolve timeline_window)
  |. Tea_promise.result (loadedTimelineWindow None)

let load_timeline_window_event_cmd timeline_window event =
  let event_id = event##getId () in
  timeline_window##load (Js.Undefined.return event_id)
  |> Js.Promise.then_ (fun _ -> timeline_window##paginate "f" 40)
  |> Js.Promise.then_ (fun _ -> Js.Promise.resolve timeline_window)
  |. Tea_promise.result (loadedTimelineWindow (Some event))

let init room ?timeline_set maybe_event_id =
  ( { new_post_title= ""
    ; send_editor= Editor.init room##roomId
    ; scrolled_position= Down
    ; display= Matrix.room_type room
    ; timeline_views= Js.Dict.empty ()
    ; current_event= None }
  , init_room_cmd room maybe_event_id )

let paginate_backwards_cmd timeline_window scroll_height =
  timeline_window##paginate "b" 40
  (* |> Js.Promise.then_ (fun result -> *)
  (*        let _ = *)
  (*          Scroll.timeout_scroll_down ~offset:(Scroll.current_height ()) () *)
  (*        in *)
  (*        Js.Promise.resolve result ) *)
  |. Tea_promise.result (paginatedBackwards scroll_height)

let change_display_cmd display room =
  let content : string Js.Dict.t = Js.Dict.fromList [("display", display)] in
  Tea_promise.result
    (Matrix.client##setRoomAccountData
       room##roomId "org.technostructures.posca.display" content )
    changedDisplay

let send_message_cmd message room =
  let content =
    match message with
    | Editor.Plain text ->
        Js.Dict.fromList [("msgtype", "m.text"); ("body", text)]
    | Html (text, html) ->
        Js.Dict.fromList
          [ ("msgtype", "m.text")
          ; ("body", text)
          ; ("format", "org.matrix.custom.html")
          ; ("formatted_body", html) ]
  in
  Tea_promise.result
    (Matrix.client##sendMessage room##roomId content)
    sentMessage

let scroll_down_cmd ?offset () =
  Tea_cmd.call (fun _enqueue ->
      let _ = Scroll.timeout_scroll_down ?offset () in
      () )

let keep_scrolled_down scrolled_position =
  match scrolled_position with
  | Scroll.Down ->
      scroll_down_cmd ()
  | _ ->
      Tea.Cmd.none

let init_scroll_cmd scrolled_position = function
  | Matrix.Chat ->
      keep_scrolled_down scrolled_position
  | Forum ->
      keep_scrolled_down scrolled_position
  | Statuses ->
      Tea.Cmd.none
  | Media ->
      Tea.Cmd.none
  | Space ->
      Tea.Cmd.none

let new_event_scroll_cmd scrolled_position = function
  | Matrix.Chat ->
      keep_scrolled_down scrolled_position
  | Forum ->
      keep_scrolled_down scrolled_position
  | Statuses ->
      Tea.Cmd.none
  | Media ->
      Tea.Cmd.none
  | Space ->
      Tea.Cmd.none

let paginated_scroll_cmd offset = function
  | Matrix.Chat ->
      scroll_down_cmd ~offset ()
  | Forum ->
      scroll_down_cmd ~offset ()
  | Statuses ->
      Tea.Cmd.none
  | Media ->
      Tea.Cmd.none
  | Space ->
      Tea.Cmd.none

let scrolled_cmd display direction timeline_window =
  let scroll_height = Scroll.current_height () in
  match (display, direction) with
  | Matrix.Chat, Scroll.Up ->
      paginate_backwards_cmd timeline_window scroll_height
  | Forum, Up ->
      paginate_backwards_cmd timeline_window scroll_height
  | Statuses, Down ->
      paginate_backwards_cmd timeline_window None
  | Media, Down ->
      paginate_backwards_cmd timeline_window None
  | _ ->
      Tea.Cmd.none

let redact_cmd matrix_event =
  let room_id = matrix_event##getRoomId () in
  Tea_promise.result
    (Matrix.client##redactEvent room_id (matrix_event##getId ()))
    sentMessage

let filter_context model timeline_window events =
  match model.current_event with
  | None ->
      events
  | Some event ->
      if
        Matrix.client##supportsThreads ()
        && Js.Undefined.testAny timeline_window##timelineSet##thread
      then [|event|]
      else events

let filter_displayable model events =
  Belt.Array.keep events (fun event ->
      match
        ( event##isRedaction ()
        , event##getContent () = [%raw {|{}|}]
        , event##isRelation (Js.Undefined.return "m.room.redaction") ()
        , event##isRelation (Js.Undefined.return "m.replace") ()
        , event##getType () )
      with
      | false, false, false, false, "m.room.message" ->
          true
      | _ ->
          false )

let build_event_tree model events =
  match model.current_event with
  | Some event ->
      Event.build_tree events |> Event.filter_for event
  | None ->
      if Matrix.client##supportsThreads () then
        Event.build_tree_from_thread_roots events
      else Event.build_tree events

let init_events events timeline_window model =
  filter_context model timeline_window events
  |> filter_displayable model |> build_event_tree model

let get_current_timeline_window room model =
  let thread_id =
    Belt.Option.flatMap model.current_event (fun event ->
        event##getThread () |> Js.Undefined.toOption )
    |. Belt.Option.map (fun thread -> thread##id)
    |. Belt.Option.getWithDefault ""
  in
  match Js.Dict.get model.timeline_views thread_id with
  | Some tv ->
      tv.timeline_window
  | None ->
      (* this should not happen? *)
      Matrix_sdk.Timeline_window.create Matrix.client
        (room##getUnfilteredTimelineSet ())

let update_events_for_current_view model f =
  let thread_id =
    Belt.Option.flatMap model.current_event (fun event ->
        event##getThread () |> Js.Undefined.toOption )
    |. Belt.Option.map (fun thread -> thread##id)
    |. Belt.Option.getWithDefault ""
  in
  match Js.Dict.get model.timeline_views thread_id with
  | Some tw ->
      Js.Dict.set model.timeline_views thread_id {tw with events= f tw.events}
  | None ->
      ()

let update_events_cmd_for_current_view model f =
  let thread_id =
    Belt.Option.flatMap model.current_event (fun event ->
        event##getThread () |> Js.Undefined.toOption )
    |. Belt.Option.map (fun thread -> thread##id)
    |. Belt.Option.getWithDefault ""
  in
  match Js.Dict.get model.timeline_views thread_id with
  | Some tw ->
      let events, cmd = f tw.events in
      Js.Dict.set model.timeline_views thread_id {tw with events} ;
      cmd
  | None ->
      Tea.Cmd.none

let update_events_for_relevant_views model event f =
  (* Js.log @@ event##getContent (); *)
  let () =
    event##getThread ()
    |> Js.Undefined.toOption
    |. Belt.Option.map (fun thread -> thread##id)
    |. Belt.Option.forEach (fun thread_id ->
           match Js.Dict.get model.timeline_views thread_id with
           | Some tw ->
               Js.Dict.set model.timeline_views thread_id
                 {tw with events= f tw.events}
           | None ->
               () )
  in
  match Js.Dict.get model.timeline_views "" with
  | Some tw ->
      Js.Dict.set model.timeline_views "" {tw with events= f tw.events}
  | None ->
      ()

let maybe_thread_event room event = function
  | Matrix.Chat ->
      ()
  | _ -> (
    match event##getThread () |> Js.Undefined.toOption with
    | Some _ ->
        ()
    | None ->
        room##createThread (event##getId ()) event [||] |> ignore )

let maybe_add_post_title model message_body =
  match (model.new_post_title, message_body) with
  | "", message_body ->
      message_body
  | title, Editor.Plain text ->
      Html
        ( "## " ^ title ^ "\n\n" ^ text
        , "<h2>" ^ title ^ "</h2><p>" ^ text ^ "</p>" )
  | title, Editor.Html (text, html) ->
      Html ("## " ^ title ^ "\n\n" ^ text, "<h2>" ^ title ^ "</h2>" ^ html)

let update room model = function
  | GoTo _ ->
      (model, Tea.Cmd.none)
  | SetCurrent None ->
      let cmd =
        match Js.Dict.get model.timeline_views "" with
        | Some tw ->
            Tea.Cmd.none
        | None ->
            let timeline_window =
              Matrix_sdk.Timeline_window.create Matrix.client
                (room##getUnfilteredTimelineSet ())
            in
            load_timeline_window_live_cmd timeline_window
      in
      ({model with current_event= None}, cmd)
  | SetCurrent (Some event_id) ->
      (model, fetch_event_cmd room event_id)
  | InitializedRoom None ->
      let timeline_set = room##getUnfilteredTimelineSet () in
      let timeline_window =
        Matrix_sdk.Timeline_window.create Matrix.client timeline_set
      in
      (model, load_timeline_window_live_cmd timeline_window)
  | InitializedRoom (Some event_id) ->
      (model, fetch_event_cmd room event_id)
  | SavePostTitle title ->
      ({model with new_post_title= title}, Tea.Cmd.none)
  | ChangeDisplay display ->
      (* Js.log display ; *)
      let modal = Html.get_element_by_id "display_modal" in
      let () = Html.close_modal_unsafe modal in
      let cmd = change_display_cmd display room in
      ({model with display= string_to_display display}, cmd)
  | ShowDisplayModal ->
      let modal = Html.get_element_by_id "display_modal" in
      let () = Html.show_modal_unsafe modal in
      (model, Tea.Cmd.none)
  | HideDisplayModal ->
      let modal = Html.get_element_by_id "display_modal" in
      let () = Html.close_modal_unsafe modal in
      (model, Tea.Cmd.none)
  | ShowSendBox ->
      let send_editor, _cmd = Editor.update model.send_editor Show in
      ({model with send_editor}, Tea.Cmd.none)
  | HideSendBox ->
      let send_editor, _cmd = Editor.update model.send_editor Hide in
      ({model with send_editor}, Tea.Cmd.none)
  | Scrolled direction ->
      (* let cmd = scrolled_cmd model.display direction room in *)
      (* let cmd = *)
      (*   match model.current_event with *)
      (*   | None -> *)
      (*       scrolled_cmd model.display direction model.timeline_window *)
      (*   | Some _ -> *)
      (*       Tea.Cmd.none *)
      (* in *)
      let timeline_window = get_current_timeline_window room model in
      let cmd = scrolled_cmd model.display direction timeline_window in
      ({model with scrolled_position= direction}, cmd)
  | FetchedEvent (Ok event) ->
      maybe_thread_event room event model.display ;
      let thread = event##getThread () |> Js.Undefined.toOption in
      let thread_id =
        match thread with Some thread -> thread##id | None -> ""
      in
      let timeline_window =
        match Js.Dict.get model.timeline_views thread_id with
        | Some {timeline_window; _} ->
            timeline_window
        | None ->
            let timeline_set =
              match thread with
              | Some thread ->
                  thread##getUnfilteredTimelineSet ()
              | None ->
                  room##getUnfilteredTimelineSet ()
            in
            Matrix_sdk.Timeline_window.create Matrix.client timeline_set
      in
      ( {model with current_event= Some event}
      , load_timeline_window_event_cmd timeline_window event )
  | FetchedEvent (Error err) ->
      let () = Js.log "EVENT NOT FOUND" in
      let () = Js.log err in
      (model, Tea.Cmd.none)
  | ChangedDisplay (Ok res) ->
      let () = Js.log res in
      (model, Tea.Cmd.none)
  | ChangedDisplay (Error err) ->
      let () = Js.log err in
      (model, Tea.Cmd.none)
  | SentMessage (Ok res) ->
      let () = Js.log res in
      (model, Tea.Cmd.none)
  | SentMessage (Error err) ->
      let () = Js.log err in
      (model, Tea.Cmd.none)
  | PaginatedBackwards (offset, Ok res) ->
      let () = Js.log res in
      (model, paginated_scroll_cmd offset model.display)
  | PaginatedBackwards (_, Error err) ->
      let () = Js.log err in
      (model, Tea.Cmd.none)
  | Focus (event_id, parent_id) ->
      update_events_for_current_view model (fun events ->
          Event.focus_list events event_id parent_id ) ;
      (model, Tea.Cmd.none)
  | Redact event ->
      (model, redact_cmd event)
  | EventMsg (matrix_event, event_msg) ->
      let cmd =
        update_events_cmd_for_current_view model (fun events ->
            Event.update_events events matrix_event event_msg )
      in
      (model, Tea.Cmd.map (eventMsg matrix_event) cmd)
  | GotEvent (matrix_event, is_current) ->
      update_events_for_relevant_views model matrix_event (fun events ->
          Event.add_event events matrix_event ) ;
      let cmd = new_event_scroll_cmd model.scrolled_position model.display in
      (* Js.log events ; *)
      (* Js.log model.timeline_windows ; *)
      (model, cmd)
  | LoadedTimelineWindow (current_event, Ok timeline_window) ->
      (* Js.log room; *)
      Js.log timeline_window ;
      Js.log @@ timeline_window##getEvents () ;
      let model = {model with current_event} in
      let thread_id =
        Belt.Option.flatMap current_event (fun event ->
            event##getThread () |> Js.Undefined.toOption )
        |. Belt.Option.map (fun thread -> thread##id)
        |. Belt.Option.getWithDefault ""
      in
      let events =
        init_events (timeline_window##getEvents ()) timeline_window model
      in
      Js.log events ;
      let () =
        Js.Dict.set model.timeline_views thread_id
          {timeline_window; events; status= Loaded}
      in
      (* Js.log events ; *)
      (model, Tea.Cmd.none)
  | LoadedTimelineWindow (maybe_event_id, Error err) ->
      Js.log err ; (model, Tea.Cmd.none)
  | EditorMsg Send -> (
    match Editor.get_content model.send_editor with
    | Plain "" | Html ("", _) ->
        (model, Tea.Cmd.none)
    | _ as message_body ->
        let message_body = maybe_add_post_title model message_body in
        let cmd = send_message_cmd message_body room in
        let send_editor = Editor.after_send model.send_editor in
        ({model with send_editor; new_post_title= ""}, Tea.Cmd.none) )
  | EditorMsg editor_msg ->
      let send_editor, editor_cmd =
        Editor.update model.send_editor editor_msg
      in
      ({model with send_editor}, Tea.Cmd.map editorMsg editor_cmd)
