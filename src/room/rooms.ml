type msg =
  | GoTo of Router.route
  | SetCurrent of Matrix.room_id * Matrix.event_id option
  | GotRoomId of (Matrix.room_id * Matrix.event_id option, string) result
  | Peeked of (Matrix.room * Matrix.event_id option, string) result
  | AddRoom of Matrix.room
  | RoomMsg of Matrix.room * Room.msg
  | GotMessage of Matrix.matrix_event * Matrix.room Js.Nullable.t
  | GotAccountData of Matrix.matrix_event * Matrix.room * Matrix.matrix_event
  | GotRedaction of Matrix.matrix_event * Matrix.room
[@@deriving accessors]

let msg_to_string = function
  | SetCurrent _ ->
      "SetCurrentRoom"
  | GoTo _ ->
      "GoTo"
  | GotRoomId _ ->
      "GotRoomId"
  | Peeked _ ->
      "Peeked"
  | AddRoom _ ->
      "AddRoom"
  | RoomMsg (_, room_msg) ->
      Room.msg_to_string room_msg
  | GotMessage (_, _) ->
      "GotMessage"
  | GotAccountData (_, _, _) ->
      "GotAccountData"
  | GotRedaction (_, _) ->
      "GotRedaction"

type current_room = NoRoom | Room of Matrix.room | Error of string

type model =
  { (* { new_messages: string Js.Dict.t *)
    (* ; scrolled_positions: scroll_position Js.Dict.t *)
    (* ; current_display: string Js.Dict.t *)
    rooms: Room.model Js.Dict.t
  ; current_room: current_room }

let string_of_option = function Some str -> str | None -> ""

let init () = {current_room= NoRoom; rooms= Js.Dict.empty ()}
(* ; current_display= Js.Dict.empty () *)
(* ; new_messages= Js.Dict.empty () *)
(* ; scrolled_positions= Js.Dict.empty () } *)

(* let get_display model room = *)
(* let account_data = room##getAccountData "display" in *)
(* Js.log "get display"; *)
(* Js.log room; *)
(* Js.log account_data; *)
(* "chat" *)
(* Js.Dict.get model.current_display room##roomId |> string_of_option *)

let subscriptions model =
  Tea.Sub.batch
    [ Matrix.subscribe_to_timeline gotMessage
    ; Matrix.subscribe_to_room_accountdata gotAccountData
    ; Matrix.subscribe_to_room_redaction gotRedaction ]

let peek_room_cmd room_id maybe_event_id msg =
  (* if Matrix.client##isLoggedIn () then *)
  Matrix.client##peekInRoom room_id
  |> Js.Promise.then_ (fun room -> Js.Promise.resolve (room, maybe_event_id))
  |. Tea_promise.result msg

let resolve_alias_cmd room_alias msg =
  Tea_promise.result (Matrix.client##getRoomIdForAlias room_alias) msg

let set_route model = function
  | Matrix.Id room_id, maybe_event_id ->
      Tea.Cmd.msg (SetCurrent (room_id, maybe_event_id))
  | Alias room_alias, maybe_event_id ->
      Matrix.client##getRoomIdForAlias room_alias
      |> Js.Promise.then_ (fun room_obj ->
             Js.Promise.resolve (room_obj##room_id, maybe_event_id) )
      |. Tea_promise.result gotRoomId

(* | (Matrix.Id room_id, None) -> *)
(*     Tea.Cmd.msg (SetCurrentRoom room_id) *)
(* | (Matrix.Id room_id, Some event_id) -> *)
(*     Tea.Cmd.batch [ *)
(*     Tea.Cmd.msg (SetCurrentRoom room_id) *)
(*     ; Tea.Cmd.msg (SetCurrentEvent event_id) *)
(*     ] *)
(* | (Alias room_alias, None) -> *)
(*     resolve_alias_cmd room_alias *)
(* | (Alias room_alias, Some event_id) -> *)
(*     Tea.Cmd.batch [ *)
(*     resolve_alias_cmd room_alias *)
(*     ; Tea.Cmd.msg (SetCurrentEvent event_id) *)
(*     ] *)

let update model = function
  | GoTo _ ->
      (model, Tea.Cmd.none)
  | SetCurrent (room_id, maybe_event_id) -> (
      let room_in_store =
        Matrix.client##getRoom room_id |> Js.Nullable.toOption
      in
      match room_in_store with
      | Some room ->
          let room_model, room_cmd =
            match Js.Dict.get model.rooms room_id with
            | Some room_model ->
                Room.update room room_model @@ Room.SetCurrent maybe_event_id
            | None ->
                Room.init room maybe_event_id
          in
          Js.Dict.set model.rooms room_id room_model ;
          ( {model with current_room= Room room}
          , Tea.Cmd.map (roomMsg room) room_cmd )
      | None ->
          if Matrix.client##isLoggedIn () then
            let _ = Js.log "logged" in
            (model, peek_room_cmd room_id maybe_event_id peeked)
          else
            let _ = Js.log "not logged" in
            (model, Tea_time.delay 1000.0 (SetCurrent (room_id, maybe_event_id)))
      )
  | GotRoomId (Ok (room_id, maybe_event_id)) ->
      (model, Tea.Cmd.msg (SetCurrent (room_id, maybe_event_id)))
  | GotRoomId (Error err) ->
      let () = Js.log err in
      ({model with current_room= Error err}, Tea.Cmd.none)
  | Peeked (Ok (room, maybe_event_id)) ->
      let room_model, room_cmd = Room.init room maybe_event_id in
      Js.Dict.set model.rooms room##roomId room_model ;
      (* ( {model with current_room= Some room} *)
      (* , first_scroll_down room_model.scrolled_position room ) *)
      ({model with current_room= Room room}, Tea.Cmd.map (roomMsg room) room_cmd)
      (* , Tea.Cmd.msg (RoomMsg (room, Room.SetCurrent maybe_event_id)) ) *)
  | Peeked (Error err) ->
      let () = Js.log "ERROR WHEN PEEKING:" in
      let () = Js.log err in
      ({model with current_room= Error err}, Tea.Cmd.none)
  | AddRoom room ->
      let room_model, room_cmd =
        match Js.Dict.get model.rooms room##roomId with
        | Some room_model ->
            (room_model, Tea.Cmd.none)
        | None ->
            Room.init room None
      in
      Js.Dict.set model.rooms room##roomId room_model ;
      (model, Tea.Cmd.map (roomMsg room) room_cmd)
  | GotMessage (event, nullable_room) ->
      (* let () = Js.log event in *)
      let cmd =
        match (nullable_room |> Js.Nullable.toOption, event##getType ()) with
        (* | Some current_room, Some event_room, "m.room.message" *)
        (*   when current_room = event_room -> *)
        (*     Tea.Cmd.batch *)
        (*     ( *)
        (*   match Js.Dict.get model.rooms current_room##roomId with *)
        (*   | Some room_model when room_model.scrolled_position = Down -> *)
        (*       scroll_down_cmd *)
        (*   | _ -> *)
        (*       Tea.Cmd.none ) *)
        | Some event_room, "m.room.message" ->
            let is_current = model.current_room = Room event_room in
            Tea.Cmd.msg (RoomMsg (event_room, Room.GotEvent (event, is_current)))
        | _ ->
            Tea.Cmd.none
      in
      (model, cmd)
  | GotRedaction (event, room) ->
      (model, Tea.Cmd.none)
  | GotAccountData (event, room, previous_event) ->
      let event_type = event##getType () in
      let event_content = event##getContent () in
      (* Js.log event_type ; *)
      (* Js.log event_content ; *)
      (* let display = [%raw {|event_content.display|}] in *)
      ( match (event_type, Js.Dict.get event_content "display") with
      | "org.technostructures.posca.display", Some display -> (
        (* Js.log @@ string_to_display display ; *)
        (* Js.log model.rooms ; *)
        match Js.Dict.get model.rooms room##roomId with
        | Some room_model ->
            Js.Dict.set model.rooms room##roomId
              {room_model with display= Room.string_to_display display}
        (* Js.log model.rooms *)
        | None ->
            () )
      | _ ->
          () ) ;
      (model, Tea.Cmd.none)
  | RoomMsg (_room, GoTo route) ->
      (model, Tea.Cmd.msg (GoTo route))
  | RoomMsg (room, msg) ->
      let before_room_model, before_room_cmd =
        match Js.Dict.get model.rooms room##roomId with
        | Some room_model ->
            (room_model, Tea.Cmd.none)
        | None ->
            Room.init room None
      in
      (* Js.Dict.set model.rooms room##roomId before_room_model ; *)
      let new_room_model, new_room_cmd =
        Room.update room before_room_model msg
      in
      let () = Js.Dict.set model.rooms room##roomId new_room_model in
      ( model
      , Tea.Cmd.map (roomMsg room)
          (Tea.Cmd.batch [before_room_cmd; new_room_cmd]) )

(* let view model = function *)
(*   | Router.Room _ -> *)
(*       match model.current_room with *)
(*       | Some room -> room_view room (Js.Dict.get model.new_messages room##roomId |> *)
(*       string_of_option) *)
(*       | None -> Tea.Html.div [] [] *)
