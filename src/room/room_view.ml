open Room

let display_to_icon = function
  | Matrix.Space ->
      "folder-open"
  | Chat ->
      "message" (* "mail" *)
  | Forum ->
      "edit"
  | Statuses ->
      "heart"
      (* "twitter" *)
      (* "microphone" *)
  | Media ->
      "video"
(* "upload" *)

let display_modal model room =
  let room_id = room##roomId in
  (*  let open Tea.Html in let open Tea.Html.Attributes in *)
  let current_display = model.display in
  let is_current_display display =
    current_display = string_to_display display
  in
  let default_display = Matrix.room_default_type room in
  let other_displays =
    Belt.List.keep [Matrix.Chat; Forum; Statuses; Media] (fun display ->
        display != current_display )
  in
  let button_text =
    if current_display = default_display then display_to_string current_display
    else
      display_to_string current_display
      ^ " (default: "
      ^ display_to_string default_display
      ^ ")"
  in
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let other_buttons =
    Belt.List.map other_displays (fun display ->
        let classes =
          "block cursor-pointer overflow-hidden hover:bg-dominant-dark \
           active:bg-deco-blue "
          ^ if default_display = display then "font-bold" else ""
        in
        let msg = ChangeDisplay (display_to_identifier_string display) in
        let text =
          if default_display = display then
            "display as " ^ display_to_string display ^ " (default)"
          else "display as " ^ display_to_string display
        in
        li
          [class' classes]
          [ Components.button_link ~classes:"justify-center" ~msg
              ~icon:(display_to_icon display) text ] )
  in
  Vdom.fullnode "" "dialog" "" ""
    [ id "display_modal"
    ; class'
        "overflow-hidden rounded-xl border-2 bg-dominant-soft-dark \
         text-cardboard-gray" ]
    [ div
        [class' "flex justify-between border-b-2"]
        [ h3 [class' "m-4 text-center text-xl font-bold"] [text "Change display"]
        ; button
            [ class' "float-right m-4"
            ; Icons.aria_label "Close"
            ; title "Close"
            ; Tea.Html.Events.onClick hideDisplayModal ]
            [Icons.icon "close"] ]
    ; ul [class' ""] other_buttons ]

let dispatch_display model room events = function
  | Matrix.Chat ->
      Chat_view.view model room events
  | Media ->
      Media_view.view model room events
  | Statuses ->
      Statuses_view.view model room events
  | Forum ->
      Forum_view.view model room events
  | _ ->
      Chat_view.view model room events

let is_thread_view model =
  match (model.display, model.current_event) with
  | _, None ->
      false
  | Matrix.Chat, _ ->
      false
  | _, Some _ ->
      true

let view model room =
  (* let () = Js.log model.events in *)
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let avatar_view classes =
    let homeserver_url = Matrix.client##getHomeserverUrl () in
    match
      room##getAvatarUrl homeserver_url 100 100 "scale" |> Js.Nullable.toOption
    with
    | Some url ->
        img
          [ class'
              ( "mr-2 inline-block rounded-full border-2 \
                 border-dominant-soft-dark " ^ classes )
          ; src url ]
          []
    | None ->
        noNode
  in
  let topic_view =
    let live_timeline = room##getLiveTimeline () in
    let live_state = live_timeline##getState "f" in
    match live_state |> Js.Undefined.toOption with
    | None ->
        noNode
    | Some state -> (
        let topic_events = state##getStateEvents "m.room.topic" in
        match Belt.Array.get topic_events 0 with
        | None ->
            noNode
        | Some topic_event -> (
            let topic_content = topic_event##getContent () in
            let parsed_content =
              Matrix_sdk.Content_helpers.parse_topic_content topic_content
            in
            let classes = "italic text-cardboard-gray/50" in
            match parsed_content##html |> Js.Undefined.toOption with
            | None ->
                div [class' classes] [text parsed_content##text]
            | Some s ->
                div [class' classes; Vdom.prop "innerHTML" s] [] ) )
  in
  let room_dropdown =
    Components.ClickDropdown.element "mr-2 font-light"
      ~items:
        [ Components.button_link ~msg:showDisplayModal ~icon:"eye"
            "Change display" ]
      ~button_icon:"room-menu" ~button_label:"More"
  in
  let spinner_view =
    div
      [class' "flex h-full place-content-center items-center"]
      [ span
          [ id "room-loading-spinner"
          ; class'
              "h-32 w-32 animate-spin rounded-full border-8 border-paper-white \
               border-b-beware-orange" ]
          [] ]
  in
  (* Js.log model.current_event; *)
  (* Js.log model.timeline_views; *)
  let timeline_view =
    let thread_id =
      Belt.Option.flatMap model.current_event (fun event ->
          event##getThread () |> Js.Undefined.toOption )
      |. Belt.Option.map (fun thread -> thread##id)
      |. Belt.Option.getWithDefault ""
    in
    Js.Dict.get model.timeline_views thread_id
  in
  let events = match timeline_view with Some tv -> tv.events | None -> [||] in
  let display_if_loaded display_callback =
    match timeline_view with
    | Some {status= Loaded; _} ->
        display_callback ()
    | _ ->
        [spinner_view]
  in
  let room_header =
    if is_thread_view model then
      div
        [ class'
            "mx-4 flex items-center justify-between border-dominant-soft-dark \
             pb-2 pt-4" ]
        [ Components.rpill_button ~icon_only:true
            (goTo (Room (Id room##roomId, None)))
            "arrow-left" "Back to room"
        ; avatar_view "h-7 w-7"
        ; div
            [class' "w-full"]
            [ h3
                [class' "mr-4 inline-block text-2xl font-bold"]
                [text room##name] ]
        ; room_dropdown ]
    else
      div
        [ class'
            "mx-4 flex items-center justify-between border-dominant-soft-dark \
             pb-2 pt-4" ]
        [ avatar_view "h-10 w-10"
        ; div
            [class' "w-full"]
            [ h3
                [class' "mr-4 inline-block text-4xl font-bold"]
                [text room##name]
            ; topic_view ]
        ; room_dropdown ]
  in
  (* Js.log events ; *)
  (* Js.log model.timeline_views; *)
  div ~unique:"chat"
    [class' "flex h-full flex-col"]
    ( [display_modal model room; room_header]
    @ display_if_loaded (fun () ->
          dispatch_display model room events model.display ) )
