open Room

let thread_preview room event =
  let matrix_event = event.Event.event in
  let open Tea.Html in
  let open Tea.Html.Attributes in
  match Matrix.parse_message matrix_event with
  | Text (Body body) ->
      noNode
  | Text (FormattedBody formatted_body) ->
      noNode
  | Image image_params ->
      let event_id = matrix_event##getId () in
      let img_tag =
        Router.link ~key:event_id goTo
          (Room (Id room##roomId, Some event_id))
          [ Matrix.image_to_html
              ~classes:"max-h-full min-w-full object-cover align-bottom"
              image_params ]
      in
      let sender_before =
        Common_view.message_card_sender
          ~additional_classes:"flex justify-around p-2 md:hidden" room event
      in
      let sender_over =
        Common_view.message_card_sender
          ~additional_classes:
            "message-sender absolute left-0 top-0 hidden w-full flex-wrap \
             bg-dominant-dark/50 p-2 opacity-0 transition-opacity duration-300 \
             group-hover:opacity-100 md:flex"
          ~large:false room event
      in
      let pills_over =
        div
          [ class'
              "absolute bottom-2 hidden w-full justify-around opacity-0 \
               transition-opacity duration-300 group-hover:opacity-100 md:flex"
          ]
          [ Common_view.replies_count_view room event
          ; Common_view.likes_count_view room event ]
      in
      let pills_after =
        div
          [class' "flex justify-around p-2 md:hidden"]
          [ Common_view.replies_count_view ~invert_colors:true room event
          ; Common_view.likes_count_view ~invert_colors:true room event ]
      in
      div
        ~unique:(matrix_event##getId ())
        [class' "flex grow flex-col"]
        [ sender_before
        ; div
            [ class' "group relative grow md:m-2 md:h-[30vh]"
            ; id (matrix_event##getId ()) ]
            [img_tag; sender_over; pills_over]
        ; pills_after ]

let thread_view model room events current_event =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let root_event = events.(0) in
  let matrix_event = root_event.Event.event in
  match Matrix.parse_message matrix_event with
  | Text (Body body) ->
      div []
        [ text
            "Thread is not an image thread, use another display mode to see it."
        ]
  | Text (FormattedBody formatted_body) ->
      div []
        [ text
            "Thread is not an image thread, use another display mode to see it."
        ]
  | Image image_params ->
      let event_id = matrix_event##getId () in
      let img_tag =
        Matrix.image_to_html ~classes:"max-h-full max-w-full" ~width:None
          ~height:None ~resize_method:None image_params
      in
      (* let from_tag = *)
      (*   span *)
      (*     [ class' *)
      (* "message-sender \ *) (* absolute right-2 top-2
         opacity-0          transition-opacity duration-300 group-hover:opacity-100" ] *)
      (*     [ text "from " *)
      (*     ; span [class' "font-bold"] [text matrix_event##sender##rawDisplayName] *)
      (*     ] *)
      (* in *)
      let pills_after =
        div
          [class' "hidden justify-around md:flex"]
          [ Common_view.replies_count_view ~invert_colors:true room root_event
          ; Common_view.likes_count_view ~invert_colors:true room root_event ]
      in
      let comments_part =
        let message_list =
          root_event.children
          |. Belt.Array.map (fun event ->
                 (* comment_view event |> Vdom.map (eventMsg event.Event.event) ) *)
                 Common_view.tree_of_cards ~large:false model room event None
                   false noNode noNode )
          |> Belt.List.fromArray
        in
        div [class' ""] message_list
      in
      div
        ~unique:(matrix_event##getId ())
        [class' "flex grow flex-col overflow-auto md:flex-row"]
        [ div
            [ class' "flex grow items-center justify-center"
            ; id (matrix_event##getId ()) ]
            [img_tag]
          (* bg-dominant-soft-dark *)
        ; div
            [class' ""]
            [ div
                [class' "max-h-full overflow-y-auto md:w-[500px]"]
                [ Common_view.message_card_sender room root_event
                ; pills_after
                ; Common_view.thread_buttons
                    ~additional_classes:
                      "!left-8 !top-0 z-50 !float-none justify-center \
                       bg-dominant-dark py-2"
                    model room root_event
                ; comments_part ] ] ]

let view model room events =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  match model.current_event with
  | None ->
      let message_list =
        events |. Belt.Array.map (thread_preview room) |> Belt.List.fromArray
      in
      [ div
          ~unique:(room##roomId ^ "media")
          [ id "message-grid"
          ; class' "flex flex-wrap overflow-y-scroll"
          ; Scroll.on_scroll scrolled ]
          (message_list @ [div [class' "grow-[10]"] []]) ]
  | Some event ->
      [thread_view model room events event]
