type scroll_position = Up | Middle | Down

type html_element =
  < scrollTop: int [@mel.set] ; clientHeight: int ; scrollHeight: int > Js.t

let scrolled_position_element element =
  match element##scrollTop with
  | 0 ->
      Up
  | n when n = element##scrollHeight - element##clientHeight ->
      Down
  | _ ->
      Middle

let on_scroll ?(key = "") msg =
  Tea.Html.Events.onCB "scroll" ~key (fun event ->
      let target = Webapi.Dom.Event.target event in
      match scrolled_position_element (Obj.magic target) with
      | Down ->
          Some (msg Down)
      | Middle ->
          Some (msg Middle)
      | Up ->
          Some (msg Up) )

let scrolled_down_element element =
  element##scrollTop + element##clientHeight = element##scrollHeight

let scroll_down_element ?(offset = None) element =
  match offset with
  | None ->
      element ## scrollTop #= (element##scrollHeight - element##clientHeight)
  | Some o ->
      element ## scrollTop #= (element##scrollHeight - o)

external get_element_by_id : string -> html_element Js.Nullable.t
  = "getElementById"
[@@mel.scope "document"]

(* let scroll_down_cmd = *)
(*   Js.log "scrolling_down"; *)
(*   Tea.Cmd.none *)

let current_height () =
  match get_element_by_id "message-list" |> Js.Nullable.toOption with
  | Some elm ->
      Some elm##scrollHeight
  | None ->
      None

let timeout_scroll_down ?(offset = None) ?(time = 100) () =
  Js.Global.setTimeout
    ~f:(fun () ->
      let () =
        match get_element_by_id "message-list" |> Js.Nullable.toOption with
        | Some elm ->
            scroll_down_element ~offset elm
        | None ->
            ()
      in
      () )
    time
