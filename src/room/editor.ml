type default_editor = Markdown | Tiptap

let default_editor = ref Tiptap

type state = Closed | Md | Tt

type model =
  { key: string
  ; state: state
  ; md: string
  ; current_link: string
  ; tiptap: Tiptap.editor option }

type msg =
  | ToggleEditor of bool
  | ToggleBold
  | ToggleItalic
  | ToggleStrike
  | ToggleCode
  | ToggleBlockquote
  | ToggleCodeBlock
  | ToggleBulletList
  | ToggleOrderedList
  | ToggleTaskList
  | ToggleHeading of int
  | PromptLink
  | CancelLink
  | SetLink
  | SaveLink of string
  | SetHorizontalRule
  | SinkListItem
  | LiftListItem
  | Undo
  | Redo
  | GotInput
  | Hide
  | Show
  | MountedTiptap of Dom.element * bool
  | UnmountedTiptap
  | SaveMd of string
  | Send
[@@deriving accessors]

let init ?(opened = false) key =
  let state =
    if opened then match !default_editor with Markdown -> Md | Tiptap -> Tt
    else Closed
  in
  {key; state; tiptap= None; current_link= ""; md= ""}

let after_send model =
  let () =
    match model.tiptap with
    | Some editor ->
        editor##commands##setContent ""
    | None ->
        ()
  in
  {model with state= Closed; current_link= ""; md= ""}

let focus_and_exec fn editor =
  editor |> Tiptap.chain |> Tiptap.focus |> fn |> Tiptap.run

let maybe_focus_and_exec fn maybe_editor =
  match maybe_editor with Some editor -> focus_and_exec fn editor | None -> ()

type content = Html of string * string | Plain of string

let get_content model =
  match model.tiptap with
  | Some editor ->
      let md = (editor##storage).markdown##getMarkdown () in
      if Markdown.is_rich md then
        let html = editor##getHTML () in
        Html (md, html)
      else Plain md
  | None ->
      let md = model.md in
      if Markdown.is_rich md then
        let html = Markdown.as_html md in
        Html (md, html)
      else Plain md

let update model = function
  | ToggleEditor is_rich_text ->
      if is_rich_text then
        let () = default_editor := Tiptap in
        ({model with state= Tt}, Tea.Cmd.none)
      else
        let () = default_editor := Markdown in
        let md =
          match model.tiptap with
          | Some editor ->
              (editor##storage).markdown##getMarkdown ()
          | None ->
              ""
        in
        ({model with md; state= Md; tiptap= None}, Tea.Cmd.none)
  | ToggleBold ->
      let () = maybe_focus_and_exec Tiptap.toggle_bold model.tiptap in
      (model, Tea.Cmd.none)
  | ToggleItalic ->
      let () = maybe_focus_and_exec Tiptap.toggle_italic model.tiptap in
      (model, Tea.Cmd.none)
  | ToggleStrike ->
      let () = maybe_focus_and_exec Tiptap.toggle_strike model.tiptap in
      (model, Tea.Cmd.none)
  | ToggleCode ->
      let () = maybe_focus_and_exec Tiptap.toggle_code model.tiptap in
      (model, Tea.Cmd.none)
  | ToggleBlockquote ->
      let () = maybe_focus_and_exec Tiptap.toggle_blockquote model.tiptap in
      (model, Tea.Cmd.none)
  | ToggleCodeBlock ->
      let () = maybe_focus_and_exec Tiptap.toggle_code_block model.tiptap in
      (model, Tea.Cmd.none)
  | ToggleBulletList ->
      let () = maybe_focus_and_exec Tiptap.toggle_bullet_list model.tiptap in
      (model, Tea.Cmd.none)
  | ToggleOrderedList ->
      let () = maybe_focus_and_exec Tiptap.toggle_ordered_list model.tiptap in
      (model, Tea.Cmd.none)
  | ToggleTaskList ->
      let () = maybe_focus_and_exec Tiptap.toggle_task_list model.tiptap in
      (model, Tea.Cmd.none)
  | ToggleHeading level ->
      let () =
        maybe_focus_and_exec (Tiptap.toggle_heading {Tiptap.level}) model.tiptap
      in
      (model, Tea.Cmd.none)
  | SetHorizontalRule ->
      let () = maybe_focus_and_exec Tiptap.set_horizontal_rule model.tiptap in
      (model, Tea.Cmd.none)
  | PromptLink ->
      let current_link =
        match model.tiptap with
        | None ->
            ""
        | Some editor -> (
          match
            (editor##getAttributes "link").href |> Js.Undefined.toOption
          with
          | None ->
              ""
          | Some link ->
              link )
      in
      let modal = Html.get_element_by_id "link_modal" in
      let () = Html.show_modal_unsafe modal in
      let input = Html.get_element_by_id "link_input" in
      let () = Html.focus input in
      ({model with current_link}, Tea.Cmd.none)
  | CancelLink ->
      let modal = Html.get_element_by_id "link_modal" in
      let () = Html.close_modal_unsafe modal in
      ({model with current_link= ""}, Tea.Cmd.none)
  | SaveLink current_link ->
      ({model with current_link}, Tea.Cmd.none)
  | SetLink ->
      let () =
        if model.current_link = "" then
          maybe_focus_and_exec
            (fun e -> e |> Tiptap.extend_mark_range "link" |> Tiptap.unset_link)
            model.tiptap
        else
          maybe_focus_and_exec
            (fun e ->
              e
              |> Tiptap.extend_mark_range "link"
              |> Tiptap.set_link {href= Js.Undefined.return model.current_link}
              )
            model.tiptap
      in
      let modal = Html.get_element_by_id "link_modal" in
      let () = Html.close_modal_unsafe modal in
      ({model with current_link= ""}, Tea.Cmd.none)
  | SinkListItem ->
      let () =
        maybe_focus_and_exec (Tiptap.sink_list_item "listItem") model.tiptap
      in
      (model, Tea.Cmd.none)
  | LiftListItem ->
      let () =
        maybe_focus_and_exec (Tiptap.lift_list_item "listItem") model.tiptap
      in
      (model, Tea.Cmd.none)
  | Undo ->
      let () = maybe_focus_and_exec Tiptap.undo model.tiptap in
      (model, Tea.Cmd.none)
  | Redo ->
      let () = maybe_focus_and_exec Tiptap.redo model.tiptap in
      (model, Tea.Cmd.none)
  | Hide ->
      ({model with state= Closed}, Tea.Cmd.none)
  | Show ->
      let state = match !default_editor with Markdown -> Md | Tiptap -> Tt in
      ({model with state}, Tea.Cmd.none)
  | MountedTiptap (elem, send_on_enter) ->
      let editor = Tiptap.new_editor ~send_on_enter elem in
      let () =
        if model.md <> "" then editor##commands##setContent model.md else ()
      in
      ({model with tiptap= Some editor}, Tea.Cmd.none)
  | SaveMd md ->
      ({model with md}, Tea.Cmd.none)
  | Send ->
      failwith "gated Editor Send msg: shoud not happen"
  | GotInput ->
      (model, Tea.Cmd.none)
  | UnmountedTiptap ->
      let md =
        match model.tiptap with
        | Some editor ->
            (editor##storage).markdown##getMarkdown ()
        | None ->
            model.md
      in
      ({model with md; tiptap= None}, Tea.Cmd.none)

let send_property send_on_enter =
  if send_on_enter then Html.on_enter send else Html.on_ctrl_enter send

let markdown_editor ~send_on_enter ?(thin = false) ?(border = true) model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let height_class = if thin then "min-h-16" else "min-h-32" in
  textarea
    [ ( class'
      @@ "w-full flex-1 rounded-[18px] border-dominant-dark \
          bg-dominant-soft-dark p-2 text-cardboard-gray shadow-none \
          outline-none dark:text-cardboard-gray " ^ height_class ^ " "
      ^ if border then "border-2" else "" )
      (* ^ textarea_classes ) *)
    ; value model.md (* ; on_send send_msg *)
    ; send_property send_on_enter
    ; Tea.Html.Events.onInput ~key:model.key saveMd ]
    []

let is_active model mark =
  match model.tiptap with Some editor -> editor##isActive mark | None -> false

let is_active_heading model level =
  match model.tiptap with
  | Some editor ->
      Tiptap.is_active_heading editor "heading" {level}
  | None ->
      false

let can_undo model =
  match model.tiptap with
  | Some editor ->
      (editor##can ())##undo ()
  | None ->
      false

let can_redo model =
  match model.tiptap with
  | Some editor ->
      (editor##can ())##redo ()
  | None ->
      false

let can_sink_list_item model =
  match model.tiptap with
  | Some editor ->
      (editor##can ())##sinkListItem "listItem"
  | None ->
      false

let can_lift_list_item model =
  match model.tiptap with
  | Some editor ->
      (editor##can ())##liftListItem "listItem"
  | None ->
      false

let dropdown outer_button inside_buttons =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  div
    [class' "group m-0 flex flex-col justify-center"]
    [ outer_button
    ; div
        [ class'
            "dropdown invisible absolute z-50 hidden w-[34px] border-2 \
             border-t-0 border-dominant-dark bg-dominant-soft-dark \
             text-cardboard-gray opacity-0 hover:visible hover:block \
             hover:opacity-100 group-focus-within:visible \
             group-focus-within:block group-focus-within:opacity-100 \
             group-hover:visible group-hover:block group-hover:opacity-100" ]
        inside_buttons ]

let tiptap_editor ~send_on_enter ?(thin = false) ?(border = true) model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  let link_modal model =
    Vdom.fullnode "" "dialog" "" ""
      [ id "link_modal"
      ; class'
          "overflow-hidden rounded-xl border-2 bg-dominant-soft-dark \
           text-cardboard-gray" ]
      [ div
          [class' "flex justify-between border-b-2"]
          [ h3
              [class' "m-4 text-center text-xl font-bold"]
              [text "Set a link URL"]
          ; button
              [ class' "float-right m-4"
              ; Icons.aria_label "Close"
              ; title "Close"
              ; onClick cancelLink ]
              [Icons.icon "close"] ]
      ; Tea.Html.form
          [class' "m-10 flex flex-row"; onSubmit setLink]
          [ input'
              [ type' "text"
              ; class' "rounded-lg bg-dominant-dark p-1 outline-none"
              ; id "link_input"
              ; onInput saveLink ]
              [text model.current_link]
          ; button
              [class' "ml-4"; type' "submit"]
              [Icons.icon ~class':"h-5 w-5" "send2"] ] ]
  in
  let btn ?(active = false) ?(disabled = false) ?(bl = true) ?(br = true) icon
      title' msg =
    button
      [ classList
          [ ( "flex w-8 shrink-0 cursor-pointer justify-center \
               border-dominant-dark py-1 text-cardboard-gray/50 \
               hover:text-cardboard-gray disabled:cursor-default \
               disabled:text-cardboard-gray/20"
            , true )
          ; ("!text-cardboard-gray", active)
          ; ("border-r", br)
          ; ("border-l", bl) ]
      ; Attributes.disabled disabled
      ; title title'
      ; onClick msg ]
      [Icons.icon icon]
  in
  let buttons =
    [ btn ~active:(can_undo model)
        ~disabled:(not @@ can_undo model)
        ~bl:false ~br:false "arrow-go-back-line" "Undo (Ctrl+Z)" undo
    ; btn ~active:(can_redo model)
        ~disabled:(not @@ can_redo model)
        ~bl:false "arrow-go-forward-line" "Redo (Ctrl+Y)" redo
    ; dropdown
        (button
           [ class'
               "flex w-8 cursor-pointer justify-center border-l border-r \
                border-dominant-dark py-1 text-cardboard-gray/50 \
                hover:text-cardboard-gray disabled:cursor-default \
                disabled:text-cardboard-gray/20"
           ; title "Heading" ]
           [Icons.icon "heading"] )
        [ btn
            ~active:(is_active_heading model 1)
            ~bl:false ~br:false "h-1" "Heading level 1 (Ctrl+Alt+1)"
            (toggleHeading 1)
        ; btn
            ~active:(is_active_heading model 2)
            ~bl:false ~br:false "h-2" "Heading level 2 (Ctrl+Alt+2)"
            (toggleHeading 2)
        ; btn
            ~active:(is_active_heading model 3)
            ~bl:false ~br:false "h-3" "Heading level 3 (Ctrl+Alt+3)"
            (toggleHeading 3)
        ; btn
            ~active:(is_active_heading model 4)
            ~bl:false ~br:false "h-4" "Heading level 4 (Ctrl+Alt+4)"
            (toggleHeading 4)
        ; btn
            ~active:(is_active_heading model 5)
            ~bl:false ~br:false "h-5" "Heading level 5 (Ctrl+Alt+5)"
            (toggleHeading 5)
        ; btn
            ~active:(is_active_heading model 6)
            ~bl:false ~br:false "h-6" "Heading level 6 (Ctrl+Alt+6)"
            (toggleHeading 6) ]
    ; btn ~active:(is_active model "bold") ~br:false "bold" "Bold (Ctrl+B)"
        toggleBold
    ; btn ~active:(is_active model "italic") ~bl:false ~br:false "italic"
        "Italic (Ctrl+I)" toggleItalic
    ; btn ~active:(is_active model "strike") ~bl:false ~br:false "strikethrough"
        "Strikethrough (Ctrl+Shift+S)" toggleStrike
    ; btn ~active:(is_active model "code") ~bl:false ~br:false "code-view"
        "Code (Ctrl+E)" toggleCode
    ; btn ~active:(is_active model "link") ~bl:false "link" "Link (Ctrl+K)"
        promptLink
    ; btn
        ~active:(is_active model "blockquote")
        ~br:false "quote-text" "Quote (Ctrl+Shift+B)" toggleBlockquote
    ; btn
        ~active:(is_active model "codeBlock")
        ~bl:false "code-block" "Code block (Ctrl+Alt+C)" toggleCodeBlock
    ; btn
        ~active:(is_active model "orderedList")
        ~br:false "list-ordered-2" "Ordered list (Ctrl+Shift+7)"
        toggleOrderedList
    ; btn
        ~active:(is_active model "bulletList")
        ~bl:false ~br:false "list-unordered" "Bullet list (Ctrl+Shift+8)"
        toggleBulletList
    ; btn
        ~active:(is_active model "taskList")
        ~bl:false ~br:false "list-check-3" "Task list (Ctrl+Shift+9)"
        toggleTaskList
    ; btn ~active:(can_lift_list_item model)
        ~disabled:(not @@ can_lift_list_item model)
        ~bl:false ~br:false "indent-decrease" "Sink list item (Tab)"
        liftListItem
    ; btn ~active:(can_sink_list_item model)
        ~disabled:(not @@ can_sink_list_item model)
        ~bl:false "indent-increase" "Lift list item (Tab)" sinkListItem
    ; btn ~br:false "separator" "Separator" setHorizontalRule ]
  in
  let height_class = if thin then "min-h-8" else "min-h-24" in
  div []
    [ link_modal model
    ; div
        [class' "relative"]
        [ div
            [ ( class'
              @@ "flex w-full flex-row justify-center rounded-t-[18px] \
                  border-dominant-dark bg-dominant-soft-dark "
              ^ if border then "border-2" else "" ) ]
            [div [class' "flex flex-row overflow-x-auto"] buttons] ]
    ; div
        [ ( class'
          @@ "w-full flex-1 rounded-b-[18px] border-t-0 border-dominant-dark \
              bg-dominant-soft-dark " ^ height_class ^ " "
          ^ if border then "border-2" else "" )
        ; onMsg "input" gotInput
        ; Vdom.onMountCB (function
            | Some elem ->
                Some (MountedTiptap (elem, send_on_enter))
            | None ->
                None )
        ; Vdom.onUnmountMsg UnmountedTiptap
        ; Html.on_ctrl_enter send
        ; (if send_on_enter then Html.on_enter send else noProp)
        ; send_property send_on_enter
        ; onCB "keydown" ~key:"link_shortcut" (fun ev ->
              if [%raw {|ev.keyCode|}] = 75 && [%raw {|ev.ctrlKey|}] then
                let () = Webapi.Dom.Event.preventDefault ev in
                Some PromptLink
              else None ) ]
        [] ]

let editor_view ~send_on_enter ?(thin = false) ?(border = true) model =
  match !default_editor with
  | Markdown ->
      markdown_editor ~send_on_enter ~thin ~border model
  | Tiptap ->
      tiptap_editor ~send_on_enter ~thin ~border model

let view_inner ?(send_on_enter = false) ?(thin = false) ?(border = true) model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  div
    [class' "grow overflow-hidden"]
    [ editor_view ~send_on_enter ~thin ~border model
    ; div
        [class' "m-1"]
        [ label
            [class' "float-right flex cursor-pointer text-xs"]
            [ input'
                [ type' "checkbox"
                ; class'
                    "bg-current toggle toggle-xs mr-2 rounded-full \
                     border-contrast-dark/50 bg-contrast-dark/50 \
                     text-cardboard-gray checked:bg-cardboard-gray \
                     focus-visible:border-contrast-dark/50"
                ; name "editor-type"
                ; value "rich"
                ; Tea.Html.Events.onClick @@ toggleEditor (model.state = Md)
                ; checked (!default_editor = Tiptap) ]
                []
            ; text "Rich text" ] ] ]

let view ?(container_classes = "") ?(textarea_classes = "")
    ?(close_button = true) ?(send_on_enter = false) ?(thin = false) model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  let hide_button =
    if close_button then
      button
        [ class' "absolute right-0 top-3"
        ; Icons.aria_label "Close"
        ; title "Close"
        ; onClick hide ]
        [Icons.icon "close"]
    else noNode
  in
  div
    [class' ("relative flex shrink-0 " ^ container_classes)]
    [ hide_button
    ; view_inner ~send_on_enter ~thin model
    ; button
        [class' "ml-2"; type' "submit"; onClick send]
        [Icons.icon ~class':"h-5 w-5" "send2"] ]
