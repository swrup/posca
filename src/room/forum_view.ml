open Room

let message_view_sender ?(large = true) ?(additional_classes = "") room event =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  let avatar_view =
    let homeserver_url = Matrix.client##getHomeserverUrl () in
    let img =
      match
        matrix_event##sender##getAvatarUrl homeserver_url 100 100 "scale"
        |> Js.Nullable.toOption
      with
      | Some url ->
          img
            [ class'
                "h-10 w-10 rounded-full border-2 border-contrast-dark md:h-16 \
                 md:w-16"
            ; src url ]
            []
      | None ->
          noNode
    in
    div [class' "mr-2 h-10 w-10 shrink-0 md:h-16 md:w-16"] [img]
  in
  let user_name_view =
    div
      [class' "mr-2 flex-auto text-xl font-bold"]
      [text matrix_event##sender##rawDisplayName]
  in
  let user_id_view =
    div
      [class' "mr-2 flex-auto text-xs italic text-cardboard-gray/50"]
      [text matrix_event##sender##userId]
  in
  div
    [ class'
        ("flex items-center md:flex-col md:items-start " ^ additional_classes)
    ]
    [ avatar_view
    ; div [] [user_name_view; user_id_view]
    ; Common_view.time_view room matrix_event "my-2 ml-auto md:ml-0"
        "mr-2 text-sm italic text-alternate-gray" ]

let message_view_inner ?(large = true) room event parent is_indented left right
    =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  let is_focused = match event.show_children with All -> true | _ -> false in
  let message_body_view =
    div []
      [ a ~key:event_id
          [ class' "cursor-pointer"
          ; Tea.Html.Events.onClick @@ focus event_id parent ]
          [ Common_view.message_body ~should_remove_title:(event.level = 0)
              matrix_event ] ]
  in
  let reply_box =
    if room##maySendMessage () && event.reply_editor.state != Closed then
      Common_view.message_card_reply_box event event_id event.new_reply
    else noNode
  in
  let shows_children =
    match event.show_children with None -> false | _ -> true
  in
  div
    [ classList
        [ ("border-t-4 border-dominant-soft-dark", event.level = 1)
        ; ( "border-t-2 border-dominant-soft-dark"
          , event.level > 1 && not is_indented )
        ; ("grow p-5", true)
        ; ( "relative before:absolute before:bottom-0  before:left-0 \
             before:top-0 before:border-l-4 before:border-l-dominant-soft-dark"
          , event.level >= 1 && shows_children
            && Belt.Array.length event.children != 0 )
        ; ("bg-dominant-soft-dark", is_focused)
        ; ("border-contrast-dark bg-dominant-dark", not is_focused)
        ; ("ml-4", (not is_indented) && left = noNode)
        ; ("md:ml-5", (not is_indented) && left = noNode && large)
        ; ("mr-4", (not is_indented) && right = noNode)
        ; ("md:mr-5", (not is_indented) && right = noNode && large) ] ]
    (* [class' "min-w-full rounded-2xl bg-dominant-soft-dark"] *)
    [ div
        [class' "flex grow flex-col md:flex-row"]
        [ div
            [class' "pr-5"] (* flex flex-col justify-between *)
            [ message_view_sender ~large room event
            ; div
                [class' "hidden md:block"]
                [ Common_view.likes_count_view ~invert_colors:true room event
                ; Common_view.replies_count_view ~invert_colors:true room event
                ] ]
        ; div [class' "grow"] [message_body_view]
        ; div []
            [ div
                [class' "flex flex-row justify-around md:hidden"]
                [ Common_view.likes_count_view ~invert_colors:true room event
                ; Common_view.replies_count_view ~invert_colors:true room event
                ] ] ]
    ; reply_box ]

let rec message_view ?(large = true) model room event parent is_indented left
    right =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  let shows_children =
    match event.show_children with None -> false | _ -> true
  in
  let replies_view =
    let maybe_sibling_button icon index =
      match Belt.Array.get event.children index with
      | Some e ->
          div
            [class' "flex w-5 flex-row items-center"]
            [ a ~key:event_id
                [ class' "cursor-pointer"
                ; Tea.Html.Events.onClick
                  @@ eventMsg event.event (Event.showChild index) ]
                [Icons.icon ~class':"h-5 w-5" icon] ]
      | None ->
          noNode
    in
    match (event.show_children, event.level) with
    | All, _ | _, 0 ->
        div
          [ classList
              [ ("ml-5 mr-5", event.level > 0)
              ; ("md:ml-5", event.level > 0 && large)
              ; ( "relative before:absolute before:-top-5 before:bottom-0 \
                   before:border-l-4 before:border-l-dominant-soft-dark"
                , event.level >= 1 && shows_children
                  && Belt.Array.length event.children != 0 ) ] ]
          ( Belt.Array.map event.children (fun child ->
                message_view model room child (Some event_id) (event.level >= 1)
                  noNode noNode )
          |> Belt.List.fromArray )
    | One index, _ -> (
      match Belt.Array.get event.children index with
      | Some e ->
          div []
            [ message_view model room e (Some event_id) false
                (maybe_sibling_button "chevron-left" (index - 1))
                (maybe_sibling_button "chevron-right" (index + 1)) ]
      | None ->
          noNode )
    | None, _ ->
        noNode
  in
  div
    ~unique:(matrix_event##getId ())
    [ classList
        [ ( "before:absolute before:left-[4px] before:mt-8 before:h-0 \
             before:w-0 before:border-y-[8px] before:border-l-[8px] \
             before:border-y-[transparent] before:border-l-dominant-soft-dark"
          , is_indented )
          (* ; ("relative block", event.level <= 1) *)
        ; ("block", event.level > 1) ]
    ; id event_id ]
    [ div
        [class' "flex flex-row items-stretch"]
        [ left
        ; message_view_inner ~large room event parent is_indented left right
        ; right ]
    ; replies_view ]

let parse_post_content content =
  let regexp = [%re "/^#+ (.*?)\\n\\n(.*)/s"] in
  match Js.String.match_ ~regexp content with
  | Some [|_; Some title; Some body|] ->
      Some (title, body)
  | _ ->
      None

let filter_posts events =
  Belt.Array.keepMap events (fun event ->
      let content = event.Event.event##getContent () in
      match Js.Dict.get content "body" with
      | None ->
          None
      | Some body -> (
        match parse_post_content body with
        | Some (title, _body) ->
            Some (event, title)
        | None ->
            None ) )

let reply_root_box model room event key current_input =
  let open Tea.Html in
  if room##maySendMessage () && model.send_editor.state != Closed then
    Editor.view ~container_classes:"border-t-2 border-dominant-soft-dark"
      ~textarea_classes:"h-40" event.Event.reply_editor
    |> Vdom.map (fun msg -> eventMsg event.Event.event (Event.editorMsg msg))
  else noNode

let send_box model room =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  (* Js.log model.new_message; *)
  (* let (thread_title, thread_body) = match parse_post_content model.new_message with *)
  (* | None -> Js.log "NOT"; ("", "") *)
  (* | Some (t, b) -> (t, b) *)
  (* in *)
  if room##maySendMessage () && model.send_editor.state != Closed then
    div ~unique:room##roomId
      [id "input-area"; class' "m-t-2 relative flex shrink-0 p-4"]
      [ button
          [ class' "absolute right-3 top-3"
          ; Icons.aria_label "Close"
          ; title "Close"
          ; onClick hideSendBox ]
          [Icons.icon "close"]
      ; div
          [ class'
              "h-46 flex flex-1 flex-col overflow-hidden rounded-[18px] \
               bg-dominant-soft-dark text-cardboard-gray shadow-none \
               outline-none hover:border-contrast-dark/50 \
               focus:border-contrast-dark/50 \
               focus-visible:border-contrast-dark/50 dark:text-cardboard-gray"
          ]
          [ textarea
              [ class'
                  "mx-5 h-14 resize-none border-b-2 border-dominant-dark \
                   bg-dominant-soft-dark p-2 text-2xl font-bold \
                   text-cardboard-gray shadow-none outline-none \
                   dark:text-cardboard-gray"
              ; value model.new_post_title
              ; Html.on_ctrl_enter (editorMsg Send)
              ; onInput ~key:room##roomId savePostTitle ]
              [text ""]
          ; Editor.view_inner ~border:false model.send_editor
            |> Vdom.map editorMsg ]
      ; button
          [class' "ml-4"; type' "submit"; onClick (editorMsg Send)]
          [Icons.icon ~class':"h-5 w-5" "send2"] ]
  else noNode

let thread_preview room (event, post_title) =
  let rec get_latest_reply e =
    match
      Js.Array.sortInPlaceWith e.Event.children ~f:(fun c1 c2 ->
          let c1date = (get_latest_reply c1)##getDate () |> Js.Date.valueOf in
          let c2date = (get_latest_reply c2)##getDate () |> Js.Date.valueOf in
          Float.compare c1date c2date )
      |. Belt.Array.get 0
    with
    | Some child ->
        child.event
    | None ->
        e.event
  in
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  let open Tea.Html in
  let open Tea.Html.Attributes in
  tr
    [class' "border-b-4 border-dominant-soft-dark last:border-b-0"]
    [ td
        [class' "w-full max-w-0 truncate py-4 text-2xl font-bold"]
        [ Router.link ~key:event_id goTo
            ~props:[title post_title]
            (Room (Id room##roomId, Some event_id))
            [text post_title] ]
    ; td
        [ class'
            "flex justify-center whitespace-nowrap px-4 pt-4 md:table-cell \
             md:pt-0" ]
        [Common_view.likes_count_view ~invert_colors:true room event]
    ; td
        [ class'
            "flex justify-center whitespace-nowrap px-4 py-2 md:table-cell \
             md:py-0" ]
        [Common_view.all_replies_count_view ~invert_colors:true room event]
    ; td
        [ class'
            "flex justify-center whitespace-nowrap px-4 pb-4 text-center \
             text-cardboard-gray/50 md:table-cell md:pb-0" ]
        [Common_view.time_view room (get_latest_reply event) "" "italic"] ]

let thread_view model room events root_event root_title focused_event =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  [ div
      [class' "mb-5 flex flex-row items-center"]
      [ h1 [class' "mx-6 grow text-3xl font-bold"] [text root_title]
      ; Components.button_link ~msg:ShowSendBox
          ~classes:
            "mr-20 w-fit cursor-pointer rounded-lg border px-8 py-2 \
             hover:bg-dominant-soft-dark"
          "Reply" ]
  ; div
      [class' "grow overflow-y-auto px-5"]
      [message_view model room root_event None false noNode noNode]
  ; reply_root_box model room root_event
      (root_event.event##getId ())
      root_event.new_reply ]

let view model room events =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  match model.current_event with
  | None ->
      let thread_list =
        events |> filter_posts
        |. Belt.Array.map (thread_preview room)
        |> Belt.List.fromArray
      in
      [ div
          ~unique:(room##roomId ^ "forum")
          [ class' "flex grow flex-wrap overflow-y-auto"
          ; Scroll.on_scroll scrolled ]
          [ table
              [class' "mx-8 h-max w-full"]
              [ thead
                  [class' "sticky left-0 top-0 bg-dominant-dark"]
                  [ tr []
                      [ th
                          [class' "w-full"]
                          [ Components.button_link ~msg:ShowSendBox
                              ~classes:
                                "mb-4 mt-8 w-fit cursor-pointer rounded-lg \
                                 border px-8 py-2 hover:bg-dominant-soft-dark"
                              "New thread" ]
                      ; th [class' "whitespace-nowrap"] []
                      ; th [class' "whitespace-nowrap"] []
                      ; th [class' "whitespace-nowrap"] [] ] ]
              ; tbody [] thread_list ] ]
      ; send_box model room ]
  | Some focused_event -> (
    match filter_posts events with
    | [|(root_event, title)|] ->
        thread_view model room events root_event title focused_event
    | _ ->
        [ div []
            [ text
                "Thread is not a forum thread, use another display mode to see \
                 it." ] ] )
