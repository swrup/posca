type traversal = Breadth | Depth

(* type order = Time | Likes *)

type children_display =
  | None
  | One of int
  (* | All of traversal * order *)
  | All

type model =
  { event: Matrix.matrix_event
  ; level: int
  ; thread: Matrix.matrix_event option
  ; children: model array
  ; show_children: children_display
  ; traversal: traversal
  ; reply_editor: Editor.model
  ; new_reply: string }

type as_thread = bool

type msg =
  | ShowChild of int
  | ChangeTraversal of traversal
  | ShowChildren
  | HideChildren
  | CollapseOne
  | ShowReplyBox
  | HideReplyBox
  | Like
  | SaveReply of string
  | SentReply of (< event_id: string > Js.t, string) result
  | EditorMsg of Editor.msg
[@@deriving accessors]

let send_reply_cmd message event as_thread =
  let matrix_event = event.event in
  let room_id = matrix_event##getRoomId () in
  let thread_id =
    if as_thread then
      match event.thread with
      | Some t ->
          t##getId ()
      | None ->
          Js.Exn.raiseError "Message parent has no thread"
    else ""
  in
  let parent_id = matrix_event##getId () in
  (* let last_event_id = *)
  (*   match Belt.Array.get (Belt.Array.reverse event.children) 0 with *)
  (*   | Some e -> *)
  (*       e.event##getId () *)
  (*   | None -> *)
  (*       parent_id *)
  (* in *)
  (* in statuses view replies in subthread should still reply to the replied message and not the last in subthread *)
  let relates_to =
    { Matrix.rel_type= "m.thread"
    ; event_id= thread_id
    ; is_falling_back= false (* true if not subthread? *)
    ; in_reply_to= {event_id= parent_id} }
  in
  let msgtype = "m.text" in
  let content =
    match message with
    | Editor.Plain text ->
        Matrix.thread_message_content ~relates_to ~msgtype ~body:text ()
    | Html (text, html) ->
        Matrix.thread_message_content ~relates_to ~msgtype ~body:text
          ~format:"org.matrix.custom.html" ~formatted_body:html ()
  in
  Tea_promise.result
    (Matrix.ThreadMessage.send_message Matrix.client room_id content)
    sentReply

let like_cmd event =
  let matrix_event = event.event in
  let room_id = matrix_event##getRoomId () in
  let content =
    { Matrix.relates_to=
        { event_id= matrix_event##getId ()
        ; key= {js|👍️|js}
        ; rel_type= "m.annotation" } }
  in
  Tea_promise.result
    (Matrix.ReactionMessage.send_event Matrix.client room_id "m.reaction"
       content )
    sentReply

let rec set_depth e =
  { e with
    show_children= One 0
  ; traversal= Depth
  ; children= Belt.Array.map e.children set_depth }

let rec set_breadth level e =
  let show_children = if level = 0 then All else None in
  { e with
    show_children
  ; traversal= Breadth
  ; children= Belt.Array.map e.children @@ set_breadth (level + 1) }

let set_traversal = function Breadth -> set_breadth 0 | Depth -> set_depth

let rec map event f =
  let children = Belt.Array.map event.children (fun e -> map e f) in
  f {event with children}

let map_list events f = Belt.Array.map events (fun e -> map e f)

let hide_children_rec event = map event (fun e -> {e with show_children= None})

let rec collapse_one event =
  let children = Belt.Array.map event.children collapse_one in
  match event.show_children with
  | One _
    when Belt.Array.some event.children (fun c ->
             match c.show_children with All -> true | _ -> false ) ->
      {event with children; show_children= All; traversal= Breadth}
  | All ->
      {event with children; show_children= None}
  | _ ->
      {event with children}

let update event = function
  | ShowChildren ->
      ({event with show_children= All}, Tea.Cmd.none)
  | HideChildren ->
      (hide_children_rec event, Tea.Cmd.none)
  | ShowChild index ->
      (* ({event with show_children= One index}, Tea.Cmd.none) *)
      (* (event, Tea.Cmd.msg (Focus (Belt.Array.get event.children index))) *)
      (* (set_traversal event.traversal event, Tea.Cmd.none) *)
      let children =
        Belt.Array.mapWithIndex event.children (fun i e ->
            if i = index then set_traversal event.traversal e else e )
      in
      (* (set_traversal event.traversal event, Tea.Cmd.msg (FocusChild (event.event, index))) *)
      ({event with children; show_children= One index}, Tea.Cmd.none)
  | CollapseOne ->
      (collapse_one event, Tea.Cmd.none)
  | Like ->
      (event, like_cmd event)
  | ShowReplyBox ->
      let reply_editor, _cmd = Editor.update event.reply_editor Show in
      ({event with reply_editor}, Tea.Cmd.none)
  | HideReplyBox ->
      let reply_editor, _cmd = Editor.update event.reply_editor Hide in
      ({event with reply_editor}, Tea.Cmd.none)
  | SaveReply reply ->
      ({event with new_reply= reply}, Tea.Cmd.none)
  | SentReply (Ok res) ->
      let () = Js.log res in
      (event, Tea.Cmd.none)
  | SentReply (Error err) ->
      let () = Js.log err in
      (event, Tea.Cmd.none)
  | ChangeTraversal Depth ->
      let rec set_depth index e =
        let show_children =
          match e.show_children with One i -> One i | _ -> One 0
        in
        { e with
          show_children
        ; traversal= Depth
        ; children= Belt.Array.mapWithIndex e.children set_depth }
      in
      (set_depth 0 event, Tea.Cmd.none)
  | ChangeTraversal Breadth ->
      (event, Tea.Cmd.none)
  | EditorMsg Send -> (
    match Editor.get_content event.reply_editor with
    | Plain "" | Html ("", _) ->
        (event, Tea.Cmd.none)
    | _ as message_body ->
        let cmd = send_reply_cmd message_body event true in
        let reply_editor = Editor.after_send event.reply_editor in
        ({event with reply_editor}, Tea.Cmd.none) )
  | EditorMsg editor_msg ->
      let reply_editor, editor_cmd =
        Editor.update event.reply_editor editor_msg
      in
      ({event with reply_editor}, Tea.Cmd.map editorMsg editor_cmd)

let rec maybe_update_event event matrix_event msg =
  if event.event##getId () = matrix_event##getId () then
    let updated_event, cmd = update event msg in
    (updated_event, cmd)
  else
    let children, cmds =
      Belt.Array.map event.children (fun child ->
          maybe_update_event child matrix_event msg )
      |> Belt.Array.unzip
    in
    ({event with children}, Tea.Cmd.batch @@ Belt.List.fromArray cmds)

let update_events events matrix_event msg =
  let events, cmds =
    Belt.Array.map events (fun event ->
        maybe_update_event event matrix_event msg )
    |> Belt.Array.unzip
  in
  (events, Tea.Cmd.batch @@ Belt.List.fromArray cmds)

let get_parent event : string option =
  match event##getRelation () |> Js.Nullable.toOption with
  | None ->
      None
  | Some
      { Matrix_sdk.Event_relation.in_reply_to
      ; is_falling_back
      ; rel_type
      ; event_id
      ; _ } -> (
    match
      ( Js.Undefined.toOption in_reply_to
      , Js.Undefined.toOption is_falling_back
      , Js.Undefined.toOption rel_type
      , Js.Undefined.toOption event_id )
    with
    | Some {event_id}, Some false, _, _ ->
        Some event_id
    | _, _, Some "m.thread", Some thread_id ->
        Some thread_id
    | _, _, _, _ ->
        None )

let rec make_tree event thread level other_events =
  let thread = match thread with Some t -> Some t | None -> Some event in
  let children =
    Belt.Array.keep other_events (fun other ->
        match get_parent other with
        | Some parent ->
            (* Js.log parent ; *)
            parent = event##getId ()
        | None ->
            false )
    |. Belt.Array.map (fun child ->
           make_tree child thread (level + 1) other_events )
  in
  { event
  ; children
  ; thread
  ; level
  ; show_children= None
  ; traversal= Breadth
  ; reply_editor= Editor.init @@ event##getId ()
  ; new_reply= "" }

let build_tree events =
  let rest, first_level =
    Belt.Array.partition events (fun event ->
        get_parent event |> Belt.Option.isSome )
  in
  (* Js.log rest; *)
  (* Js.log first_level; *)
  Belt.Array.map first_level (fun event -> make_tree event None 0 rest)
  |> Belt.Array.reverse

let build_tree_from_thread_roots roots =
  (* Js.log rest ; *)
  (* Js.log first_level ; *)
  Belt.Array.map roots (fun event ->
      let descendants =
        match event##getThread () |> Js.Undefined.toOption with
        | None ->
            [||]
        | Some thread ->
            ((thread##getUnfilteredTimelineSet ())##getLiveTimeline ())##getEvents
              ()
      in
      make_tree event (Some event) 0 descendants )
  |> Belt.Array.reverse

let sort events =
  let () =
    Belt.SortArray.stableSortInPlaceBy events (fun e1 e2 ->
        (* let e1_time = e1.event##getDate () |> Js.Date.getTime in *)
        (* let e2_time = e2.event##getDate () |> Js.Date.getTime in *)
        (* e1_time < e2_time *)
        (* let e1_time = e1.event##getDate () in *)
        (* let e2_time = e2.event##getDate () in *)
        [%raw {|e2.event.getDate() - e1.event.getDate()|}] )
  in
  events

let add_if_not_present array item =
  if Js.Array.some ~f:(fun i -> i.event##getId () = item.event##getId ()) array
  then array
  else Belt.Array.concat array [|item|] |> sort

let add_event events event =
  let new_model =
    { event
    ; children= [||]
    ; thread= None
    ; level= 0
    ; show_children= None
    ; traversal= Breadth
    ; reply_editor= Editor.init @@ event##getId ()
    ; new_reply= "" }
  in
  let rec add_child current parent_id child level =
    if current.event##getId () = parent_id then
      { current with
        children=
          add_if_not_present current.children
            {child with level; thread= current.thread} }
    else
      { current with
        children=
          Belt.Array.map current.children (fun c ->
              add_child c parent_id child (level + 1) ) }
  in
  match get_parent event with
  | Some parent_id ->
      Belt.Array.map events (fun e -> add_child e parent_id new_model 1)
  | None ->
      add_if_not_present events {new_model with thread= Some event}

(* let init timeline = get_messages timeline |> build_tree *)

let rec focus event event_id parent_id =
  let current_id = event.event##getId () in
  if current_id = event_id then
    let show_children = if event.show_children = All then None else All in
    { event with
      show_children
    ; children= Belt.Array.map event.children hide_children_rec }
  else
    let children =
      Belt.Array.map event.children (fun e -> focus e event_id parent_id)
    in
    match
      Belt.Array.getIndexBy children (fun c ->
          match c.show_children with All -> true | One _ -> true | _ -> false )
    with
    | Some index ->
        {event with children; show_children= One index; traversal= Breadth}
    | None ->
        {event with children; show_children= None}

let focus_list events event_id parent_id =
  Belt.Array.map events (fun e -> focus e event_id parent_id)

(* let filter_for focused_event events = *)
(*   let rec filter_event focused_event e = *)
(*     (* match filter_event event e.childre *) *)
(*     if e.event = focused_event then *)
(*       let children = *)
(*         Belt.Array.map e.children (fun child -> *)
(*             {child with show_children= None} ) *)
(*       in *)
(*       Some {e with children; show_children= All} *)
(*     else *)
(*       let children = *)
(*         Tablecloth.Array.filter_map e.children ~f:(fun child -> *)
(*             filter_event focused_event child ) *)
(*       in *)
(*       match children with *)
(*       | [||] -> *)
(*           None *)
(*       | [|child|] -> *)
(*           Some {e with children; show_children= One 0} *)
(*       | _ -> *)
(*           None *)
(*   in *)
(*   Tablecloth.Array.filter_map events ~f:(fun e -> filter_event focused_event e) *)

let filter_for focused_event events =
  let rec filter_event focused_event e =
    if e.event##getId () = focused_event##getId () then
      let children =
        Belt.Array.map e.children (fun child ->
            {child with show_children= None} )
      in
      {e with children; show_children= All}
    else
      let children =
        Js.Array.map e.children ~f:(fun child ->
            filter_event focused_event child )
      in
      let show_children =
        match
          Js.Array.findIndex children ~f:(fun child ->
              match child.show_children with None -> false | _ -> true )
        with
        | -1 ->
            None
        | i ->
            One i
      in
      {e with children; show_children}
  in
  Js.Array.map events ~f:(fun e -> filter_event focused_event e)
