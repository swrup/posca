external sanitize : string -> string = "sanitize"
[@@mel.module "dompurify"] [@@mel.scope "default"]

open Room

let message_body ?(should_remove_title = false) event =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  match Matrix.parse_message event with
  | Text (Body body) ->
      let body =
        if should_remove_title then Matrix.remove_title body else body
      in
      div
        [class' "prose max-w-screen-lg text-cardboard-gray dark:prose-invert"]
        [p [class' "whitespace-pre-wrap"] [text body]]
  | Text (FormattedBody formatted_body) ->
      let formatted_body =
        if should_remove_title then Matrix.remove_title_formatted formatted_body
        else formatted_body
      in
      div
        [ class' "prose max-w-screen-lg text-cardboard-gray dark:prose-invert"
        ; Vdom.prop "innerHTML" @@ sanitize formatted_body ]
        []
  | Image image_params ->
      Matrix.image_to_html image_params

let replies_count_view ?(invert_colors = false) room event =
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  let key_prefix, msg =
    if event.Event.reply_editor.state != Closed then
      ("hide", eventMsg event.event HideReplyBox)
    else ("close", eventMsg event.event ShowReplyBox)
  in
  Components.message_pill ~invert_colors (key_prefix ^ event_id) "reply2"
    (Belt.Array.length event.children)
    false
    (if room##maySendMessage () then Some msg else None)

let all_replies_count_view ?(invert_colors = false) room event =
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  let rec reply_count e =
    Js.Array.reduce e.Event.children ~init:0 ~f:(fun acc c ->
        acc + 1 + reply_count c )
  in
  Components.message_pill ~invert_colors ("replies" ^ event_id) "reply2"
    (reply_count event) false None

let likes_count_view ?(invert_colors = false) room event =
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  let personal_like = Matrix.personal_like room event_id in
  let key_prefix, msg =
    match personal_like with
    | Some reaction ->
        ("unlike", redact reaction)
    | None ->
        ("like", eventMsg event.event Like)
  in
  Components.message_pill ~invert_colors (key_prefix ^ event_id) "heart"
    (Matrix.like_count room event_id)
    (Belt.Option.isSome personal_like)
    (if room##maySendMessage () then Some msg else None)

let time_view room event link_classes time_classes =
  let event_id = event##getId () in
  let date = event##getDate () in
  let iso_date = Js.Date.toISOString date in
  let open Tea.Html in
  let open Tea.Html.Attributes in
  Router.link ~key:event_id goTo
    ~props:[class' link_classes; title (T.chat_message_date {date})]
    (Room (Id room##roomId, Some event_id))
    [ time
        [class' time_classes; Vdom.prop "datetime" iso_date]
        [text (Time_ago.format date)] ]

let message_card_sender ?(large = true) ?(additional_classes = "") room event =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  let avatar_view =
    let homeserver_url = Matrix.client##getHomeserverUrl () in
    let img =
      match
        matrix_event##sender##getAvatarUrl homeserver_url 100 100 "scale"
        |> Js.Nullable.toOption
      with
      | Some url ->
          img
            [ class' "h-10 w-10 rounded-full border-2 border-contrast-dark"
            ; src url ]
            []
      | None ->
          noNode
    in
    div [class' "mr-2 h-10 w-10 shrink-0"] [img]
  in
  let user_name_view =
    div
      [class' "mr-2 flex-auto text-xl font-bold"]
      [text matrix_event##sender##rawDisplayName]
  in
  let user_id_view =
    div
      [class' "mr-2 flex-auto text-base italic text-cardboard-gray/50"]
      [text matrix_event##sender##userId]
  in
  div
    [ classList
        [ ("mb-4 flex items-center " ^ additional_classes, true)
        ; ("md:min-w-[430px]", large) ] ]
    [ avatar_view
    ; div [] [user_name_view; user_id_view]
    ; time_view room matrix_event "ml-auto"
        "mr-2 text-sm italic text-alternate-gray" ]

let message_card_reply_box event key current_input =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  div
    [class' "p-2"]
    [ Editor.view ~container_classes:"border-t-2 border-dominant-soft-dark"
        ~textarea_classes:"h-24 !bg-dominant-dark" ~close_button:false
        event.Event.reply_editor
      |> Vdom.map (fun msg -> eventMsg event.Event.event (Event.editorMsg msg))
    ]

let message_card ?(large = true) room event parent is_indented left right =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  let is_focused = match event.show_children with All -> true | _ -> false in
  let message_body_view =
    div []
      [ a ~key:event_id
          [ class' "cursor-pointer"
          ; Tea.Html.Events.onClick @@ focus event_id parent ]
          [message_body matrix_event] ]
  in
  let reply_box =
    if room##maySendMessage () && event.reply_editor.state != Closed then
      message_card_reply_box event event_id event.new_reply
    else noNode
  in
  div
    [ classList
        [ ("grow overflow-hidden p-5", true)
        ; ("cut-corners bg-dominant-soft-dark", not is_focused)
        ; ( "cut-corners-borders border-contrast-dark bg-dominant-dark"
          , is_focused )
        ; ("ml-4", (not is_indented) && left = noNode)
        ; ("md:ml-5", (not is_indented) && left = noNode && large)
        ; ("mr-4", (not is_indented) && right = noNode)
        ; ("md:mr-5", (not is_indented) && right = noNode && large) ] ]
    (* [class' "min-w-full rounded-2xl bg-dominant-soft-dark"] *)
    [ div
        [classList [("flex grow flex-col", true); ("md:flex-row", large)]]
        [ div
            [classList [("grow", true); ("md:mr-10", large)]]
            [message_card_sender ~large room event; message_body_view]
        ; div
            [ classList
                [("flex flex-row justify-around", true); ("md:flex-col", large)]
            ]
            [replies_count_view room event; likes_count_view room event] ]
    ; reply_box ]

let thread_buttons ?(additional_classes = "") model room event =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let shows_children =
    match event.Event.show_children with None -> false | _ -> true
  in
  let collapse_thread_button =
    match model.current_event with
    | None when shows_children && Belt.Array.length event.children != 0 ->
        Components.rpill_button ~icon_only:true
          (eventMsg event.event HideChildren)
          "chevron-left" "Collapse thread"
    | _ ->
        noNode
  in
  let back_to_room_button =
    match model.current_event with
    | Some _ ->
        Components.rpill_button ~icon_only:true
          (goTo (Room (Id room##roomId, None)))
          "arrow-left" "Back to room"
    | None ->
        noNode
  in
  let collapse_one_button =
    match event.traversal with
    | Breadth
      when shows_children
           && Belt.Array.some event.children (fun e ->
                  match e.show_children with None -> false | _ -> true ) ->
        Components.rpill_button ~icon_only:true
          (eventMsg event.event CollapseOne)
          "chevron-top" "Collapse one level"
    | _ ->
        noNode
  in
  let change_order_button =
    if shows_children && Belt.Array.length event.children != 0 then
      Components.rpill_button ~icon_only:true
        (eventMsg event.event (ChangeTraversal Depth))
        "heart" "Sort by likes (NOT IMPLEMENTED)"
    else noNode
  in
  let change_traversal_button =
    match event.traversal with
    | Breadth when shows_children && Belt.Array.length event.children != 0 ->
        Components.rpill_button (* ~key:("depth-" ^ matrix_event##getId ()) *)
          ~icon_only:true
          (eventMsg event.event (ChangeTraversal Depth))
          "arrow-bottom" "Linear mode"
    (* | Depth when shows_children && Belt.Array.length event.children != 0 -> *)
    (*     Components.rpill_button *)
    (*     (* ~key:("breadth-" ^ matrix_event##getId ()) *) *)
    (*       ~icon_only:true *)
    (*       (eventMsg event.event (ChangeTraversal Breadth)) *)
    (*       "room-menu" "traverse breadth-first" *)
    | _ ->
        noNode
  in
  if event.level = 0 then
    div
      [ class' @@ "sticky left-8 top-12 float-left flex flex-row "
        ^ additional_classes ]
      [ collapse_thread_button
      ; back_to_room_button
      ; collapse_one_button
      ; change_order_button
      ; change_traversal_button ]
  else noNode

let rec tree_of_cards ?(large = true) model room event parent is_indented left
    right =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let matrix_event = event.Event.event in
  let event_id = matrix_event##getId () in
  (* Js.log @@ Matrix.likes room event_id ; *)
  (* let () = Js.log matrix_event in *)
  (* Js.log @@ message_body matrix_event ; *)
  (* Js.log @@ matrix_event##getRelation () ; *)
  (* let message_display = *)
  (*   Printf.sprintf *)
  (*     "<%s> %s" *)
  (*     matrix_event##sender##rawDisplayName *)
  (*     matrix_event##event##content##body *)
  (* in *)
  let shows_children =
    match event.show_children with None -> false | _ -> true
  in
  let replies_view =
    let maybe_sibling_button icon index =
      match Belt.Array.get event.children index with
      | Some e ->
          div
            [class' "flex w-5 flex-row items-center"]
            [ a ~key:event_id
                [ class' "cursor-pointer"
                ; Tea.Html.Events.onClick
                  @@ eventMsg event.event (Event.showChild index) ]
                [Icons.icon ~class':"h-5 w-5" icon] ]
      | None ->
          noNode
    in
    match event.show_children with
    | None ->
        noNode
    | One index -> (
      match Belt.Array.get event.children index with
      | Some e ->
          div []
            [ tree_of_cards model room e (Some event_id) false
                (maybe_sibling_button "chevron-left" (index - 1))
                (maybe_sibling_button "chevron-right" (index + 1)) ]
      | None ->
          noNode )
    | All ->
        div
          [classList [("ml-10 mr-5", true); ("md:ml-14", large)]]
          ( Belt.Array.map event.children (fun child ->
                (* comment_view child |> Vdom.map (eventMsg child.event) ) *)
                tree_of_cards model room child (Some event_id) true noNode
                  noNode )
          |> Belt.List.fromArray )
  in
  div []
    [ thread_buttons ~additional_classes:"!hidden md:!flex" model room event
    ; div
        ~unique:(matrix_event##getId ())
        [ classList
            [ ("mx-auto max-w-screen-md", true)
            ; ("relative my-20 block", event.level = 0)
            ; ( "before:absolute before:bottom-0 before:left-6 before:top-5 \
                 before:border-l-2"
              , event.level = 0 && shows_children
                && Belt.Array.length event.children != 0 )
            ; ( "before:absolute before:left-[25px] before:mt-8 before:h-0 \
                 before:w-0 before:border-y-[8px] before:border-l-[8px] \
                 before:border-y-[transparent] before:border-l-contrast-dark"
              , is_indented )
            ; ("my-5 block", event.level != 0) ]
        ; id event_id ]
        [ div
            [class' "flex flex-row items-stretch"]
            [ left
            ; message_card ~large room event parent is_indented left right
            ; right ]
        ; thread_buttons
            ~additional_classes:
              "!top-0 z-50 !float-none -mb-2 ml-10 mt-4 justify-center \
               bg-dominant-dark md:!hidden"
            model room event
        ; replies_view ] ]
