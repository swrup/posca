open Room

let input_area model room =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  if room##maySendMessage () then
    if model.send_editor.state != Closed then
      div
        [class' "flex flex-col items-center p-2"]
        [ Editor.view
            ~container_classes:
              "w-full max-w-screen-md items-center justify-center"
            ~textarea_classes:"h-24" model.send_editor
          |> Vdom.map editorMsg ]
    else
      div
        [class' "flex flex-col items-center"]
        [ Components.button_link ~msg:ShowSendBox
            ~classes:
              "mb-4 w-fit cursor-pointer rounded-lg border px-8 py-2 \
               hover:bg-dominant-soft-dark"
            "New status" ]
  else noNode

let view model room events =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let message_list =
    events
    |. Belt.Array.map (fun event ->
           (* message_view event |> Vdom.map (eventMsg event.Event.event) ) *)
           Common_view.tree_of_cards model room event None false noNode noNode )
    |> Belt.List.fromArray
  in
  [ input_area model room
  ; div
      ~unique:(room##roomId ^ "statuses")
      [ id "message-list"
      ; class' "grow overflow-y-scroll"
      ; Scroll.on_scroll scrolled ]
      [div [class' ""] message_list] ]
