open Room

let id_color_hash id =
  let char_code_sum =
    Js.String.split ~sep:"" id
    |. Belt.Array.reduce 0 (fun acc char ->
           acc + int_of_float (Js.String.charCodeAt ~index:0 char) )
  in
  (char_code_sum mod 8) + 1

let username_color user_id =
  match id_color_hash user_id with
  | 1 ->
      "text-username-1"
  | 2 ->
      "text-username-2"
  | 3 ->
      "text-username-3"
  | 4 ->
      "text-username-4"
  | 5 ->
      "text-username-5"
  | 6 ->
      "text-username-6"
  | 7 ->
      "text-username-7"
  | 8 ->
      "text-username-8"
  | _ ->
      ""

let reply_border_color user_id =
  match id_color_hash user_id with
  | 1 ->
      "border-username-1"
  | 2 ->
      "border-username-2"
  | 3 ->
      "border-username-3"
  | 4 ->
      "border-username-4"
  | 5 ->
      "border-username-5"
  | 6 ->
      "border-username-6"
  | 7 ->
      "border-username-7"
  | 8 ->
      "border-username-8"
  | _ ->
      ""

let reply_view matrix_event =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let sender = matrix_event##sender in
  let user_id = sender##userId in
  let sender_view =
    let avatar_view =
      let homeserver_url = Matrix.client##getHomeserverUrl () in
      match
        matrix_event##sender##getAvatarUrl homeserver_url 100 100 "scale"
        |> Js.Nullable.toOption
      with
      | Some url ->
          img
            [ class'
                "mr-2 inline-block h-6 w-6 rounded-full border-[0.1em] \
                 border-dominant-soft-dark text-right"
            ; src url ]
            []
      | None ->
          noNode
    in
    let username_view =
      span
        [ class' @@ "mr-2 font-bold "
          ^ username_color matrix_event##sender##userId ]
        [text matrix_event##sender##rawDisplayName]
    in
    div [class' ""] [avatar_view; username_view]
  in
  div
    [class' @@ "mb-2 border-l-4 pl-4 opacity-60 " ^ reply_border_color user_id]
    [sender_view; div [] [Common_view.message_body matrix_event]]

let message_content room matrix_event =
  let open Tea.Html in
  (* Js.log matrix_event ; *)
  let own_body = Common_view.message_body matrix_event in
  let reply =
    match matrix_event##replyEventId |> Js.Undefined.toOption with
    | None ->
        noNode
    | Some event_id -> (
      match room##findEventById event_id |> Js.Undefined.toOption with
      | None ->
          div [] [text "Reply not found"]
      | Some event ->
          reply_view event )
  in
  div [] [reply; own_body]

let message_view room event =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let matrix_event = event.Event.event in
  let avatar_view =
    let homeserver_url = Matrix.client##getHomeserverUrl () in
    let image_tag =
      match
        matrix_event##sender##getAvatarUrl homeserver_url 100 100 "scale"
        |> Js.Nullable.toOption
      with
      | Some url ->
          img
            [ class'
                "mr-2 inline-block rounded-full border-[0.1em] \
                 border-dominant-soft-dark text-right"
            ; src url ]
            []
      | None ->
          noNode
    in
    div [class' "mr-4 h-10 w-10"] [image_tag]
  in
  let username_view =
    span
      [class' @@ "mr-2 font-bold " ^ username_color matrix_event##sender##userId]
      [text matrix_event##sender##rawDisplayName]
  in
  (* div [class' "mr-[3%] w-[14%] p-1 text-right"] [avatar_view; username_view] *)
  let content_view =
    div
      [class' "rounded-lg p-1 group-hover:bg-dominant-soft-dark"]
      [message_content room matrix_event]
  in
  let message_view =
    div
      [class' "mr-8 w-full"]
      [ div
          [class' "flex items-center justify-between"]
          [ username_view
          ; Common_view.time_view room matrix_event "ml-auto"
              "invisible p-1 font-light italic group-hover:visible" ]
      ; content_view ]
  in
  div
    ~unique:(matrix_event##getId ())
    [class' "group mt-2 block flex flex-row"; id (matrix_event##getId ())]
    [avatar_view; message_view]

let input_area model room =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  if room##maySendMessage () then
    div
      [class' "mx-2"]
      [ Editor.view ~close_button:false ~send_on_enter:true ~thin:true
          model.send_editor
        |> Vdom.map editorMsg ]
  else noNode

let view model room events =
  let message_list =
    events |> Belt.Array.reverse
    |. Belt.Array.map (message_view room)
    |> Belt.List.fromArray
  in
  let open Tea.Html in
  let open Tea.Html.Attributes in
  [ div
      ~unique:(room##roomId ^ "chat")
      [ id "message-list"
      ; class' "grow overflow-y-scroll pl-4"
      ; Scroll.on_scroll scrolled ]
      message_list
  ; input_area model room ]
