open Rooms

let view model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  match model.current_room with
  | NoRoom ->
      div
        [class' "flex h-full place-content-center items-center"]
        [text "No room selected"]
  | Error _ ->
      div
        [class' "flex h-full place-content-center items-center"]
        [text "Room not found"]
  | Room room -> (
    match Js.Dict.get model.rooms room##roomId with
    | None ->
        div
          [class' "flex h-full place-content-center items-center"]
          [text "Room not found"]
    | Some room_model ->
        Room_view.view room_model room |> Vdom.map (roomMsg room) )
