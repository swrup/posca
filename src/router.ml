type route =
  | Index
  | Login
  | Register
  | Logout
  | Settings
  | CreateRoom of Matrix.room_type * Matrix.room_id option
  | Room of Matrix.room_address * Matrix.event_id option

module StringCmp = Belt.Id.MakeComparable (struct
  type t = string

  let cmp a b = String.compare a b
end)

type route_params = (StringCmp.t, string, StringCmp.identity) Belt.Map.t

(* we use Belt.Map until Belt.Map.String is fixed *)

let route_of_location location =
  (* let as_list = Belt.Array.toList input in *)
  let rec list_to_map output = function
    | key :: value :: rest ->
        list_to_map (Belt.Map.set output key value) rest
    | _ ->
        output
  in
  let parse_params params =
    params
    (* |> Js.String.sliceToEnd ~from:1 *)
    |> Js.String.split ~sep:"&"
    |> Array.to_list
    |. Belt.List.map (fun kv -> Js.String.split ~sep:"=" kv |> Array.to_list)
    |> Belt.List.flatten
    |> list_to_map (Belt.Map.make ~id:(module StringCmp))
  in
  (* Js.log location; *)
  (* Js.log (parse_params location.Web.Location.search); *)
  let regexp = Js.Re.fromString "[/?]" in
  let route =
    Js.String.splitByRe ~regexp location.Tea.Navigation.Location.hash
  in
  (* let () = Js.log (Js.String.split "/" location.Web.Location.pathname) in *)
  (* Js.log location ; *)
  (* Js.log "route:" ; *)
  (* Js.log route ; *)
  match route with
  | [|Some "#"; Some "login"|] ->
      Login
  | [|Some "#"; Some "register"|] ->
      Register
  | [|Some "#"; Some "logout"|] ->
      Logout
  | [|Some "#"; Some "settings"|] ->
      Settings
  | [|Some "#"; Some "room"; Some "new"|] ->
      CreateRoom (Chat, None)
  | [|Some "#"; Some "room"; Some "new"; Some params|] ->
      let parsed_params = parse_params params in
      let display = Belt.Map.getWithDefault parsed_params "type" "chat" in
      let space_id = Belt.Map.get parsed_params "space" in
      CreateRoom (Matrix.room_type_of_string display, space_id)
  | [|Some "#"; Some "room"; Some room_address|] -> (
    match
      Js.String.charAt ~index:0 (Js.Global.decodeURIComponent room_address)
    with
    | "!" ->
        Room (Id room_address, None)
    | "#" ->
        Room (Alias room_address, None)
    | _ ->
        Index )
  | [|Some "#"; Some "room"; Some room_address; Some event_id|] -> (
    match
      Js.String.charAt ~index:0 (Js.Global.decodeURIComponent room_address)
    with
    | "!" ->
        Room (Id room_address, Some event_id)
    | "#" ->
        Room (Alias room_address, Some event_id)
    | _ ->
        Index )
  | _ ->
      Index

type dom

external dom : dom = "document"

external set_dom_title : dom -> string -> unit = "title" [@@mel.set]

let set_title title = set_dom_title dom title

let set_title_for_route = function
  | Index ->
      set_title (T.router_title {page= "index"})
  | Login ->
      set_title (T.router_title {page= "login"})
  | Register ->
      set_title (T.router_title {page= "signup"})
  | Logout ->
      set_title (T.router_title {page= "logout"})
  | Settings ->
      set_title "Settings"
  | CreateRoom _ ->
      set_title (T.router_title {page= "create-chat"})
  | Room (Id room_id, _) ->
      set_title (T.router_title {page= "room"})
  | Room (Alias room_alias, _) ->
      set_title (T.router_title {page= "room"})

let location_of_route = function
  | Index ->
      "#/"
  | Login ->
      "#/login"
  | Register ->
      "#/register"
  | Logout ->
      "#/logout"
  | Settings ->
      "#/settings"
  | CreateRoom (display, None) ->
      "#/room/new?type=" ^ Matrix.string_of_room_type display
  | CreateRoom (display, Some space_id) ->
      "#/room/new?type="
      ^ Matrix.string_of_room_type display
      ^ "&space=" ^ space_id
  | Room (Id room_id, None) ->
      Printf.sprintf "#/room/%s" room_id
  | Room (Alias room_alias, None) ->
      Printf.sprintf "#/room/%s" room_alias
  | Room (Id room_id, Some event_id) ->
      Printf.sprintf "#/room/%s/%s" room_id event_id
  | Room (Alias room_alias, Some event_id) ->
      Printf.sprintf "#/room/%s/%s" room_alias event_id

let link_props msg route =
  [ Tea.Html.Attributes.href (location_of_route route)
  ; Tea.Html.Events.preventDefaultOn "click"
      (Tea_json.Decoder.succeed (msg route)) ]

let link ?(key = "") ?(unique = "") ?(props = []) msg route content =
  Tea.Html.a ~key ~unique
    (Belt.List.concat props (link_props msg route))
    content
