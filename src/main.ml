external is_hot : bool = "hot" [@@mel.scope "import", "meta"]

external hot_accept : (unit -> unit) -> unit = "accept"
[@@mel.scope "import", "meta", "hot"]

external hot_accept_dep : string -> (string -> unit) -> unit = "accept"
[@@mel.scope "import", "meta", "hot"]

external hot_accept_deps : string array -> (string array -> unit) -> unit
  = "accept"
[@@mel.scope "import", "meta", "hot"]

external hot_dispose : (unit -> unit) -> unit = "dispose"
[@@mel.scope "import", "meta", "hot"]

let container =
  Html.get_element_by_id "main" |. Belt.Option.map Webapi.Dom.Element.asNode

let run () =
  if Config.is_dev then
    if is_hot then
      let shutdown_fun = ref (App.start_hot_debug_app container None) in
      hot_accept_dep "/src/app.ml" (fun _mod ->
          Js.log "accept_dep" ;
          (* let _ = Js.Nullable.bind container (fun c -> c##setAttribute "hidden" "true") in *)
          let model = !shutdown_fun () in
          shutdown_fun :=
            ( [%raw {|_mod.start_hot_debug_app|}]
              : Dom.node option -> App.model option -> unit -> App.model option
              )
              container model ;
          () )
    else App.start_debug_app container |. ignore
  else App.start_app container |. ignore

let _ = Js.Global.setTimeout ~f:(fun _ -> run ()) 0
