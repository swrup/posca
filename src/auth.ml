type msg =
  | GoTo of Router.route
  | Info of (string, string) result
  | RestoredSession of (string * string * string * bool, string) result
  | GotGuestParams of (Matrix_sdk.Client.guest_params, string) result
  | Sync of string
  | Logout
  | ListInfo of (unit list, string) result
[@@deriving accessors]

let msg_to_string (msg : msg) =
  match msg with
  | GoTo _ ->
      "go to"
  | Info _ ->
      "info"
  | RestoredSession _ ->
      "restored session"
  | GotGuestParams _ ->
      "got guest params"
  | Sync _ ->
      "sync"
  | Logout ->
      "logout"
  | ListInfo _ ->
      "list info"

let promiseToTask promise =
  let open Tea_task in
  nativeBinding (fun cb ->
      let _ =
        promise
        |> Js.Promise.then_ (function res ->
               let resolve = cb (Ok res) in
               Js.Promise.resolve resolve )
        |> Js.Promise.catch (function err ->
               let reject = cb (Error (Obj.magic err)) in
               Js.Promise.resolve reject )
      in
      () )

let restore_cmd () =
  [ Tea.Ex.LocalStorage.getItem "access_token"
  ; Tea.Ex.LocalStorage.getItem "matrix_id"
  ; Tea.Ex.LocalStorage.getItem "home_server"
  ; Tea.Ex.LocalStorage.getItem "is_guest" ]
  |> Tea_task.sequence
  |> Tea_task.andThen (function
       | [Some a; Some b; Some c; Some d] ->
           Tea_task.succeed (a, b, c, d = "true")
       | _ ->
           Tea_task.fail "Not in LocalStorage" )
  |> Tea_task.andThen (fun (access_token, matrix_id, home_server, is_guest) ->
         Tea_task.succeed (access_token, matrix_id, home_server, is_guest) )
  |> Tea_task.attempt restoredSession

let remove_storage_cmd : msg Tea.Cmd.t =
  Tea.Cmd.batch
    [ Tea.Ex.LocalStorage.removeItemCmd "access_token"
    ; Tea.Ex.LocalStorage.removeItemCmd "matrix_id"
    ; Tea.Ex.LocalStorage.removeItemCmd "home_server"
    ; Tea.Ex.LocalStorage.removeItemCmd "is_guest" ]

let subscriptions () =
  (* let () = Js.log "chat subs" in *)
  match Matrix.client##clientRunning with
  | true ->
      Matrix.subscribe_once_logged_out (fun _error -> logout)
  | false ->
      Tea.Sub.none

let login_with_password username password =
  Matrix.login_with_password username password |> ignore

let is_logged_in () = Matrix.client##isLoggedIn ()

let is_guest () = Matrix.client##isGuest ()

let user_id () = Matrix.client##credentials##userId

let init () = restore_cmd ()

let register_guest_cmd () =
  let tmp_client = Matrix.create_client true in
  tmp_client##registerGuest () |. Tea_promise.result gotGuestParams

let save_cmd guest_params =
  [ Tea.Ex.LocalStorage.setItem "access_token"
      guest_params.Matrix_sdk.Client.access_token
  ; Tea.Ex.LocalStorage.setItem "matrix_id" guest_params.user_id
  ; Tea.Ex.LocalStorage.setItem "home_server" Matrix.default_server
  ; Tea.Ex.LocalStorage.setItem "is_guest" "true"
    (* login_response##home_server *)
    (* can't do that because synapse strips the port from the homeserver url *)
  ]
  |> Tea_task.sequence |> Tea_task.attempt listInfo

let update = function
  | RestoredSession (Ok (access_token, matrix_id, home_server, is_guest)) ->
      let () = Js.log "restored access token" in
      Matrix.set_client
      @@ Matrix.create_client_with_token ~base_url:home_server matrix_id
           access_token is_guest ;
      Matrix.client##setGuest is_guest ;
      Matrix.trigger_client_start () ;
      Tea.Cmd.none
  | RestoredSession (Error err) ->
      let () = Js.log ("restore failed: " ^ err) in
      register_guest_cmd ()
  | GotGuestParams (Ok guest_params) ->
      let () = Js.log guest_params in
      Matrix.set_client
      @@ Matrix.create_client_with_token ~base_url:Matrix.default_server
           guest_params.user_id guest_params.access_token true ;
      Matrix.client##setGuest true ;
      Matrix.trigger_client_start () ;
      save_cmd guest_params
  | GotGuestParams (Error err) ->
      let () = Js.log err in
      Tea.Cmd.none
  | ListInfo (Ok res) ->
      let () = Js.log res in
      Tea.Cmd.none
  | ListInfo (Error err) ->
      let () = Js.log err in
      Tea.Cmd.none
  | GoTo _ ->
      Tea.Cmd.none
  | Info (Ok res) ->
      let () = Js.log res in
      Tea.Cmd.none
  | Info (Error err) ->
      let () = Js.log err in
      Tea.Cmd.none
  | Sync _state ->
      (* let () = Js.log state in *)
      Tea.Cmd.none
  | Logout ->
      Matrix.logout () |> ignore ;
      Matrix.stop_client () ;
      Matrix.remove_all_listeners () ;
      Matrix.destroy_store () ;
      Tea.Cmd.batch
        [remove_storage_cmd; register_guest_cmd (); Tea.Cmd.msg @@ GoTo Index]
