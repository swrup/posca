type error = HomeserverError | CredentialsError | NoError

type model =
  {homeserver: string; username: string; password: string; error: error}

let init () =
  {homeserver= Config.matrix_domain; username= ""; password= ""; error= NoError}

type msg =
  | GoTo of Router.route
  | SaveHomeserver of string
  | SaveUserName of string
  | SavePassword of string
  | Login
  | LoggedIn of (Matrix.login_response * string, string) result
  | SavedSession of (unit list, string) result
  | GotClientConfig of (Matrix.client_config * string, string) result
[@@deriving accessors]

(* let login_cmd model = *)
(*   Matrix.login_with_password model.username model.password *)
(*   |> Js.Promise.then_ (fun res -> Js.Promise.resolve (res, Config.matrix_url)) *)
(*   |. Tea_promise.result loggedIn *)

let login_to_remote_server_cmd model localpart base_url =
  Matrix.set_client @@ Matrix.create_client_to_server base_url ;
  Matrix.login_with_password localpart model.password
  |> Js.Promise.then_ (fun res ->
         Js.log res ;
         Matrix.trigger_client_start () ;
         Js.Promise.resolve (res, base_url) )
  |. Tea_promise.result loggedIn

let save_cmd login_response homeserver =
  [ Tea.Ex.LocalStorage.setItem "access_token" login_response##access_token
  ; Tea.Ex.LocalStorage.setItem "matrix_id" login_response##user_id
  ; Tea.Ex.LocalStorage.setItem "home_server" homeserver
  ; Tea.Ex.LocalStorage.setItem "is_guest" "false"
    (* login_response##home_server *)
    (* can't do that because synapse strips the port from the homeserver url *)
  ]
  |> Tea_task.sequence
  |> Tea_task.attempt savedSession

type username = Local of string | Remote of string * string

let parse_username username =
  let regexp = [%re "/@([a-z0-9\\.\\_\\=\\-\\/]+)\\:([a-z0-9\\.\\-]+)/"] in
  (* Js.log (Js.String.match_ regex model.username); *)
  match Js.String.match_ ~regexp username with
  | Some [|_; Some localpart; Some domain|] ->
      Remote (localpart, domain)
  | _ ->
      (* FIXME also test for invalid chararacters *)
      Local username

let find_server_config_cmd model localpart domain =
  Matrix.find_server_config domain
  |> Js.Promise.then_ (fun res -> Js.Promise.resolve (res, localpart))
  |. Tea_promise.result gotClientConfig

let update model = function
  | SaveHomeserver homeserver ->
      ({model with homeserver}, Tea.Cmd.none)
  | SaveUserName username ->
      ({model with username}, Tea.Cmd.none)
  | SavePassword password ->
      ({model with password}, Tea.Cmd.none)
  | Login ->
      let cmd =
        match parse_username model.username with
        | Remote (localpart, domain) ->
            find_server_config_cmd model localpart domain
        | Local localpart ->
            find_server_config_cmd model localpart model.homeserver
      in
      (model, cmd)
  | LoggedIn (Ok (res, homeserver)) ->
      let () = Js.log res in
      (model, Tea.Cmd.batch [Tea.Cmd.msg (GoTo Index); save_cmd res homeserver])
  | LoggedIn (Error err) ->
      let () = Js.log ("login failed: " ^ err) in
      ({model with error= CredentialsError}, Tea.Cmd.none)
  | SavedSession (Ok res) ->
      let () = Js.log res in
      (model, Tea.Cmd.none)
  | SavedSession (Error err) ->
      let () = Js.log err in
      (model, Tea.Cmd.none)
  | GotClientConfig (Ok (client_config, localpart)) -> (
      let () = Js.log client_config in
      let homeserver_wellknown =
        Js.Dict.unsafeGet client_config "m.homeserver"
      in
      match homeserver_wellknown##state with
      | "SUCCESS" ->
          let base_url =
            homeserver_wellknown##base_url
            |> Js.Nullable.toOption |. Belt.Option.getUnsafe
          in
          ( {model with error= NoError}
          , login_to_remote_server_cmd model localpart base_url )
      | _ ->
          ({model with error= HomeserverError}, Tea.Cmd.none) )
  | GotClientConfig (Error err) ->
      let () = Js.log err in
      (model, Tea.Cmd.none)
  | GoTo _ ->
      (* this should never match *)
      (model, Tea.Cmd.none)

let view model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  let error_view = function
    | NoError ->
        noNode
    | HomeserverError ->
        div [class' "my-4 font-bold"] [text "Homeserver error"]
    | CredentialsError ->
        div [class' "my-4 font-bold"] [text "Invalid username or password"]
  in
  div
    [class' "grid h-full place-content-center"]
    [ Tea.Html.form ~unique:"login"
        [ id "login"
        ; class' "cut-corners mx-auto bg-dominant-soft-dark p-10"
        ; onSubmit login ]
        [ h1 [class' "mb-5 text-center text-2xl font-bold"] [text "Login"]
        ; error_view model.error
        ; fieldset
            [class' "grid"]
            [ label [for' "homeserver-field"; class' "p-2"] [text "Homeserver"]
            ; input'
                [ type' "text"
                ; class' "mb-4 rounded-lg bg-dominant-dark p-1 outline-none"
                ; id "homeserver-field"
                ; onInput saveHomeserver
                ; value model.homeserver ]
                []
            ; label
                [for' "username-field"; class' "p-2"]
                [text (T.login_username_label ())]
            ; input'
                [ type' "text"
                ; class' "rounded-lg bg-dominant-dark p-1 outline-none"
                ; id "username-field"
                ; onInput saveUserName
                ; value model.username ]
                []
            ; label
                [for' "password-field"; class' "p-2"]
                [text (T.login_password_label ())]
            ; input'
                [ type' "password"
                ; id "password-field"
                ; class' "rounded-lg bg-dominant-dark p-1 outline-none"
                ; onInput savePassword
                ; value model.password ]
                []
            ; div
                [class' "text-center"]
                [ button
                    [ type' "submit"
                    ; class'
                        "mb-4 mt-8 rounded-lg border px-8 py-2 \
                         hover:bg-dominant-dark" ]
                    [text (T.login_submit ())] ] ]
        ; div
            [class' "flex justify-between"]
            [ div [class' ""] [text (T.login_no_account_yet ())]
            ; Router.link
                ~props:[class' "font-bold underline"]
                goTo Register
                [text (T.login_register_button ())] ] ] ]
