let logged_out_index_view _model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  div
    [class' "mx-auto mt-16 max-w-prose"]
    [h3 [class' "text-center text-3xl"] [text (T.content_welcome ())]]

let logged_in_index_view _model =
  let open Tea.Html in
  div [] []

let view model =
  if Auth.is_logged_in () then logged_in_index_view model
  else logged_out_index_view model
