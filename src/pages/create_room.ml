type model =
  { name: string
  ; topic: string
  ; display: Matrix.room_type
  ; space: Matrix.room_id option }

let init () = {name= ""; topic= ""; display= Chat; space= None}

type msg =
  | GoTo of Router.route
  | SetParams of Matrix.room_type * Matrix.room_id option
  | SaveName of string
  | SaveTopic of string
  | ChangeDisplay of string
  | CreateRoom of Matrix.room_id option
  | CreatedRoom of
      Matrix.room_id option * (Matrix.create_room_response, string) result
  | SentSpaceEvents of (string array, string) result
[@@deriving accessors]

let create_room_cmd model maybe_space_id =
  (* let space = Space.create_space model.statements in *)
  (* Tea.Cmd.msg (GoTo space) *)
  (* Tea.Cmd.msg (GoTo Index) *)
  Js.log (Belt.Option.getWithDefault maybe_space_id "no parent space") ;
  Matrix.create_room model.name ~topic:model.topic ~display:model.display
  |. Tea_promise.result (createdRoom maybe_space_id)

let send_space_events_cmd model room_id = function
  | None ->
      Tea.Cmd.none
  | Some space_id -> (
    match Matrix.client##getRoom space_id |> Js.Nullable.toOption with
    | None ->
        Tea.Cmd.none
    | Some space ->
        let domain = Matrix.client##getDomain () in
        [| Matrix.SpaceRelationState.send_state_event Matrix.client space_id
             "m.space.child" [%mel.obj {via= [|domain|]}] room_id
         ; Matrix.SpaceRelationState.send_state_event Matrix.client room_id
             "m.space.parent" [%mel.obj {via= [|domain|]}] space_id |]
        |> Js.Promise.all
        |. Tea_promise.result sentSpaceEvents )

let display_to_string = function
  | Matrix.Chat ->
      "Chat"
  | Forum ->
      "Forum"
  | Statuses ->
      "Statuses"
  | Media ->
      "Media"
  | Space ->
      "Space"

let display_to_identifier_string = function
  | Matrix.Chat ->
      "org.technostructures.posca.chat"
  | Forum ->
      "org.technostructures.posca.forum"
  | Statuses ->
      "org.technostructures.posca.statuses"
  | Media ->
      "org.technostructures.posca.media"
  | Space ->
      "m.space"

let string_to_display = function
  | "org.technostructures.posca.forum" ->
      Matrix.Forum
  | "org.technostructures.posca.statuses" ->
      Statuses
  | "org.technostructures.posca.media" ->
      Media
  | "m.space" ->
      Space
  | _ ->
      Chat

let update model = function
  | GoTo _ ->
      (model, Tea.Cmd.none)
  | SetParams (display, space) ->
      ({model with display; space}, Tea.Cmd.none)
  | SaveName name ->
      ({model with name}, Tea.Cmd.none)
  | SaveTopic topic ->
      ({model with topic}, Tea.Cmd.none)
  | ChangeDisplay display ->
      ({model with display= string_to_display display}, Tea.Cmd.none)
  | CreateRoom maybe_space_id ->
      (* {model with space = maybe_space}, *)
      (model, create_room_cmd model maybe_space_id)
      (* TODO: add room_id in state to use for edit chat *)
  | CreatedRoom (maybe_space_id, Ok res) ->
      ( model
      , Tea.Cmd.batch
          [ send_space_events_cmd model res##room_id maybe_space_id
          ; Tea.Cmd.msg (goTo (Room (Id res##room_id, None))) ] )
  | CreatedRoom (_maybe_space, Error err) ->
      let () = Js.log ("create space failed: " ^ err) in
      Js.Exn.raiseError "erreur" |> ignore ;
      (model, Tea.Cmd.none)
  | SentSpaceEvents (Ok res) ->
      Js.log res ; (model, Tea.Cmd.none)
  | SentSpaceEvents (Error err) ->
      Js.log err ; (model, Tea.Cmd.none)

let display_border = function
  | Matrix.Space ->
      "border-room-space"
  | Chat ->
      "border-room-chat"
  | Statuses ->
      "border-room-statuses"
  | Forum ->
      "border-room-forum"
  | Media ->
      "border-room-media"

let display_bg = function
  | Matrix.Space ->
      "has-[:checked]:bg-room-space"
  | Chat ->
      "has-[:checked]:bg-room-chat"
  | Statuses ->
      "has-[:checked]:bg-room-statuses"
  | Forum ->
      "has-[:checked]:bg-room-forum"
  | Media ->
      "has-[:checked]:bg-room-media"

let display_selector model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let current_display = model.display in
  Js.log current_display ;
  let is_current_display display = current_display = display in
  let radio_input_and_label display =
    let radio_input display =
      input'
        [ type' "radio"
        ; class' "hidden"
        ; name "display"
        ; value (display_to_identifier_string display)
        ; Tea.Html.Events.onChange ~key:"create_room" changeDisplay
        ; checked (is_current_display display) ]
        []
    in
    label
      [ class'
        @@ "block flex cursor-pointer items-center justify-center rounded-lg \
            border-2 p-1 " ^ display_border display ^ " " ^ display_bg display
      ]
      [radio_input display; text (display_to_string display)]
  in
  div []
    [ label [class' "block p-2"] [text "Room type"]
    ; div
        [class' "grid grid-cols-2 grid-rows-2 gap-x-8 gap-y-4"]
        [ radio_input_and_label Matrix.Chat
        ; radio_input_and_label Matrix.Statuses
        ; radio_input_and_label Matrix.Media
        ; radio_input_and_label Matrix.Forum ] ]

let form_view model maybe_space_id =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  let space = Belt.Option.getWithDefault maybe_space_id "" in
  let () = Js.log space in
  let onSubmit ?(key = "") msg =
    Tea.Html.Events.preventDefaultOn ~key "submit"
      (Tea_json.Decoder.succeed msg)
  in
  let title' = function Matrix.Space -> "Create space" | _ -> "Create room" in
  Tea.Html.form
    ~unique:(Belt.Option.getWithDefault maybe_space_id "")
    [ onSubmit (createRoom maybe_space_id)
    ; class' "cut-corners mx-auto bg-dominant-soft-dark p-10" ]
    [ h1
        [class' "mb-5 text-center text-2xl font-bold"]
        [text @@ title' model.display]
    ; fieldset
        [class' "grid"]
        [ (if model.display <> Space then display_selector model else noNode)
        ; label
            [for' "name-field"; class' "p-2"]
            [text (T.create_chat_name_label ())]
        ; input'
            [ type' "text"
            ; class' "rounded-lg bg-dominant-dark p-1 outline-none"
            ; id "name-field"
            ; onInput saveName ]
            [text model.name]
        ; label
            [for' "topic-field"; class' "p-2"]
            [text (T.create_chat_topic_label ())]
        ; input'
            [ type' "text"
            ; class' "rounded-lg bg-dominant-dark p-1 outline-none"
            ; id "topic-field"
            ; onInput saveTopic ]
            [text model.topic]
        ; div
            [class' "text-center"]
            [ button
                [ class'
                    "mb-4 mt-8 rounded-lg border px-8 py-2 \
                     hover:bg-dominant-dark"
                ; type' "submit" ]
                [text (T.create_chat_submit ())] ] ] ]

let view model room_type maybe_space_id =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  div ~unique:"create_chat"
    [id "create-chat"; class' "grid h-full place-content-center"]
    [form_view model maybe_space_id]
