type color_theme = Dark | Light

type model = {color_theme: color_theme; locale: string ref}

type msg =
  | GoTo of Router.route
  | ChangeColorTheme of string
  | ChangeLocale of string
[@@deriving accessors]

let init () = {color_theme= Dark; locale= Locale.init}

let update model = function
  | GoTo _ ->
      (model, Tea.Cmd.none)
  | ChangeColorTheme "light" ->
      ({model with color_theme= Light}, Tea.Cmd.none)
  | ChangeColorTheme "dark" ->
      ({model with color_theme= Dark}, Tea.Cmd.none)
  | ChangeColorTheme _ ->
      (model, Tea.Cmd.none)
  | ChangeLocale lc ->
      model.locale := lc ;
      Time_ago.update_locale lc ;
      (model, Tea.Cmd.none)

let view model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  let locale_options =
    let select_option locale =
      option [value locale; selected (locale = !(model.locale))] [text locale]
    in
    div
      [class' "grid"]
      [ label [for' "locale-field"; class' "p-2"] [text "Interface language"]
      ; select
          [ class' "bg-dominant-dark p-2"
          ; id "locale-field"
          ; onChange changeLocale ]
          [select_option "en"; select_option "fr"] ]
  in
  let color_theme_options =
    let color_theme_label color_theme =
      match color_theme with
      | Dark ->
          T.header_dark_mode ()
      | Light ->
          T.header_light_mode ()
    in
    let color_theme_name color_theme =
      match color_theme with Dark -> "dark" | Light -> "light"
    in
    let radio_and_label color_theme =
      let radio_input color_theme =
        input'
          [ type' "radio"
          ; class' "mr-1"
          ; name "display"
          ; value @@ color_theme_name color_theme
          ; onChange changeColorTheme
          ; checked (color_theme = model.color_theme) ]
          []
      in
      label
        [class' "px-2"]
        [radio_input color_theme; text @@ color_theme_label color_theme]
    in
    div
      [class' "grid"]
      [ label [for' "color-scheme-field"; class' "p-2"] [text "Color scheme"]
      ; div
          [class' "flex flex-row"]
          [radio_and_label Dark; radio_and_label Light] ]
  in
  let setting name options =
    div [class' ""] [div [class' ""] [text name]; div [class' ""] [options]]
  in
  div ~unique:"settings"
    [class' "grid h-full place-content-center"]
    [ div
        [class' "cut-corners mx-auto bg-dominant-soft-dark p-10"]
        [ h1 [class' "mb-5 text-center text-2xl font-bold"] [text "Settings"]
        ; locale_options
        ; color_theme_options ] ]
