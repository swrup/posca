open Matrix_sdk.Interactive_auth

type error =
  | HomeserverError
  | UsernameInUse
  | InvalidUsername
  | PasswordEmpty
  | PasswordsDontMatch
  | EmailInUse
  | NeedsEmail
  | UnknownError
  | NoError

type step = Credentials | InteractiveStart | Recaptcha | Terms | Email

type model =
  { homeserver: string
  ; username: string
  ; password: string
  ; password_confirm: string
  ; email: string
  ; needs_email: bool
  ; error: error
  ; step: step
  ; iauth: Matrix.login_response interactive_auth option }

let init () =
  { homeserver= Config.matrix_domain
  ; username= ""
  ; password= ""
  ; password_confirm= ""
  ; email= ""
  ; needs_email= false
  ; error= NoError
  ; step= Credentials
  ; iauth= None }

type msg =
  | GoTo of Router.route
  | SaveHomeserver of string
  | SaveUserName of string
  | SavePassword of string
  | SavePasswordConfirm of string
  | SaveEmail of string
  | SubmitCredentials
  | AttemptedAuth of (Matrix.login_response, string) result
  | StateChanged of auth_type * stage_status
  | GotRecaptchaResponse of string
  | AcceptTerms
  | CreatedIAuth of Matrix.login_response interactive_auth
  | SavedSession of (unit list, string) result
  | GotClientConfig of (Matrix.client_config, string) result
[@@deriving accessors]

let msg_to_string = function
  | SaveHomeserver _ ->
      "save homeserver"
  | SaveUserName _msg ->
      "save username"
  | SavePassword _msg ->
      "save password"
  | SavePasswordConfirm _msg ->
      "save password confirm"
  | SaveEmail _msg ->
      "save password"
  | SubmitCredentials ->
      "SubmitCredentials"
  | GoTo _msg ->
      "goto"
  | SavedSession _msg ->
      "saved session"
  | GotClientConfig _msg ->
      "got client config"
  | StateChanged _ ->
      "state changed"
  | CreatedIAuth _ ->
      "created iauth"
  | AttemptedAuth _ ->
      "created iauth"
  | GotRecaptchaResponse _ ->
      "got recaptcha response"
  | AcceptTerms ->
      "accept terms"

let subscribe_to_iauth model created_tagger state_updated_tagger =
  let open Vdom in
  let enableCall callbacks =
    let options =
      interactive_auth_options ~matrixClient:Matrix.client
        ~inputs:(inputs ~emailAddress:model.email ())
        ~doRequest:(fun auth_dict ->
          Matrix.register model.username model.password None
            (Js.Null.toOption auth_dict) )
        ~stateUpdated:(fun auth_type stage_status ->
          callbacks.enqueue (state_updated_tagger auth_type stage_status) )
        ~requestEmailToken:(fun email secret attempt session_id ->
          Matrix.request_register_email_token email secret attempt )
        ()
    in
    let iauth = new_interactive_auth options in
    let poll_interval_id =
      Js.Global.setInterval ~f:(fun () -> iauth##poll () |> ignore) 2000
    in
    callbacks.enqueue (created_tagger iauth) ;
    fun () -> Js.Global.clearInterval poll_interval_id
  in
  Tea_sub.registration "Register.iauth" enableCall

let subscriptions model =
  if model.step = Credentials then Tea.Sub.none
  else subscribe_to_iauth model createdIAuth stateChanged

let find_server_config_cmd model =
  Matrix.find_server_config model.homeserver
  |. Tea_promise.result gotClientConfig

let attempt_auth_cmd iauth =
  Js.log "attempting auth" ;
  iauth##attemptAuth () |. Tea_promise.result attemptedAuth

(* TODO: confirm password *)
(* TODO: errors when fields are empty *)

let save_cmd login_response base_url =
  [ Tea.Ex.LocalStorage.setItem "access_token" login_response##access_token
  ; Tea.Ex.LocalStorage.setItem "matrix_id" login_response##user_id
  ; Tea.Ex.LocalStorage.setItem "home_server" base_url
  ; Tea.Ex.LocalStorage.setItem "is_guest" "false" ]
  |> Tea_task.sequence
  |> Tea_task.attempt savedSession

let needs_email maybe_flows =
  match Js.Undefined.toOption maybe_flows with
  | Some flows ->
      Js.Array.every flows ~f:(fun flow ->
          Js.Array.some flow.stages ~f:(fun stage ->
              stage = "m.login.email.identity" ) )
  | None ->
      false

let update model = function
  | SaveHomeserver homeserver ->
      ({model with homeserver}, Tea.Cmd.none)
  | SaveUserName username ->
      ({model with username}, Tea.Cmd.none)
  | SavePassword password ->
      ({model with password}, Tea.Cmd.none)
  | SavePasswordConfirm password_confirm ->
      ({model with password_confirm}, Tea.Cmd.none)
  | SaveEmail email ->
      ({model with email}, Tea.Cmd.none)
  | SubmitCredentials ->
      if model.password = "" || model.password_confirm = "" then
        ({model with error= PasswordEmpty}, Tea.Cmd.none)
      else if model.password <> model.password_confirm then
        ({model with error= PasswordsDontMatch}, Tea.Cmd.none)
      else (model, find_server_config_cmd model)
  | SavedSession (Ok res) ->
      let () = Js.log res in
      (model, Tea.Cmd.none)
  | SavedSession (Error err) ->
      let () = Js.log err in
      (model, Tea.Cmd.none)
  | GotClientConfig (Ok client_config) -> (
      let () = Js.log client_config in
      let homeserver_wellknown =
        Js.Dict.unsafeGet client_config "m.homeserver"
      in
      match homeserver_wellknown##state with
      | "SUCCESS" ->
          let base_url =
            homeserver_wellknown##base_url
            |> Js.Nullable.toOption |. Belt.Option.getUnsafe
          in
          Matrix.set_client @@ Matrix.create_client_to_server base_url ;
          ({model with step= InteractiveStart; error= NoError}, Tea.Cmd.none)
      | _ ->
          ({model with error= HomeserverError}, Tea.Cmd.none) )
  | GotClientConfig (Error err) ->
      Js.log 1 ;
      let () = Js.log err in
      (model, Tea.Cmd.none)
  | StateChanged ("m.login.recaptcha", _) ->
      ( {model with step= Recaptcha}
      , Html.global_callback_cmd "recaptchaCallback" gotRecaptchaResponse )
  | StateChanged ("m.login.terms", _) ->
      ({model with step= Terms}, Tea.Cmd.none)
  | StateChanged ("m.login.email.identity", _) ->
      ({model with step= Email}, Tea.Cmd.none)
  | StateChanged (auth_type, stage_status) ->
      let () = Js.log auth_type in
      let () = Js.log stage_status in
      (model, Tea.Cmd.none)
  | GotRecaptchaResponse response ->
      Js.log "got recaptcha_response" ;
      let iauth = Belt.Option.getUnsafe model.iauth in
      let auth_dict = Obj.magic @@ {type'= "m.login.recaptcha"; response} in
      Js.log auth_dict ;
      iauth##submitAuthDict auth_dict false ;
      (model, Tea.Cmd.none)
  | AcceptTerms ->
      Js.log "accept terms" ;
      let iauth = Belt.Option.getUnsafe model.iauth in
      let auth_dict = Obj.magic @@ {type'= "m.login.terms"} in
      Js.log auth_dict ;
      iauth##submitAuthDict auth_dict false ;
      (model, Tea.Cmd.none)
  | CreatedIAuth iauth ->
      ({model with iauth= Some iauth}, attempt_auth_cmd iauth)
  | AttemptedAuth (Ok res) ->
      Js.log 2 ;
      let () = Js.log res in
      let base_url = Matrix.client##baseUrl in
      Matrix.set_client
      @@ Matrix.create_client_with_token ~base_url res##user_id
           res##access_token false ;
      Matrix.trigger_client_start () ;
      (model, Tea.Cmd.batch [Tea.Cmd.msg (GoTo Index); save_cmd res base_url])
  | AttemptedAuth (Error err) -> (
      Js.log 3 ;
      let () = Js.log err in
      let error : matrix_error = Obj.magic err in
      match error.errcode |> Js.Undefined.toOption with
      | Some "M_USER_IN_USE" ->
          ({model with step= Credentials; error= UsernameInUse}, Tea.Cmd.none)
      | Some "M_THREEPID_IN_USE" ->
          ({model with step= Credentials; error= EmailInUse}, Tea.Cmd.none)
      | Some "M_INVALID_USERNAME" ->
          ({model with step= Credentials; error= InvalidUsername}, Tea.Cmd.none)
      | Some _ ->
          ({model with step= Credentials; error= UnknownError}, Tea.Cmd.none)
      | None ->
          if needs_email error.flows then
            ( { model with
                step= Credentials
              ; needs_email= true
              ; error= NeedsEmail }
            , Tea.Cmd.none )
          else
            ({model with step= Credentials; error= UnknownError}, Tea.Cmd.none)
      )
  | GoTo _ ->
      (* this should never match *)
      (model, Tea.Cmd.none)

let view model =
  let open Tea.Html in
  let open Tea.Html.Attributes in
  let open Tea.Html.Events in
  let error_view = function
    | NoError ->
        noNode
    | HomeserverError ->
        div [class' "my-4 font-bold"] [text "Homeserver error"]
    | UsernameInUse ->
        div [class' "my-4 font-bold"] [text "Username already in use"]
    | InvalidUsername ->
        div [class' "my-4 font-bold"] [text "Invalid username"]
    | PasswordEmpty ->
        div [class' "my-4 font-bold"] [text "No password specified"]
    | PasswordsDontMatch ->
        div [class' "my-4 font-bold"] [text "Passwords don't match"]
    | EmailInUse ->
        div [class' "my-4 font-bold"] [text "Email already in use"]
    | NeedsEmail ->
        div
          [class' "my-4 font-bold"]
          [text "You need to specify an email address"]
    | UnknownError ->
        div [class' "my-4 font-bold"] [text "Unknown error"]
  in
  let step_view = function
    | Credentials | InteractiveStart ->
        Tea.Html.form
          [onSubmit submitCredentials]
          [ fieldset
              [class' "grid"]
              [ label [for' "homeserver-field"; class' "p-2"] [text "Homeserver"]
              ; input'
                  [ type' "text"
                  ; class' "mb-4 rounded-lg bg-dominant-dark p-1 outline-none"
                  ; id "homeserver-field"
                  ; onInput saveHomeserver
                  ; value model.homeserver ]
                  []
              ; label
                  [for' "username-field"; class' "p-2"]
                  [text (T.signup_username_label ())]
              ; input'
                  [ type' "text"
                  ; class' "rounded-lg bg-dominant-dark p-1 outline-none"
                  ; id "username-field"
                  ; onInput saveUserName
                  ; value model.username ]
                  []
              ; label
                  [for' "password-field"; class' "p-2"]
                  [text (T.signup_password_label ())]
              ; input'
                  [ type' "password"
                  ; class' "rounded-lg bg-dominant-dark p-1 outline-none"
                  ; id "password-field"
                  ; onInput savePassword
                  ; value model.password ]
                  []
              ; label
                  [for' "password-confirm-field"; class' "p-2"]
                  [text "Confirm password"]
              ; input'
                  [ type' "password"
                  ; class' "rounded-lg bg-dominant-dark p-1 outline-none"
                  ; id "password-confirm-field"
                  ; onInput savePasswordConfirm
                  ; value model.password_confirm ]
                  []
              ; ( if model.needs_email then
                    div
                      [class' "grid"]
                      [ label [for' "email-field"; class' "p-2"] [text "Email"]
                      ; input'
                          [ type' "text"
                          ; class'
                              "rounded-lg bg-dominant-dark p-1 outline-none"
                          ; id "email-field"
                          ; onInput saveEmail
                          ; value model.email ]
                          [] ]
                  else noNode )
              ; div
                  [class' "text-center"]
                  [ button
                      [ type' "submit"
                      ; class'
                          "mb-4 mt-8 rounded-lg border px-8 py-2 \
                           hover:bg-dominant-dark" ]
                      [text (T.signup_submit ())] ] ] ]
    | Recaptcha ->
        let recaptcha_params =
          get_params_recaptcha (Belt.Option.getUnsafe model.iauth) ()
        in
        let public_key = recaptcha_params.public_key in
        fieldset
          [class' "grid"]
          [ div [] [text "Captcha"]
          ; div
              [ class' "g-recaptcha"
              ; Vdom.attribute "" "data-sitekey" public_key
              ; Vdom.attribute "" "data-callback" "recaptchaCallback"
              ; Vdom.attribute "" "data-theme" "dark" ]
              []
          ; Vdom.fullnode "" "script" "" ""
              [ Vdom.attribute "" "src"
                  "https://www.recaptcha.net/recaptcha/api.js" ]
              [] ]
    | Terms ->
        let terms_params =
          get_params_terms (Belt.Option.getUnsafe model.iauth) ()
        in
        let policy_items =
          Js.Dict.values terms_params.policies
          |. Belt.Array.map (fun policy ->
                 let policy_en = Js.Dict.unsafeGet policy "en" in
                 li []
                   [ a
                       [ href policy_en.url
                       ; target "_blank"
                       ; class' "cursor-pointer underline" ]
                       [text policy_en.name] ] )
          |> Belt.List.fromArray
        in
        fieldset
          [class' "grid"]
          [ div [] [text "Terms"]
          ; ul [class' "list-disc"] policy_items
          ; div
              [class' "text-center"]
              [ button ~key:"accept_terms"
                  [ onClick acceptTerms
                  ; class'
                      "mb-4 mt-8 rounded-lg border px-8 py-2 \
                       hover:bg-dominant-dark" ]
                  [text "Accept"] ] ]
    | Email ->
        div [] [text "Waiting for email confirmation"]
  in
  div ~unique:"signup"
    [class' "grid h-full place-content-center"]
    [ div
        [class' "cut-corners mx-auto bg-dominant-soft-dark p-10"]
        [ h1 [class' "mb-5 text-center text-2xl font-bold"] [text "Register"]
        ; error_view model.error
        ; step_view model.step ] ]
