external env : string = "import.meta.env.MODE"

external base_url : string = "import.meta.env.BASE_URL"

external is_prod : bool = "import.meta.env.PROD"

external is_dev : bool = "import.meta.env.DEV"

external matrix_url : string = "import.meta.env.VITE_MATRIX_URL"

external matrix_domain : string = "import.meta.env.VITE_MATRIX_DOMAIN"
