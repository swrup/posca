# Posca

[![Latest Release](https://gitlab.com/technostructures/posca/posca/-/badges/release.svg)](https://gitlab.com/technostructures/posca/posca/-/releases)
[![Matrix room](https://img.shields.io/matrix/posca:matrix.asso-fare.fr)](https://matrix.to/#/#posca:matrix.asso-fare.fr)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/technostructures/posca/posca?branch=main)](https://gitlab.com/technostructures/posca/posca/-/pipelines)

Posca is an experimental social network based on Matrix.

For a description of the project, see [our article](https://technostructures.org/en/projects/posca/).

Specifications can be found in [the wiki](https://gitlab.com/technostructures/posca/posca/-/wikis/Home).

## Features

- [x] Guest mode
- [x] Tree of replies
    - [x] Displayed in a breadth-first way
    - [x] Displayed in a depth-first way
    - [ ] Sorted by likes
- [x] Typed rooms for specific displays: statuses, forum, media (using [MSC1772](https://github.com/matrix-org/matrix-spec-proposals/pull/1772))
- [x] Chat view
    - [ ] Threads
    - [ ] Reply tree
- [x] Statuses view
    - [x] Threads
    - [x] Reply tree
- [x] Forum view
    - [x] Threads
    - [x] Reply tree
    - [x] Title
- [x] Media view
    - [x] Threads
    - [x] Reply tree
    - [ ] Albums
    - [ ] New messages with upload
- [ ] Generic features
    - [x] Login
        - [ ] OIDC login
    - [x] Register
        - [x] Username and password
        - [x] With email confirmation
        - [x] With captcha
    - [x] WYSIWYG editor
    - [x] Likes
    - [ ] Reactions
    - [ ] Mentions
    - [ ] Joins and leaves
    - [ ] Invitations
    - [ ] Knocks
    - [ ] E2EE
    - [ ] Editions
    - [ ] Deletions
    - [ ] Member list
    - [ ] Room settings
    - [ ] Profile settings
    - [ ] Notifications
    - [ ] Room list filters
    - [ ] Search
    - [ ] Sliding sync
    - [ ] Read receipts
- [x] PWA
- [ ] Shareable links
- [ ] Integration with kazarma
- [ ] Extensible profile rooms (using [MSC1769](https://github.com/matrix-org/matrix-spec-proposals/pull/1769))
- [ ] Wall rooms (using [MSC1769](https://github.com/matrix-org/matrix-spec-proposals/pull/1769))
- [ ] Feed rooms with filters (including user activity rooms)
- [ ] Event forwarding (using [MSC2730](https://github.com/matrix-org/matrix-spec-proposals/pull/2730))
- [ ] Moderation tools
- [ ] Keyboard navigation

See the [development board](https://gitlab.com/technostructures/posca/posca/-/boards/5918980).

## Try it

- The `main` branch is deployed on [posca.pm](https://posca.pm)
- The `develop` branch is deployed on [dev.posca.pm](https://dev.posca.pm)

Until we find the means to host a proper infrastructure, those instances use the `matrix.org` homeserver by default. This means that guest accounts are created on `matrix.org`.

[Here is an example of a public thread.](https://posca.pm/#/room/!WwdyqVGLhAqmBvWyBX:matrix.org/$7Jmww5ROZB6ts_iGDQQv5trWBlwI6BqhJLQ-HNCwXmU)

## Deploy using Docker

We build [a Docker image](https://gitlab.com/technostructures/posca/posca/container_registry/4397972?orderBy=NAME&sort=desc&search[]=) that serves the application using Nginx.
The `latest` tag packages the tip of the `main` branch, semver tags match releases.

```bash
docker run -d --restart=unless-stoped --name posca \
    -e POSCA_MATRIX_URL=https://matrix.example.com \
    -e POSCA_MATRIX_DOMAIN=example.com \
    registry.gitlab.com/technostructures/posca/posca:latest
```

## Developer setup

### Quick Start

You need [opam](https://opam.ocaml.org/doc/Install.html) (on Windows, use the [DkML installer](https://diskuv.com/dkmlbook/#how-to-install)), [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) and GNU make (on Windows, you can get it from [Chocolatey](https://community.chocolatey.org/packages/make)). On Windows, it may be easier to use [WSL](https://learn.microsoft.com/en-us/windows/wsl/install).

```shell
make init
make dev
```

If dependencies have been updated afterwards, run `make install`.

### Commands

You can see all available commands by running `make help` or just `make`. Here
are a few of the most useful ones:

- `make init`: set up opam local switch and download OCaml, Melange and
JavaScript dependencies
- `make install`: install OCaml, Melange and JavaScript dependencies
- `make dev`: run a development server that watches for changes
- `make serve`: serve a production build with a local HTTP server
- `make bundle`: bundle a production build in the `dist/` directory
- `make format`: format
- `npm run sort_tailwind_classes`: sort Tailwind classes inside views (you should format afterwards)

## Thanks

Posca leverages the following open-source dependencies – thanks to them!

- [bytesize-icons](https://github.com/danklammer/bytesize-icons)
- [DOMPurify](https://github.com/cure53/DOMPurify)
- [Fuse.js](https://www.fusejs.io)
- [javascript-time-ago](https://gitlab.com/catamphetamine/javascript-time-ago)
- [markdown-it](https://github.com/markdown-it/markdown-it)
- [matrix-js-sdk](https://github.com/matrix-org/matrix-js-sdk)
- [Melange](https://melange.re)
- [Remix Icon](https://remixicon.com)
- [Tailwind CSS](https://tailwindcss.com)
- [Tiptap](https://tiptap.dev)
- [Vite](https://vitejs.dev)
