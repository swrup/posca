FROM registry.gitlab.com/technostructures/docker-images/opam-npm:alpine-ocaml-5.1 as build-stage

COPY --link --chown=1000:1000 posca.opam /home/opam/

RUN opam switch create . 5.1.1 -y --deps-only

RUN opam update

RUN opam install -y . --deps-only

RUN opam pin -y add posca.dev .

COPY --link --chown=1000:1000 package.json package-lock.json /home/opam/

COPY --link --chown=1000:1000 packages /home/opam/packages/

RUN npm install --legacy-peer-deps

COPY --link --chown=1000:1000 dune dune-project index.html postcss.config.js tailwind.config.js vite.config.js Makefile /home/opam/

COPY --link --chown=1000:1000 icons /home/opam/icons/

COPY --link --chown=1000:1000 locales /home/opam/locales/

COPY --link --chown=1000:1000 public /home/opam/public/

COPY --link --chown=1000:1000 styles /home/opam/styles/

COPY --link --chown=1000:1000 src /home/opam/src/

ENV VITE_MATRIX_URL=POSCA_MATRIX_URL

ENV VITE_MATRIX_DOMAIN=POSCA_MATRIX_DOMAIN

ENV NODE_ENV=production

RUN npm run bundle



FROM nginx:1.25-alpine

COPY --link nginx-app-config.sh /docker-entrypoint.d/

COPY --link --from=build-stage /home/opam/dist/ /usr/share/nginx/html

COPY nginx.conf /etc/nginx/conf.d/default.conf
