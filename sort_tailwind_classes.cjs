const fs = require('fs')
const path = require('path')
const process = require('process')
const glob = require('glob')

const tailwindConfigPath = path.resolve(process.cwd(), 'tailwind.config.js')
const file_glob = "./src/**/*.ml"

const resolveConfig = require('./node_modules/tailwindcss/resolveConfig')
const createContext = require('./node_modules/tailwindcss/lib/lib/setupContextUtils.js').createContext
const loadConfig = require('./node_modules/tailwindcss/loadConfig')

const loadedConfig = loadConfig(tailwindConfigPath)
const tailwindConfig = loadedConfig.default ?? loadedConfig

var context = createContext(resolveConfig(tailwindConfig))

function bigSign(bigIntValue) {
  return (bigIntValue > 0n) - (bigIntValue < 0n)
}

function sortClassList(classList) {
  let classNamesWithOrder = context.getClassOrder(classList)

  return classNamesWithOrder
    .sort(([, a], [, z]) => {
      if (a === z) return 0
      // if (a === null) return options.unknownClassPosition === 'start' ? -1 : 1
      // if (z === null) return options.unknownClassPosition === 'start' ? 1 : -1
      if (a === null) return -1
      if (z === null) return 1
      return bigSign(a - z)
    })
    .map(([className]) => className)
}

function sortClasses(classStr) {
  let result = ''

  const parts = classStr.split(/([\t\r\f\n ]+)/)
  const whitespace = parts.filter((_, i) => i % 2 !== 0)

  let classes = parts.filter((_, i) => i % 2 === 0)

  if (classes[classes.length - 1] === '') {
    classes.pop()
  }

  classes = sortClassList(classes)

  for (let i = 0; i < classes.length; i++) {
    result += `${classes[i]}${whitespace[i] ?? ''}`
  }

  return result
}

function sortClassesInFile(file) {
  const file_path = path.resolve(process.cwd(), file)

  const content = fs.readFileSync(file_path, {encoding: "utf-8"})

  const new_content = content.replaceAll(/"(.*?)"/gs, function (whole_match, match) {
    const match_without_newlines = match.replaceAll(/\\\n\W*/g, "")
    return ("\"" + sortClasses(match_without_newlines) + "\"")
  })

  fs.writeFileSync(file_path, new_content)
}

for (let file of glob.sync(file_glob)) {
  sortClassesInFile(file)
}
