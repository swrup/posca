module.exports = {
  content: ['./index.html', './src/**/*.ml'],
  darkMode: 'class',
  theme: {
    extend: {},
    colors: {
      'abyss-blue': '#2980b9',
      'alternate-gray': '#bdc3c7',
      'beware-orange': '#f67400',
      'burnt-charcoal': '#3b4045',
      'cardboard-gray': '#eff0f1',
      'charcoal-gray': '#31363b',
      'coastal-fog': '#7f8c8d',
      'danger-red': '#ed1515',
      'deco-blue': '#1e92ff',
      'hover-blue': '#93cee9',
      'hyper-blue': '#3daee6',
      'icon-blue': '#1d99f3',
      'icon-gray': '#4d4d4d',
      'icon-green': '#2ecc71',
      'icon-prune': '#804453',
      'icon-red': '#da4453',
      'icon-yellow': '#fdbc4b',
      'lazy-gray': '#afb0b3',
      'noble-fir': '#27ae60',
      'paper-white': '#fcfcfc',
      'pimpinella': '#e74c3c',
      'plasma-blue': '#3daee9',
      'shade-black': '#232629',
      'sunbeam-yellow': '#c9ce3b',
      'verdant-green': '#11d116',
      'dominant': {
        'dark': '#122149',
        'light': '#ffffff',
        'soft-dark': '#1d2f56',
        'soft-light': '#ffffff'
      },
      'contrast': {
        'dark': '#ffffff',
        'light': '#000000'
      },
      'room': {
        'space': '#efa437',
        'statuses': '#4e8384',
        'chat': '#0f5932',
        'forum': '#f4cd51',
        'media': '#ce6a42',
      },
      'username': {
        '1': '#efa437',
        '2': '#7c4d59',
        '3': '#4e8384',
        '4': '#f4cd51',
        '5': '#ce6a42',
        '6': '#4e6282',
        '7': '#6b4e7f',
        '8': '#0f5932'
      }
    }
  },
  plugins: [require('@tailwindcss/typography')],
}
