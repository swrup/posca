# title =
#     .index = Posca
#     .login = Posca – Login
#     .signup = Posca – Register
#     .logout = Posca – Logout
#     .create-group = Posca – New group
#     .create-chat = Posca – New chat
#     .chat = Posca – Chat
#     .group = Posca – Group

router =
    .title =
        { $page ->
           *[index] Posca
            [login] Connexion – Posca
            [signup] Nouveau compte – Posca
            [logout] Déconnexion – Posca
            [create-group] Nouveau groupe – Posca
            [create-chat] Nouvelle conversation – Posca
            [chat] Chat – Posca
            [group] Groupe – Posca
        }
header =
    .settings = Paramètres
    .profile = Profil
    .login = Connexion
    .logout = Déconnexion
    .dark-mode = Mode nuit
    .light-mode = Mode jour
    .toggle-menu = Afficher le menu
login =
    .username-label = Nom d'utilisateur
    .password-label = Mot de passe
    .submit = Envoyer
    .no-account-yet = Pas encore de compte ?
    .register-button = Créer un compte
signup =
    .username-label = Nom d'utilisateur
    .password-label = Mot de passe
    .submit = Envoyer
content =
    .welcome = Bienvenue sur Posca !
sidebar =
    .new-chat = Nouvelle conversation
    .outside-groups = En-dehors des groupes
    .create-group = Créer un groupe
group =
    .links-title = Liens
    .chats-title = Conversations
    .events-title = Évènements
    .room-not-found = Salon introuvable
chat =
    .room-not-found = Salon introuvable
    .message-date = { DATETIME($date, day: "2-digit", month: "2-digit", year: "2-digit", hour: "2-digit", minute: "2-digit", second: "2-digit") }

create-chat =
    .name-label = Nom
    .topic-label = Description
    .submit = Créer
