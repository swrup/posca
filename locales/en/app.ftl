# title =
#     .index = Posca
#     .login = Posca – Login
#     .signup = Posca – Register
#     .logout = Posca – Logout
#     .create-group = Posca – New group
#     .create-chat = Posca – New chat
#     .chat = Posca – Chat
#     .group = Posca – Group

router =
    .title = {$page ->
    *[index] Posca
    [login] Login – Posca
    [signup] Register – Posca
    [logout] Logout – Posca
    [create-group] New group – Posca
    [create-chat] New chat – Posca
    [chat] Chat – Posca
    [group] Group – Posca
    }

header =
    .settings = Settings
    .profile = Profile
    .login = Login
    .logout = Logout
    .dark-mode = Dark mode
    .light-mode = Light mode
    .toggle-menu = Toggle menu

login =
    .username-label = Username
    .password-label = Password
    .submit = Login
    .no-account-yet = No account yet?
    .register-button = Register

signup =
    .username-label = Username
    .password-label = Password
    .submit = Register

content =
    .welcome = Welcome on Posca!

sidebar =
    .new-chat = New chat
    .outside-groups = Outside groups
    .create-group = Create group

group =
    .links-title = Links
    .chats-title = Chats
    .events-title = Events
    .room-not-found = Room not found

chat =
    .room-not-found = Room not found
    .message-date = {DATETIME($date, day: "2-digit", month: "2-digit", year: "2-digit", hour: "2-digit", minute: "2-digit", second: "2-digit")}

create-chat =
    .name-label = Name
    .topic-label = Topic
    .submit = Create
